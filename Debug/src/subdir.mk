################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../src/adcExtern.c \
../src/adcIntern.c \
../src/cmd.c \
../src/communicate.c \
../src/externTemp.c \
../src/flashExtern.c \
../src/hardware.c \
../src/iic.c \
../src/input.c \
../src/looslessMeasure.c \
../src/main.c \
../src/measurement.c \
../src/memory.c \
../src/onBoardTemp.c \
../src/param.c \
../src/serielle.c \
../src/serielleHandy.c \
../src/spi.c \
../src/status.c \
../src/rtc.c \
../src/timer.c \
../src/status_led.c \
../src/twn4.c \
../src/accessCtrl.c \
../src/sc16is7xx.c \
../src/accelerometer.c \
../src/iic_accel.c \
../src/stringHelper.c

S_SRCS += \
../src/Startup.s

OBJS += \
./src/Startup.o \
./src/adcExtern.o \
./src/adcIntern.o \
./src/cmd.o \
./src/communicate.o \
./src/externTemp.o \
./src/flashExtern.o \
./src/hardware.o \
./src/iic.o \
./src/input.o \
./src/looslessMeasure.o \
./src/main.o \
./src/measurement.o \
./src/memory.o \
./src/onBoardTemp.o \
./src/param.o \
./src/serielle.o \
./src/serielleHandy.o \
./src/spi.o \
./src/status.o \
./src/rtc.o \
./src/timer.o \
./src/status_led.o \
./src/twn4.o \
./src/accessCtrl.o \
./src/sc16is7xx.o  \
./src/accelerometer.o \
./src/iic_accel.o \
./src/stringHelper.o

C_DEPS += \
./src/adcExtern.d \
./src/adcIntern.d \
./src/cmd.d \
./src/communicate.d \
./src/externTemp.d \
./src/flashExtern.d \
./src/hardware.d \
./src/iic.d \
./src/input.d \
./src/looslessMeasure.d \
./src/main.d \
./src/measurement.d \
./src/memory.d \
./src/onBoardTemp.d \
./src/param.d \
./src/serielle.d \
./src/serielleHandy.d \
./src/spi.d \
./src/status.d \
./src/rtc.d \
./src/timer.d \
./src/status_led.d \
./src/twn4.d \
./src/accessCtrl.d \
./src/sc16is7xx.d \
./src/accelerometer.d \
./src/iic_accel.d \
./src/stringHelper.d

S_DEPS += \
./src/Startup.d


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Windows GCC Assembler'
	arm-elf-gcc -x assembler-with-cpp -Wall -Wa,-adhlns="$@.lst" -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -mcpu=arm7tdmi -g3 -gdwarf-2 -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Windows GCC C Compiler'
	arm-elf-gcc -O0 -Wall -Wa,-adhlns="$@.lst" -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -mcpu=arm7tdmi -g3 -gdwarf-2 -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
