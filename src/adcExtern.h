/*
* spi.h
*
*  Created on: 22.11.2014
*      Author: Hütter Stefan
*/
#ifndef ADCEXTERN_H_
#define ADCEXTERN_H_

#define ADCEXTERN_CH_0  0
#define ADCEXTERN_CH_1  1

void adcExtern_init() ;
unsigned int adcExtern_read(unsigned char channel);
#endif //ADCEXTERN_H_
