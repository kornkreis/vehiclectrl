/*
* twn4.h
*
*  Created on: 23.02.2016
*      Author: Hütter Wolfgang
*/
#ifndef TWN4_H_
#define TWN4_H_
#include <stdint.h>

#define TWN4_ADDRESS 0x30

#define TWN4_ERR_NO           0
#define TWN4_ERR_TRANS_IIC    1

#define TWN4_ONLINE     1
#define TWN4_OFFLINE    0

#define TWN4_MAX_ID_SIZE 16  // do not change without change memory management
#define TWN4_ID_ACCESS_BYTE 15

extern char Twn4LastIDString[TWN4_MAX_ID_SIZE*2];
extern uint32_t Twn4LastIDAddress;

extern char Twn4TempString[TWN4_MAX_ID_SIZE * 2];

extern unsigned char Twn4IdChanged;
extern unsigned char twn4online;

unsigned char Twn4Busy;

void twn4_init();
unsigned char twn4_check_waiting_message();
void twn4_handle_message();

#endif //TWN4_H_
