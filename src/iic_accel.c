/*
    date:   15.07.2015
    author: Sonja Klug
*/

#include "iic_accel.h"
#include "iic.h"
#include "LPC214x.h"
#include "MMA8652FC.h"
#include "accelerometer.h"


// Schreibt in ein Register
void I2C_WriteRegister(unsigned char SlaveAddress, unsigned char RegisterAddress, char Data)
{
	I2C_Start();                       // start condition
	I2C_Wait();                        // wait for ACK
	I20DAT = SlaveAddress << 1;		   // slaveaddress with 0 for write
	I20CONCLR = 1<<STA;              // reset start -> startflag l�schen
    I20CONCLR = 1<<SI;                 // interrupt flag l�schen
	I2C_Wait();                        //auf ACK warten

	I20DAT = RegisterAddress;	      // register address
	I20CONCLR = 1<<SI;
	I2C_Wait();                       //auf ACK warten

	I20DAT = Data;					 // write data
	I20CONCLR = 1<<SI;
	I2C_Wait();

	I20CONCLR = (1<<STAC) | (1<<SIC);   // clear start flag, clear interrupt flag

	I2C_Stop();
}

// Liest aus einem Register
unsigned char I2C_ReadRegister(unsigned char SlaveAddress, unsigned char RegisterAddress)
{
    unsigned char data;

    I2C_Start();                     // startcondition
    I2C_Wait();                     // wait for acknowledge
    I20DAT = SlaveAddress << 1;     // slaveaddress with 0 for write
    I20CONCLR = 1 << STA;           // reset start -> startflag l�schen
    I20CONCLR = 1<<SI;              // interrupt flag l�schen
    I2C_Wait();
    // status = 0x18
    I20DAT = RegisterAddress;       // register address
    I20CONCLR = 1<<SI;
    I2C_Wait();                     // wait for acknowledge
    // status = 0x28
    I2C_RepeatedStart();
    I20CONCLR = 1<<SI;
    I2C_Wait();                     // wait for acknowledge
    // status = 0x16
    I20DAT = (SlaveAddress << 1) | 0x01; // slaveaddress with 1 for read
    I20CONCLR = 1 << STA;           // reset start -> startflag l�schen
    I20CONCLR = 1<<SI;              // interrupt flag l�schen
    I2C_Wait();                     // wait for acknowledge
    // status 0x40
    I20CONCLR = 1<<SI;              // interupt flag l�schen -> Empfang wird ausgel�st
    I2C_Wait();
    // status 0x58
    data = I20DAT;                  // read data
    I20CONCLR = 1<<SI;              // interrupt l�schen
    I2C_Stop();                     // stop condition

    return data;
}

// Liest aus mehreren Registern
void I2C_ReadMultiRegisters(unsigned char SlaveAddress, unsigned char RegisterAddress, unsigned char n, unsigned char *r) {
    // RegisterAddress = start address -> auto incrementing register address after read
    //status_LED_toggl(1);
    char i;

	I2C_Start();                       // start condition
	I2C_Wait();                        // wait for ACK
	I20DAT = SlaveAddress << 1;		   // slaveaddress with 0 for write
	I20CONCLR = 1 << STA;              // reset start -> startflag l�schen
    I20CONCLR = 1 << SI;                 // interrupt flag l�schen
	I2C_Wait();                        // wait for ACK

	I20DAT = RegisterAddress;		   // Send register address
	I20CONCLR = 1 << SI;
	I2C_Wait();

	I2C_RepeatedStart();
	I20CONCLR = 1 << SI;
    I2C_Wait();

	I20DAT = (SlaveAddress << 1) | 0x01; // slaveaddress with 1 for read
	I20CONCLR = 1 << STA;                // reset start -> startflag l�schen
    I20CONCLR = 1 << SI;                   // interrupt flag l�schen
	I2C_Wait();

	I20CONSET = 1 << AA;                // ACK ausgeben

	for(i = 0; i < n-1; i++)
	{
	    I20CONCLR = 1 << SI;
	    I2C_Wait();
	    *r = I20DAT;
	    r++;                            //  -> bis zum 5. Element
	}

	I20CONCLR = 1 << AA;
	I20CONCLR = 1<<SI;
	I2C_Wait();
	*r = I20DAT;                        // 6. Element lesen

    I20CONCLR = 1<<SI;
	I2C_Stop();
}
