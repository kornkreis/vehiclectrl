 /*
 * spi.c
 *
 *  Created on: 22.11.2014
 *      Author: Hütter Stefan
 */
#include "spi.h"
#include "defines.h"
#include "LPC214x.h"
void spi_init(void)
{
	PINSEL1 &=~(0x000000A8);
	PINSEL1 |=  0x000000A8;
	PCONP   |=  0x400;
	SSPCPSR = 0x02; //prescaler
	SSPCR0 = 0x02c7; ///8 bit, SPI mode, CPHA = 1, CPOL = 1, SCR + 1 = 59 + 1 = 60;
	SSPCR1 = 0x02; //enable SSP modul, master mode
}
