/*
 * sc16is7xx.c
 *
 *  Created on: 21.06.2016
 *      Author: H�tter Brothers
 */

#include "sc16is7xx.h"
#include "iic.h"
#include "timer.h"
#include <stdlib.h>


// -- THIS LIBARY WAS STOLEN FROM https://www.ccsinfo.com/forum/viewtopic.php?t=36523 ---
// But it was completely changed because I need other functions of sc16is7xx

/* sc16is7xx_init()|sc16is7xx.c
*
*  Parameter:		---
*
*  Return:			0 - if no error and sc16is7xx is online
*                   either iic error code or 0xFF if device is not online
*
*  Description:     initialisizes the cannel A from the sc16is7xx
*
*/
unsigned char sc16is7xx_init (void){

    // Channel A Setups
    //Prescaler in MCR defaults on MCU reset to the value of 1
    unsigned char answer = 0;

    answer = sc16is7xx_write_register(LCR, CHANA, 0x80); // 0x80 to program baud rate divisor
    if (answer != IIC_ERROR_NO)
        return answer;
    answer = sc16is7xx_write_register(DLL, CHANA, 0x0C); // for X1=1,8432Mhz  //0x18); // 0x18=9600K, 0x06 =38,42K with X1=3.6864MHz
    if (answer != IIC_ERROR_NO)
        return answer;
    answer = sc16is7xx_write_register(DLH, CHANA, 0x00); //
    if (answer != IIC_ERROR_NO)
        return answer;
    //
    answer = sc16is7xx_write_register(LCR, CHANA, 0xBF); // access EFR register
    if (answer != IIC_ERROR_NO)
        return answer;
    answer = sc16is7xx_write_register(EFR, CHANA, 0x10); // enable enhanced registers
    if (answer != IIC_ERROR_NO)
        return answer;
    //
    answer = sc16is7xx_write_register(LCR, CHANA, 0x03); // 8 data bits, 1 stop bit, no parity
    if (answer != IIC_ERROR_NO)
        return answer;

    answer = sc16is7xx_write_register(FCR, CHANA, 0x06); // reset TXFIFO, reset RXFIFO, non FIFO mode
    if (answer != IIC_ERROR_NO)
        return answer;

    answer = sc16is7xx_write_register(FCR, CHANA, 0x01); // enable FIFO mode
    if (answer != IIC_ERROR_NO)
        return answer;

    if (sc16is7xx_is_online())
        return IIC_ERROR_NO;
    else
        return 0xFF; //only that function != 0
}

/* sc16is7xx_read_register()|sc16is7xx.c
*
*  Parameter:		RegAddr --> the address of the register
*                   CHAN --> the channel
*
*  Return:			either the value of the register or iic error code - which is normally 22
*                   this issue should be changed - either no error code or data over pointer
*
*  Description:     reads a char from a register
*
*/
unsigned char sc16is7xx_read_register(int RegAddr,unsigned char CHAN){
    // returns byte read from the UART register
    unsigned char data;
    unsigned char command[1];
    unsigned char iicAnswer;

    // rtfm
    command[0] = (RegAddr << 3) | (CHAN << 1);

    iicAnswer = iic_write(SC16IS7xx_ADDRESS,command, 1);  // write cycle
    if(iicAnswer != IIC_ERROR_NO)
        return iicAnswer;

    iicAnswer = iic_read(SC16IS7xx_ADDRESS,&data,1);
    if (iicAnswer != IIC_ERROR_NO)
        return iicAnswer;

    return data;
}

/* sc16is7xx_write_register()|sc16is7xx.c
*
*  Parameter:		RegAddr --> the register address
*                   CHAN    --> the cannel
*                   data    --> data which should be written
*
*
*  Return:			iic error code
*
*  Description:     writes a char in a register
*
*/
unsigned char sc16is7xx_write_register(int RegAddr, unsigned char CHAN, unsigned char data){ // sends data byte to selected UART register
    unsigned char command[2];

    command[0] = (RegAddr << 3) | (CHAN << 1);
    command[1] = data;

    return iic_write(SC16IS7xx_ADDRESS,command,2); // write cycle
}

/* sc16is7xx_is_online()|sc16is7xx.c
*
*  Parameter:		---
*
*  Return:			1 --> sc169s7xx is online
*                   0 --> is offline
*
*  Description:     writes on user register (SPR) a char and reads it - if it is the same as before - chip is online.
*
*/
unsigned char sc16is7xx_is_online(){
    unsigned char answer;
    unsigned char testChar = 0xAA;
    #define OFFLINE 0
    #define ONLINE 1

    answer = sc16is7xx_write_register(SPR,CHANA, testChar);
    if (answer != IIC_ERROR_NO)
        return OFFLINE;

    answer = sc16is7xx_read_register(SPR,CHANA);
    if (answer != testChar)
        return ONLINE;
    else
        return OFFLINE;
}

/* sc16is7xx_is_data_in_receiver()|sc16is7xx.c
*
*  Parameter:		CHAN --> the channel
*
*  Return:			1 --> data is waiting in RHR
*                   0 --> no data is waiting
*
*  Description:     self describing
*
*/
unsigned char sc16is7xx_is_data_in_receiver(unsigned char CHAN){
    // is data waiting??
    if (sc16is7xx_read_register(LSR, CHAN) & 0x01) // data present in receiver FIFO
        return 1;
    else
        return 0;
}

/* sc16is7xx_set_gpio_dir()|sc16is7xx.c
*
*  Parameter:		bits --> the bits where 0 is input and 1 output
*
*  Return:			iic error code
*
*  Description:     self describing
*
*/
unsigned char sc16is7xx_set_gpio_dir(unsigned char bits){ // Set Direction on UART GPIO Port pins GPIO0 to GPIO7
     // 0=input   1=Output
     unsigned char answer;

     sc16is7xx_write_register(IOControl, 0, 0x03); // Set the IOControl Register to GPIO Control
     answer = sc16is7xx_write_register(IODir,0, bits); // output the control bits to the IO Direction Register
     return answer;
}

/* sc16is7xx_read_gpio()|sc16is7xx.c
*
*  Parameter:		bits --> the bits where 0 is input and 1 output
*
*  Return:			iic error code
*
*  Description:     self describing
*
*/
unsigned char sc16is7xx_read_gpio(){ // Read UART GPIO Port
     unsigned char data = 0x00;

     data=sc16is7xx_read_register(IOState,0); // get GPIO Bits state 0-7

     return data;
}

/* sc16is7xx_set_gpio()|sc16is7xx.c
*
*  Parameter:		data --> the data for the register --> for bits: bit=1 output high otherwise low
*
*  Return:			---
*
*  Description:     self describing
*
*/
void sc16is7xx_set_gpio(unsigned char data){ // Load UART GPIO Port
     sc16is7xx_write_register(IOState,0, data); // set GPIO Output pins state 0-7
}

/* sc16is7xx_Read_String()|sc16is7xx.c
*
*  Parameter:		---
*
*  Return:			iic error code
*
*  Description:     self describing
*
*/
void sc16is7xx_Read_String(unsigned char CHAN, char *str){
    unsigned char length = 0;
    unsigned int maxTry = 0;
    //char *str = malloc(32);

    while(sc16is7xx_is_data_in_receiver(CHAN)){
        str[length] = sc16is7xx_read_register(RHR,CHAN);
        // wait for end sign or next sign --> so no ACK Return and nothing in receiver
        while((str[length] != '\0') && ( !sc16is7xx_is_data_in_receiver(CHAN) ) ){
            maxTry++;
            if (maxTry > 256)
                str = NULL;
        }
        length++;
    }
    //return str;
}

/* sc16is7xx_send_char_to_uart()|sc16is7xx.c
*
*  Parameter:		CHAN --> channel
*                   sign --> the character
*
*  Return:			---
*
*  Description:     self describing
*
*/
void sc16is7xx_send_char_to_uart(unsigned char CHAN, unsigned char sign){

    // wait until buffer is free
    while(sc16is7xx_read_register(TXLVL,CHAN) == 0){
        timer_4_warte_start();
        while(!timer_4_warte(10));
    }
    sc16is7xx_write_register(THR,CHAN,sign);
}

/* sc16is7xx_Send_Str()|sc16is7xx.c
*
*  Parameter:		bits --> the bits where 0 is input and 1 output
*
*  Return:			error or no error
*
*  Description:     ATTENTION!!!! not tested!!!
*
*/
unsigned char sc16is7xx_Send_Str(char *signs, unsigned char len){

    if (len > SC16IS7xx_TX_BUFFER)
        return 255;

    // wait until buffer is free
    while(sc16is7xx_read_register(TXLVL,CHANA) < len){
        timer_4_warte_start();
        while(!timer_4_warte(10));
    }

    unsigned char i;
    for(i = 0; i < len; i++)
        sc16is7xx_write_register(THR,CHANA,signs[i]);

    return 0;
}

