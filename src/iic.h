 /*
 * iic.h
 *
 *  Created on: 17.01.2014
 *      Author: H�tter Stefan
 */

#ifndef IIC_H_
#define IIC_H_

#define IIC_TIMEOUT_TIME 10000

#define I2EN	6
#define STA		5
#define STO		4
#define SI		3
#define AA		2

#define AAC		2
#define SIC		3
#define STAC	5
#define I2ENC	6


#define IIC_STAT_STARTCON_TRANS         0x08
#define IIC_STAT_SLA_W_TRANS_ACK_REC    0x18
#define IIC_STAT_DATA_TRANS_ACK_REC     0x28

#define IIC_STAT_SLA_R_TRANS_ACK_REC    0x40
#define IIC_STAT_DATA_REC_ACK_REC       0x50
#define IIC_STAT_DATA_REC_NO_ACK        0x58

#define IIC_ERROR_NO				0
#define IIC_ERROR_NO_STARTCON_SENT	1
#define IIC_ERROR_ADDR_NO_ACK		2
#define IIC_ERROR_NO_ACK_AFTER_SENT	3

void iic_init(void);
unsigned char iic_write(unsigned char addr, unsigned char *data, unsigned char data_cnt);
unsigned char iic_read(unsigned char addr, unsigned char *data, unsigned char data_cnt);
void iic_stop(void);
#endif //IIC_H_
