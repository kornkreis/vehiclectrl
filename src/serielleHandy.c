/*
 * serielleHandy.c
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#include "serielleHandy.h"
#include "serielle.h"
#include "main.h"
#include "LPC214x.h"
#include "status_led.h"
#include "timer.h"

unsigned char serielleHandy_rec_len=0;
unsigned char serielleHandy_rec_len_count=0;
char serielleHandy_rec_buffer[256];
unsigned char serielleHandy_rec_state=0;
unsigned char serielleHandy_rec_index=0;
unsigned char serielleHandy_rec_cksum=0;
unsigned char serielleHandy_rec_error=0;
unsigned char serielleHandy_rec_ready=0;
char serielleHandy_text[1024];
char serielleHandy_stringToProg[18]={'s','l','c',':','p','r','o','g',':','p','r','o','g','r','a','m',0x0d};
unsigned char serielleHandy_stringToProgChecked=0;

void serielleHandy_init(){
	/**
	 * Configure UART0
	 */
	PINSEL0|= (1<<16)|(1<<18); //P0.8 und P0.9 m�ssen auf RXD und TXD gestellt werden bit16:1 bit17:0 bit18:1 bit19:0
	PINSEL0&= ~((1<<17)|(1<<19)); //b0101

	IODIR0|=(1<<8); //P0.0 als OUTPUT and P0.1 als INPUT
	IODIR0&= ~(1<<9); //b01



	U1LCR = 0x83; 									// 8N1, divisor latch enabled
	U1DLL = 0x18; 									// 0x18 bei 38400 Baud; 62 f�r 9600 Baud bei 15 MHz  HAB: 38400 Baud
	U1DLM = 0x00;
	U1FDR = 0x00000010; 							// nicht �ber Bruch
	U1LCR = 0x03; 									// 8N1, divisor latch disabled
	U1FCR = 0x07; 									// Reset FIfO
	U1TER = 0x80; 									// Transmit enable
	U1IER = 0x01; 									// UART0 Interrupt enable Rx available, THR empty : 0x03
	VICVectAddr14 = (unsigned long) serielleHandy_isr; 		// UART0 Interrupt -> Vector 14
	VICVectCntl14 = 0x20 | 7; 						// UART0 Interrupt -> IRQ Slot 14
	VICIntEnable |= 1 << 7;							// Enable UART0 Interrupt

	serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_START;
}

void __attribute__ ((interrupt("IRQ"))) serielleHandy_isr(){
    status_LED_set(1);
    // Receive Data available
	if ((U1IIR & 0x0e) == 0x04){
		serielleHandy_write_in_buffer(U1RBR & 0xfF);
		status_LED_toggl(1);
	}
	status_LED_clear(1);
	VICVectAddr = 0;
}

char serielleHandy_putch(char nch){
	while (!(U1LSR & 0x40));
	U1THR = nch;
	return nch;
}

char serielleHandy_puttext( char *textp){ // Ausgabe eines Strings
	unsigned char byte;
	char cksm = 0;
    status_LED_set(1);

	while (1)
	{
		byte = *textp++; // Zeichen holen
		if (byte){
            status_LED_toggl(1);
			cksm += serielleHandy_putch(byte);
        }
		else
		{
			break;
		}
	}
    status_LED_clear(1);
	return cksm;
}

void serielleHandy_send(){
    serielleHandy_puttext(serielleHandy_text);
}

/* serielleHandy_write_in_buffer()|serielle.c
*
*  Parameter:		unsigned char c
*
*  Return:			---
*
*  Beschreibung:	Trennt die Daten vom Header und schreibt diese in serielleHandy_rec_buffer[]
*
*/
void serielleHandy_write_in_buffer(unsigned char c){
	if(c==serielleHandy_stringToProg[serielleHandy_stringToProgChecked])
		serielleHandy_stringToProgChecked++;
	else
		serielleHandy_stringToProgChecked=0;
	if(serielleHandy_stringToProgChecked==17)
	{
		interprog = 0x76543210;
		//jump = 0;
		//sprintf(serielle_text, "App: jump to %X\r\n", jump);
		//serielle_send();
		WDTC = 0x100; //Zeit stimmt nicht gibt 4,4739 sec Timeout
		timer_feed_watchdog(); // neu kurze Zeit �bernehmen
		//serielle_chk_send();

	}
switch(serielleHandy_rec_state)
	{
		case SERIELLEHANDY_STATE_WAIT_FOR_START:
			if(c==SERIELLEHANDY_START_SIGN_RX)
			{
				serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_LEN;
				serielleHandy_rec_ready=SERIELLEHANDY_REC_BUSY;
				serielleHandy_rec_index=0;
			}
			break;

		case SERIELLEHANDY_STATE_WAIT_FOR_LEN:
			serielleHandy_rec_len_count=serielleHandy_rec_len = c;
			serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_DATA;
			serielleHandy_rec_cksum=0;
			break;
		case SERIELLEHANDY_STATE_WAIT_FOR_DATA:
			serielleHandy_rec_cksum+=c;
			serielleHandy_rec_buffer[serielleHandy_rec_index]=c;
			serielleHandy_rec_index++;
			serielleHandy_rec_len_count--;
			if(serielleHandy_rec_len_count==0)
				serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_CKSUM;
			break;
		case SERIELLEHANDY_STATE_WAIT_FOR_CKSUM:
			if(serielleHandy_rec_cksum==c)
				serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_ACK;
			else
			{
				serielleHandy_rec_error=SERIELLEHANDY_REC_ERROR_CKSUM;
				serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_ACK;
			}
			break;
		case SERIELLEHANDY_STATE_WAIT_FOR_ACK:
			if(c=='\r')
				serielleHandy_rec_ready=SERIELLEHANDY_REC_READY;
			serielleHandy_rec_state=SERIELLEHANDY_STATE_WAIT_FOR_START;
			break;
	}
}

/* serielleHandy_write_to_uart()|serielle.c
*
*  Parameter:		char *buffer	| pointer zum Buffer des Datenblocks
*					unsignded char len		| Anzahl der Bytes die im Datenblock sind
*
*  Return:			---
*
*  Beschreibung:	Trennt die Daten vom Header und schreibt diese in serielleHandy_rec_buffer[]
*
*/
void serielleHandy_write_to_uart(char *buffer, unsigned char len){
	unsigned char i;
	status_LED_set(1);
	for(i=0;i<len;i++){
		serielleHandy_putch(buffer[i]);
		status_LED_toggl(1);
	}
    status_LED_clear(1);
}
