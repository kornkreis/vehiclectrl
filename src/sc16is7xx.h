/*
 * sc16is7xx.h
 *
 *  Created on: 21.06.2016
 *      Author: H�tter Brothers
 */

#ifndef SC16IS7xx_H_
#define SC16IS7xx_H_

#define RHR          0x00 //  Recv Holding Register is 0x00 in READ Mode
#define THR          0x00 //  Xmit Holding Register is 0x00 in WRITE Mode
//
#define IER          0x01  // Interrupt Enable Register
//
#define IIR          0x02  // Interrupt Identification Register in READ Mode
#define FCR          0x02  // FIFO Control Register in WRITE Mode
//
#define LCR          0x03  // Line Control Register
#define MCR          0x04  // Modem Control Register
#define LSR          0x05  // Line status Register
#define MSR          0x06  // Modem Status Register
#define SPR          0x07  // ScratchPad Register
#define TCR          0x06  // Transmission Control Register
#define TLR          0x07  // Trigger Level Register
#define TXLVL        0x08  // Xmit FIFO Level Register
#define RXLVL        0x09  // Recv FIFO Level Register
#define IODir        0x0A  // I/O P:ins Direction Register
#define IOState      0x0B  // I/O Pins State Register
#define IOIntEna     0x0C  // I/O Interrupt Enable Register
#define IOControl    0x0E  // I/O Pins Control Register
#define EFCR         0x0F  // Extra Features Control Register
//
#define DLL          0x00  // Divisor Latch LSB  0x00
#define DLH          0x01  // Divisor Latch MSB  0x01
//
#define EFR          0x02  // Enhanced Function Register
//
#define I2CWRITE     0x00
#define I2CREAD      0x01

#define CHANA      0
#define CHANB      1

#define SC16IS7xx_ADDRESS   0x4D //A0 and A1 on VSS
#define SC16IS7xx_TX_BUFFER 64


unsigned char sc16is7xx_read_register(int RegAddr, unsigned char CHAN);
unsigned char sc16is7xx_write_register(int RegAddr, unsigned char CHAN, unsigned char Data);
void sc16is7xx_send_char_to_uart(unsigned char CHAN, unsigned char sign);
unsigned char sc16is7xx_init (void);
unsigned char sc16is7xx_is_online();
unsigned char sc16is7xx_is_data_in_receiver(unsigned char CHAN);
unsigned char sc16is7xx_set_gpio_dir(unsigned char bits);
unsigned char sc16is7xx_read_gpio();
void sc16is7xx_set_gpio(unsigned char data);
void sc16is7xx_Read_String(unsigned char CHAN, char *str);
unsigned char sc16is7xx_Send_Str(char *signs, unsigned char len);

#endif //SC16IS7xx_H_

