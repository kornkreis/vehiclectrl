/*
* externTemp.c
*
*  Created on: 22.11.2014
*      Author: H�tter Stefan
*/
#include "externTemp.h"
#include "defines.h"
#include "LPC214x.h"

/*
*  externTemp_init()|externTemp.c
*  
*  Parameter:		---
* 
*  Return:			---
*
*  Beschreibung:	Initialisiert den Intenern ADC zur Messung der Temperatur in der Batterie
*
*/
void externTemp_init(void)
{
	
	
	PINSEL1&= ~((0<<18)|(1<<19));
	PINSEL1|=  ((1<<18)|(0<<19));
	PCONP  |=   (1<<12);
	
}

/*
*  externTemp_read_ADC()|externTemp.c
*  
*  Parameter:		---
* 
*  Return:			int --> Wert des ADCs
*
*  Beschreibung:	Liest den internen ADC aus
*
*/
int externTemp_read_ADC(void)
{
	unsigned long dummy;
	dummy=(AD0GDR & (1<<31));
	AD0CR =   (1<<4)	//Select ADC0.4
			| (19 << 8) //CLKDIV auf 3
			| (0 <<17) //CLKS auf 0 -->10bit
			| (1 <<21); //PN auf 1 (converter is operational)
	dummy=(AD0GDR & (1<<31));
	// Stop
	AD0CR &= ~(7<<24); // 0b111  
	// Start
	AD0CR |=  (1<<24);
	
	while (!(AD0GDR & (1<<31)));
	// Return the processed results
	return ((AD0GDR & 0xFFC0)>>6);
}