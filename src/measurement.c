/*
* measurement.c
*
*  Created on: 02.12.2014
*      Author: Hütter Stefan
*/
#include "defines.h"
#include "hardware.h"
#include "measurement.h"
#include "adcIntern.h"
#include "defines.h"
#include "onBoardTemp.h"
#include "externTemp.h"
#include "memory.h"
#include "serielle.h"
#include "rtc.h"
#include "spi.h"
#include "input.h"
#include "adcExtern.h"
#include "communicate.h"
#include "LPC214x.h"
#include "param.h"

#include "timer.h"
#include "status.h"
#include <math.h>
#include <stdio.h>

double measurement_sum_U=0;
double measurement_sum_I=0;
double measurement_average_U=0.0;
double measurement_average_I=0.0;
double measurement_average_P=0.0;

unsigned char measurement_average_count=0;

/* measurement_init()|measurement.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert alle Register für die Messungen.
*					- on Board Temperatur
*					- Füllstand
*					- adcIntern
*
*/
void measurement_init(void){
	spi_init();
	onBoardTemp_init();
	input_init();
	adcIntern_init();
	adcExtern_init();
	externTemp_init();
}

/* measurement_getU()|measurement.c
*
*  Parameter:		---
*
*  Return:			double --> Messwert der Spannung in V!
*
*  Beschreibung:	Misst den ADC Wert und liefert die Spannung
*/
double measurement_getU(void){
	double val;
	int valADC;
	int i;

	valADC=0;
	for(i=0;i<16;i++)
	{
		valADC+= adcIntern_read_ADC1_2();

	}
	val=valADC;
	val/=16;
	val*= 3.300;
	val/= 1024;//val in mV
	val*=56;//Verstärkung

    val *= (float)(memory_calib_batt_u_k)/100000;
	val += (float)(memory_calib_batt_u_d)/100000;

	return val;
}

/* measurement_getPressure()|measurement.c
*
*  Parameter:		---
*
*  Return:			double --> Messwert des Drucks in mbar!
*
*  Beschreibung:	Misst den ADC Wert und liefert den Druck
*/
double measurement_getPressure(void){
	double val;
	int valADC;
	int i;

	valADC=0;
	for(i=0;i<16;i++)
	{
		//valADC+= adcIntern_read_ADC1_2();
		valADC+= adcIntern_read_ADC0_4();
	}
	val=valADC;
	val/=16;
	val*=5;
	val/= 1024;//val in mV

    val *= (float)(memory_calib_batt_u_k)/100000;
	val += (float)(memory_calib_batt_u_d)/100000;

    val *= (250/5);
	return val;
}

/* measurement_getWeight()|measurement.c
*
*  Parameter:		---
*
*  Return:			double --> kilo
*
*  Beschreibung:	takes the pressure and makes of it a weight
*/
double measurement_getWeight(){
    double val;

    val = measurement_getPressure() - memory_calib_weight0;
    val *= memory_calib_weight_faktor;

    return val;
}

/* measurement_calib_weight0()|measurement.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	calibs the weight0
*/
void measurement_calib_weight0(void){
    memory_calib_weight0 = measurement_getPressure();
    memory_calib_weight_faktor = 1;
}

/* measurement_calib_weight_faktor()|measurement.c
*
*  Parameter:		weight - the kilos which are on pressure
*
*  Return:			---
*
*  Beschreibung:	takes the pressure and makes of it a weight
*/
void measurement_calib_weight_faktor(double weight){
    double val;

    val = measurement_getPressure() - memory_calib_weight0;
    memory_calib_weight_faktor = (weight/100)/val;
}

unsigned char measurement_getFuellstand(void){
	return input_getCH(0);
}

/* measurement_getI()|measurement.c
*
*  Parameter:		---
*
*  Return:          charge rate in Ampere
*
*  Beschreibung:	Vout = Vref + 1,25*Ip/Ipn
*					Ip..... gemessener Strom
*					Ipn.... Nennstrom des Fühlers = 400A
*					Vref... Referenzspannung des Fühlers --> 5V/2 = 2,5V
*					Ip = (Vout-Vref)*Ipn/1,25 = (Vout-2,5)*400/1,25
*					abschließend muss der Wert noch invertiert werden,
*					da der Sensor verkehrt eingebaut wird.
*/
float measurement_getI(void){
	float val;
	int valADC;
	int i;

	valADC=0;
	for(i=0;i<MSM_AVERAGE_CTRL;i++)
		valADC += adcExtern_read(ADCEXTERN_CH_0);

	valADC/=MSM_AVERAGE_CTRL;  //16
	//valADC*=HW_ADCEXT_REF_CH_0;  //2500
	valADC*=3300;  //2500
	//valADC/=HW_ADCEXT_MAXVAL_CH_1;// 4096
    //valADC/=HW_ADCEXT_MAXVAL_CH_0;// 65535 valADC is now in mV
    valADC/=4096;

    val=(float)(valADC)/1000;
	val-=2.5;//HW_ADCEXT_OFFSET_CH_1;  // 2.5
	val*=400;//HW_ADCEXT_IPN_CH_1;  // 400
	val/=1.25;

	// calibration
	val *= (float)(memory_calib_batt_i_k)/100000;
	val -= (float)(memory_calib_batt_i_d)/100000;
    val *= (-1); //reverse current direction

	return val;
}

double measurement_getAverageU(void){
    return measurement_average_U;
}

double measurement_getAverageI(void){
    return measurement_average_I;
}

double measurement_getAverageP(void){
    return measurement_average_P;
}

/* measurement_getOnBoardTemp()|measurement.c
*
*  Parameter:		---
*
*  Return:			onboard Temp in 1/10 degree
*
*  Description:
*
*/
int measurement_getOnBoardTemp(void){
	return onBoardTemp_getTemp();
}

void measurement_meas_every_ms(void){
    //measurement_getU();
    //measurement_getI();
}

void measurement_buildSum(void){
    if(measurement_average_count<255){
        measurement_sum_U+=measurement_getU();
        measurement_sum_I+=measurement_getI();
        measurement_average_count++;
    }
}

void measurement_calcAverage(void){

    measurement_average_U=measurement_sum_U/measurement_average_count;
    measurement_average_I=measurement_sum_I/measurement_average_count;
    measurement_average_P=measurement_average_I*measurement_average_U;
    measurement_average_count=0;
    measurement_sum_U=0.0;
    measurement_sum_I=0.0;
}
