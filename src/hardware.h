/*
* hardware.h
*
*  Created on: 28.04.2015
*      Author: H�tter Wolfgang
*/
#ifndef HARDWARE_H_
#define HARDWARE_H_

#define HW_CHARGER_CTRL

#define HW_FT311_RESET_CLR      IOCLR0 = 1 << 23
#define HW_FT311_RESET_SET      IOSET0 = 1 << 23

#define HW_FT311_RESET_LONG     20 * 1000 //sec
#define HW_FT311_RESET_SHORT    2  * 1000 //sec

// pins on P0
#define HW_DREADY 15
// Pins on P1
#define HW_D_OUT_0  16
#define HW_D_OUT_1  17
#define HW_D_OUT_2  18

#define HW_EXT_IO_0 22  // GIPO on TWN4
#define HW_EXT_IO_1 23  // Reset on TWN4
#define HW_EXT_IO_2 24  // Powerdown on TWN4

#define HW_TWN4_RESET_CLR       IOCLR1 = 1 << HW_EXT_IO_1 // Active Low
#define HW_TWN4_RESET_SET       IOSET1 = 1 << HW_EXT_IO_1

#define HW_TWN4_POWER_OFF       IOCLR1 = 1 << HW_EXT_IO_2
#define HW_TWN4_POWER_ON        IOSET1 = 1 << HW_EXT_IO_2

#define HW_TWN4_GET_ID_SET      IOSET1 = 1 << HW_EXT_IO_0
#define HW_TWN4_GET_ID_CLR      IOCLR1 = 1 << HW_EXT_IO_0

#define HW_TWN4_ID_WAITS        (IOPIN0 & (1 << HW_DREADY))

#define HW_RELAIS_1_SET         IOSET1 = 1 << HW_D_OUT_0
#define HW_RELAIS_2_SET         IOSET1 = 1 << HW_D_OUT_1
#define HW_RELAIS_3_SET         IOSET1 = 1 << HW_D_OUT_3

#define HW_RELAIS_1_CLR         IOCLR1 = 1 << HW_D_OUT_0
#define HW_RELAIS_2_CLR         IOCLR1 = 1 << HW_D_OUT_1
#define HW_RELAIS_3_CLR         IOCLR1 = 1 << HW_D_OUT_3

//********* Hardware is Charger Controller *********
#ifdef HW_CHARGER_CTRL
#include "chargerCtrl.h"
#endif // HW_CHARGER_CTRL

//********* Hardware is Charger Controller *********
#ifdef HW_BATT_CTRL
#include "battCtrl.h"
#endif // HW_BATT_CTRL

//functions
void hardware_FT311Reset(int timeMS);
void hardware_TWN4_reset(int timeMS);
#endif //HARDWARE_H_


