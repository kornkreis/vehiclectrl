/*
 * serielle.c
 *
 *  Created on: 21.01.2015
 *      Author: Stefan H�tter
 */

#include "serielle.h"
#include "main.h"
#include "LPC214x.h"
#include "status_led.h"
#include "timer.h"

unsigned char serielle_rec_len=0;
unsigned char serielle_rec_len_count=0;
char serielle_rec_buffer[256];
unsigned char serielle_rec_state=0;
unsigned char serielle_rec_index=0;
unsigned char serielle_rec_cksum=0;
unsigned char serielle_rec_error=0;
unsigned char serielle_rec_ready=0;
char serielle_text[1024];

char serielle_stringToProg[18]={'s','l','c',':','p','r','o','g',':','p','r','o','g','r','a','m',0x0d};
unsigned char serielle_stringToProgChecked=0;

/*
*  serielle_init()|serielle.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert die Sereile schnittstelle
*
*/
void serielle_init(){
	/**
	* Configure UART0
	*/
	PINSEL0|= 0x00000005; //P0.0 und P0.1 m�ssen auf RXD und TXD gestellt werden
	PINSEL0&= 0xFFFFFFF5; //b0101
	IODIR0|=0x00000001; //P0.0 als OUTPUT and P0.1 als INPUT
	IODIR0&=0xFFFFFFFD; //b01
	U0LCR = 0x83; 									// 8N1, divisor latch enabled
	U0DLL = 0x18; 									// 0x18 bei 38400 Baud; 62 f�r 9600 Baud bei 15 MHz  HAB: 38400 Baud
	U0DLM = 0x00;
	U0FDR = 0x00000010; 							// nicht �ber Bruch
	U0LCR = 0x03; 									// 8N1, divisor latch disabled
	U0FCR = 0x07; 									// Reset FIfO
	U0TER = 0x80; 									// Transmit enable
	U0IER = 0x01; 									// UART0 Interrupt enable Rx available, THR empty : 0x03
	VICVectAddr15 = (unsigned long) serielle_isr; 		// UART0 Interrupt -> Vector 15
	VICVectCntl15 = 0x20 | 6; 						// UART0 Interrupt -> IRQ Slot 15
	VICIntEnable |= 1 << 6; 						// Enable UART0 Interrupt

	serielle_rec_state=SERIELLE_STATE_WAIT_FOR_START;
}

void __attribute__ ((interrupt("IRQ"))) serielle_isr(){
    status_LED_set(1);
     // Receive Data available
	if ((U0IIR & 0x0e) == 0x04){
		serielle_write_in_buffer(U0RBR & 0xfF);
		status_LED_toggl(1);
	}
    status_LED_clear(1);
	VICVectAddr = 0;
}

char serielle_putch(char nch){
	while (!(U0LSR & 0x40));
	U0THR = nch;
	return nch;
}

// Ausgabe eines Strings
char serielle_puttext( char *textp){
	unsigned char byte;
	char cksm = 0;
    status_LED_set(1);
	while (1)
	{
		byte = *textp++; // Zeichen holen
		if (byte){
			cksm += serielle_putch(byte);
			status_LED_toggl(1);
		}
		else
		{
			break;
		}
	}
    status_LED_clear(1);
	return cksm;
}

void serielle_send(){
	serielle_puttext(serielle_text);
}

/* serielle_write_in_buffer()|serielle.c
*
*  Parameter:		unsigned char c
*
*  Return:			---
*
*  Beschreibung:	Trennt die Daten vom Header und schreibt diese in serielle_rec_buffer[]
*
*/
void serielle_write_in_buffer(unsigned char c){
	if(c==serielle_stringToProg[serielle_stringToProgChecked])
		serielle_stringToProgChecked++;
	else
		serielle_stringToProgChecked=0;
	if(serielle_stringToProgChecked==17)
	{
		interprog = 0x76543210;
		//jump = 0;
		//sprintf(serielle_text, "App: jump to %X\r\n", jump);
		//serielle_send();
		WDTC = 0x100; //Zeit stimmt nicht gibt 4,4739 sec Timeout
		timer_feed_watchdog(); // neu kurze Zeit �bernehmen
		//serielle_chk_send();

	}
	switch(serielle_rec_state)
	{
		case SERIELLE_STATE_WAIT_FOR_START:
			if(c==SERIELLE_START_SIGN_RX)
			{
				serielle_rec_state=SERIELLE_STATE_WAIT_FOR_LEN;
				serielle_rec_ready=SERIELLE_REC_BUSY;
				serielle_rec_index=0;
			}
			break;

		case SERIELLE_STATE_WAIT_FOR_LEN:
			serielle_rec_len_count=serielle_rec_len = c;
			serielle_rec_state=SERIELLE_STATE_WAIT_FOR_DATA;
			serielle_rec_cksum=0;
			break;
		case SERIELLE_STATE_WAIT_FOR_DATA:
			serielle_rec_cksum+=c;
			serielle_rec_buffer[serielle_rec_index]=c;
			serielle_rec_index++;
			serielle_rec_len_count--;
			if(serielle_rec_len_count==0)
				serielle_rec_state=SERIELLE_STATE_WAIT_FOR_CKSUM;
			break;
		case SERIELLE_STATE_WAIT_FOR_CKSUM:
			if(serielle_rec_cksum==c)
				serielle_rec_state=SERIELLE_STATE_WAIT_FOR_ACK;
			else
			{
				serielle_rec_error=SERIELLE_REC_ERROR_CKSUM;
				serielle_rec_state=SERIELLE_STATE_WAIT_FOR_ACK;
			}
			break;
		case SERIELLE_STATE_WAIT_FOR_ACK:
			if(c=='\r')
				serielle_rec_ready=SERIELLE_REC_READY;
			serielle_rec_state=SERIELLE_STATE_WAIT_FOR_START;
			break;
	}
}

/* serielle_write_to_uart()|serielle.c
*
*  Parameter:		char *buffer	| pointer zum Buffer des Datenblocks
*					unsignded char len		| Anzahl der Bytes die im Datenblock sind
*
*  Return:			---
*
*  Beschreibung:	Trennt die Daten vom Header und schreibt diese in serielle_rec_buffer[]
*
*/
void serielle_write_to_uart(char *buffer, unsigned char len){
    status_LED_set(1);
	unsigned char i;
	for(i=0;i<len;i++){
        serielle_putch(buffer[i]);
        status_LED_toggl(1);
    }
    status_LED_clear(1);
}
