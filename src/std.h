/*
 * std.h
 *
 *  Created on: 10.12.2010
 *      Author: hab
 */

#ifndef STD_H_
#define STD_H_


#define NULL ((void *)0)
#define MAX_STRING_LENGTH 200

int strlen(char *s);


#endif /* STD_H_ */
