/*
 * memory.c
 *
 *  Created on: 21.01.2011
 *      Author: Stefan H�tter
 */

#include "LPC214x.h"
#include "flashExtern.h"
#include "memory.h"
#include "defines.h"
#include "serielle.h"
#include "param.h"
#include "stringHelper.h"
#include "timer.h"

//only for UART comm:
#include "serielle.h"
#include <stdio.h>
#include "cmd.h"
#include "twn4.h"
//**
#include <stdlib.h>
#include "string.h"

unsigned long memory_sn_no;
short memory_flash_version;
short memory_fw_version;
short memory_hw_version;

uint32_t memory_calib_batt_i_k;
uint32_t memory_calib_batt_u_k;
int32_t memory_calib_batt_i_d;
int32_t memory_calib_batt_u_d;
char memory_calib_batt_avg;

//firmware 0.03
uint32_t memory_movement_count_threshold=200;
double memory_x_threshold=0.05;
double memory_y_threshold=0.075;
double memory_z_threshold=0.100;
double memory_calib_weight0 = 0;
double memory_calib_weight_faktor = 1;

unsigned int memory_status_log_off_delay = 900;

unsigned long memory_data_version;

/* memory_init()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_init(void){
	flashExtern_init();
}

/* memory_paramSetToDefault()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_paramSetToDefault(void){
	memory_flash_version=FLASH_VERSION;

	memory_calib_batt_i_k = 100000;
    memory_calib_batt_u_k = 100000;
    memory_calib_batt_i_d = 0;
    memory_calib_batt_u_d = 0;
    memory_calib_batt_avg = 1;

    memory_movement_count_threshold=200;
    memory_x_threshold=0.05;
    memory_y_threshold=0.075;
    memory_z_threshold=0.100;

    memory_calib_weight0 = 0;
    memory_calib_weight_faktor = 1;

    memory_status_log_off_delay = 900;  
    // a story about this variable:
    // every 5 seconds accel_no_move_count is triggered
    // so divide it by 5 --> 900/5 = 180. Thats the seconds for the logoff delay
    // this is how I understand it at moment -> good luck with it, because the
    // accel_no_move_count is also a adjustable parameter :)
    // this whole thing is a astable multivibrator
}

/* memory_param_set_serialNr()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_param_set_serialNr(unsigned long serialNr){
    unsigned char page[256];

	page[0]=(serialNr>>24) % 0x100;
	page[1]=(serialNr>>16) % 0x100;
	page[2]=(serialNr>>8) % 0x100;
	page[3]=serialNr % 0x100;

	flashExtern_writeEnable(0);
	flashExtern_pageWrite(0,0,page,4);
}

/* memory_paramSaveAllParams()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_paramSaveAllParams(void){
    unsigned char page[256];
    //char answerString[256];
    unsigned int uiHelp;
    int iHelp;
    char *cHelp;
    int i;

    page[0]=(memory_sn_no>>24) % 0x100;
    page[1]=(memory_sn_no>>16) % 0x100;
    page[2]=(memory_sn_no>>8) % 0x100;
    page[3]=memory_sn_no % 0x100;

    page[4]=(memory_flash_version>>8) % 0x100;
    page[5]=memory_flash_version % 0x100;

    page[6]=(memory_hw_version>>8) % 0x100;
    page[7]=memory_hw_version % 0x100;

    uiHelp=memory_calib_batt_i_k;
    page[8]=(uiHelp>>24) % 0x100;
    page[9]=(uiHelp>>16) % 0x100;
    page[10]=(uiHelp>>8) % 0x100;
    page[11]=uiHelp % 0x100;

    uiHelp=memory_calib_batt_u_k;
    page[12]=(uiHelp>>24) % 0x100;
    page[13]=(uiHelp>>16) % 0x100;
    page[14]=(uiHelp>>8) % 0x100;
    page[15]=uiHelp % 0x100;

    iHelp=memory_calib_batt_i_d;
    page[16]=(iHelp>>24) % 0x100;
    page[17]=(iHelp>>16) % 0x100;
    page[18]=(iHelp>>8) % 0x100;
    page[19]=iHelp % 0x100;

    iHelp=memory_calib_batt_u_d;
    page[20]=(iHelp>>24) % 0x100;
    page[21]=(iHelp>>16) % 0x100;
    page[22]=(iHelp>>8) % 0x100;
    page[23]=iHelp % 0x100;

    page[24] = memory_calib_batt_avg;

    page[25]=(memory_data_version>>24) % 0x100;
    page[26]=(memory_data_version>>16) % 0x100;
    page[27]=(memory_data_version>>8) % 0x100;
    page[28]=memory_data_version % 0x100;

    cHelp = (char*)&memory_x_threshold;
    for(i = 0; i < 4; i++)
        page[29 + i] = cHelp[i]; //Until 32

    cHelp = (char*)&memory_y_threshold;
    for(i = 0; i < 4; i++)
        page[33 + i] = cHelp[i]; //Until 36

    cHelp = (char*)&memory_z_threshold;
    for(i = 0; i < 4; i++)
        page[37 + i] = cHelp[i]; //Until 40

    uiHelp=memory_movement_count_threshold;
    page[41]=(uiHelp>>24) % 0x100;
    page[42]=(uiHelp>>16) % 0x100;
    page[43]=(uiHelp>>8) % 0x100;
    page[44]=uiHelp % 0x100;

	uiHelp=memory_calib_weight0;
    page[45]=(uiHelp>>24) % 0x100;
    page[46]=(uiHelp>>16) % 0x100;
    page[47]=(uiHelp>>8) % 0x100;
    page[48]=uiHelp % 0x100;

    uiHelp=memory_calib_weight_faktor;
    page[49]=(uiHelp>>24) % 0x100;
    page[50]=(uiHelp>>16) % 0x100;
    page[51]=(uiHelp>>8) % 0x100;
    page[52]=uiHelp % 0x100;

    uiHelp=memory_status_log_off_delay;
    page[53]=(uiHelp>>8) % 0x100;
    page[54]=uiHelp % 0x100;

    flashExtern_writeEnable(0);
    flashExtern_pageWrite(0,0,page,55);
    flashExtern_writeDisable(0);

    /*sprintf(answerString,"ans: saved page for srnr: %lu\n",(unsigned long)((page[0]<<24)+(page[1]<<16)+(page[2]<<8)+page[3]));
    cmd_answer(answerString,CMD_DEVICE_PC);*/
}

/* memory_paramLoadAllParams()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_paramLoadAllParams(void){
    unsigned char page[256];
    char *cHelp;
    int i;

    flashExtern_readBytes(0, 0, page, 196);

    memory_sn_no=(page[0]<<24)+(page[1]<<16)+(page[2]<<8)+page[3];
    memory_flash_version=(page[4]<<8)+page[5];
    memory_hw_version=(page[6]<<8)+page[7];

    if(memory_flash_version==0x0002){//version 0.02
        memory_calib_batt_i_k = (page[43]<<24) + (page[44]<<16) + (page[45]<<8) + page[46];
        memory_calib_batt_u_k = (page[47]<<24) + (page[48]<<16) + (page[49]<<8) + page[50];
        memory_calib_batt_i_d = (page[51]<<24) + (page[52]<<16) + (page[53]<<8) + page[54];
        memory_calib_batt_u_d = (page[55]<<24) + (page[56]<<16) + (page[57]<<8) + page[58];
        memory_calib_batt_avg = page[192];
        memory_data_version=(page[193]<<24)+(page[194]<<16)+(page[195]<<8)+page[196];
        memory_movement_count_threshold=200;
        memory_x_threshold=0.05;
        memory_y_threshold=0.075;
        memory_z_threshold=0.100;
        memory_calib_weight0 = 0;
        memory_calib_weight_faktor = 1;
    }else if(memory_flash_version==0x0003){
        memory_calib_batt_i_k = (page[8]<<24) + (page[9]<<16) + (page[10]<<8) + page[11];
        memory_calib_batt_u_k = (page[12]<<24) + (page[13]<<16) + (page[14]<<8) + page[15];
        memory_calib_batt_i_d = (page[16]<<24) + (page[17]<<16) + (page[18]<<8) + page[19];
        memory_calib_batt_u_d = (page[20]<<24) + (page[21]<<16) + (page[22]<<8) + page[23];
        memory_calib_batt_avg = page[24];
        memory_data_version=(page[25]<<24)+(page[26]<<16)+(page[27]<<8)+page[28];
        memory_movement_count_threshold=200;
        memory_x_threshold=0.05;
        memory_y_threshold=0.075;
        memory_z_threshold=0.100;
        memory_calib_weight0 = 0;
        memory_calib_weight_faktor = 1;
    }else if(memory_flash_version==0x0006){
        memory_calib_batt_i_k = (page[8]<<24) + (page[9]<<16) + (page[10]<<8) + page[11];
        memory_calib_batt_u_k = (page[12]<<24) + (page[13]<<16) + (page[14]<<8) + page[15];
        memory_calib_batt_i_d = (page[16]<<24) + (page[17]<<16) + (page[18]<<8) + page[19];
        memory_calib_batt_u_d = (page[20]<<24) + (page[21]<<16) + (page[22]<<8) + page[23];
        memory_calib_batt_avg = page[24];
        memory_data_version=(page[25]<<24)+(page[26]<<16)+(page[27]<<8)+page[28];

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[29 + i]; // until 32
        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[33 + i]; // until 36

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[37 + i]; // until 40

        memory_movement_count_threshold=(page[41]<<24) + (page[42]<<16) + (page[43]<<8) + page[44];

        memory_calib_weight0 = 0;
        memory_calib_weight_faktor = 1;
    }else if(memory_flash_version==0x0007){
        memory_calib_batt_i_k = (page[8]<<24) + (page[9]<<16) + (page[10]<<8) + page[11];
        memory_calib_batt_u_k = (page[12]<<24) + (page[13]<<16) + (page[14]<<8) + page[15];
        memory_calib_batt_i_d = (page[16]<<24) + (page[17]<<16) + (page[18]<<8) + page[19];
        memory_calib_batt_u_d = (page[20]<<24) + (page[21]<<16) + (page[22]<<8) + page[23];
        memory_calib_batt_avg = page[24];
        memory_data_version=(page[25]<<24)+(page[26]<<16)+(page[27]<<8)+page[28];

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[29 + i]; // until 32
        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[33 + i]; // until 36

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[37 + i]; // until 40

        memory_movement_count_threshold=(page[41]<<24) + (page[42]<<16) + (page[43]<<8) + page[44];

        memory_calib_weight0=(double)((page[45]<<24) + (page[46]<<16) + (page[47]<<8) + page[48]);
        memory_calib_weight_faktor =(double)((page[49]<<24) + (page[50]<<16) + (page[51]<<8) + page[52]);
    }else{
        memory_calib_batt_i_k = (page[8]<<24) + (page[9]<<16) + (page[10]<<8) + page[11];
        memory_calib_batt_u_k = (page[12]<<24) + (page[13]<<16) + (page[14]<<8) + page[15];
        memory_calib_batt_i_d = (page[16]<<24) + (page[17]<<16) + (page[18]<<8) + page[19];
        memory_calib_batt_u_d = (page[20]<<24) + (page[21]<<16) + (page[22]<<8) + page[23];
        memory_calib_batt_avg = page[24];
        memory_data_version=(page[25]<<24)+(page[26]<<16)+(page[27]<<8)+page[28];

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[29 + i]; // until 32
        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[33 + i]; // until 36

        cHelp = (char*)&memory_x_threshold;
        for(i = 0; i < 4; i++)
            cHelp[i] = page[37 + i]; // until 40

        memory_movement_count_threshold=(page[41]<<24) + (page[42]<<16) + (page[43]<<8) + page[44];

        memory_calib_weight0=(double)((page[45]<<24) + (page[46]<<16) + (page[47]<<8) + page[48]);
        memory_calib_weight_faktor =(double)((page[49]<<24) + (page[50]<<16) + (page[51]<<8) + page[52]);

        memory_status_log_off_delay = (unsigned int)((page[53]<<8) + page[54]);
        return;
    }
    memory_flash_version=FLASH_VERSION;
    memory_paramSaveAllParams();
}

/* memory_RFID_getMemAddr()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
uint32_t memory_RFID_getMemAddr(char *ID){
    uint64_t address = 0;

    address = str_string_to_uint64(ID);
    address %= MEMORY_MAX_ADDRESS;

    /*sprintf(serielle_text,"\ngot address: %lu for id:%02x \n",(uint32_t)address,ID[0]);
    serielle_send();*/

    return (uint32_t)address;
}

/* memory_RFID_readRFID()|memory.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void memory_RFID_readRFID(uint32_t address, char *readID){
    uint32_t pageAddr;
    uint32_t addrInPage;
    unsigned char charHelp;
    unsigned char page[256];

    pageAddr = address - address%256;//address/256;
    addrInPage = (address%256);///16;
    flashExtern_readBytes(1,pageAddr,page,255);

    for(charHelp = 0; charHelp < TWN4_MAX_ID_SIZE; charHelp++)
        readID[charHelp] = page[addrInPage + charHelp];

    /*sprintf(serielle_text,"ReadRFID from Address:%lu pageAddr:%lu addrInPage:%lu\n",address,pageAddr,addrInPage);
    serielle_send();
    sprintf(serielle_text,"found ID:%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",
                readID[0],readID[1],readID[2],readID[3],readID[4],readID[5],readID[6],readID[7],readID[8],readID[9],readID[10],readID[11],readID[12],readID[13],readID[14],readID[15]);
    serielle_send();*/
}

/* memory_RFID_saveRFID_perm()|memory.c
*
*  Parameter:		---
*
*  Return:			true if everything ok, false if memory is full
*
*  Description:
*
*/
unsigned char memory_RFID_saveRFID_perm(char *ID, unsigned char perm){
    uint32_t address;
    uint32_t startAddress;
    uint32_t pageAddr;
    uint32_t addrInPage;
    unsigned char i;
    unsigned char page[256];
    char readID[TWN4_MAX_ID_SIZE];

    address = Twn4LastIDAddress;//memory_RFID_getMemAddr(ID);
    startAddress = address;


    ID[TWN4_ID_ACCESS_BYTE] = 0x00; // delete access byte

    /* **** FOR DEBUG ****/
    /*str_array_to_string(ID, Twn4TempString,TWN4_MAX_ID_SIZE);
    sprintf(serielle_text,"try to save: ID:%s address:%lu perm:%d\n",Twn4TempString,address,perm);
    serielle_send();*/
    /* **** *** ****/

    do{
        memory_RFID_readRFID(address,readID);

        // if memory is empty on this address, save and exit
        if(memory_RFID_is_ID_empty(readID)){
            pageAddr = address - address%256;//address/256;
            addrInPage = (address%256);///16;
            flashExtern_readBytes(1,pageAddr,page,255);

            ID[TWN4_ID_ACCESS_BYTE] = perm;
            for(i = 0; i < TWN4_MAX_ID_SIZE; i++)
                page[addrInPage + i] = ID[i];

            flashExtern_writeEnable(1);
            flashExtern_pageWrite(1,pageAddr,page,255);
            flashExtern_writeDisable(1);

            /*sprintf(serielle_text,"saved on address: %lu ,pageAddr: %lu, addrInPage: %lu\n",address, pageAddr, addrInPage);
            serielle_send();*/
            return 1;
        }

        readID[TWN4_ID_ACCESS_BYTE] = 0x00; // delete access byte

        // if ID on memory is same
        if(str_cmp(ID,readID,TWN4_MAX_ID_SIZE)){
            pageAddr = address - address%256;//address/256;
            addrInPage = (address%256);///16;

            flashExtern_readBytes(1,pageAddr,page,255);
            page[addrInPage + TWN4_ID_ACCESS_BYTE] = perm;

            flashExtern_writeEnable(1);
            flashExtern_pageWrite(1,pageAddr,page,255);
            flashExtern_writeDisable(1);

            /*sprintf(serielle_text,"changed on address: %lu ,pageAddr: %lu, addrInPage: %lu\n",address, pageAddr, addrInPage);
            serielle_send();*/
            return 1;
        }
        else{
            /*sprintf(serielle_text,"address: %lu ,pageAddr: %lu, addrInPage: %lu not empty, so try with next\n",address, pageAddr, addrInPage);
            serielle_send();*/
            timer_feed_watchdog();
            address+=16;
            address%=MEMORY_MAX_ADDRESS;
            if (address == startAddress)
                return 0;
        }
    }while(1);//memory_RFID_is_empty(address));

    return 1;
}

/* unsigned char memory_get_RFID_perm(char *ID)|memory.c
*
*  Parameter:		address
*
*  Return:			1 if allowed, 0 if forbidden, 2 if error occurred
*
*  Description:
*
*/
unsigned char memory_get_RFID_perm(char *ID){
    uint32_t address;
    uint32_t startAddress;
    char readID[TWN4_MAX_ID_SIZE];
    unsigned char perm;

    address = Twn4LastIDAddress;//memory_RFID_getMemAddr(ID);
    startAddress = address;

    ID[TWN4_ID_ACCESS_BYTE] &= 0xFE; // to delete access bit
    /*sprintf(serielle_text,"search ID:%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x on address:%lu\n",
                ID[0],ID[1],ID[2],ID[3],ID[4],ID[5],ID[6],ID[7],ID[8],ID[9],ID[10],ID[11],ID[12],ID[13],ID[14],ID[15],address);
    serielle_send();*/

    while(1){
        memory_RFID_readRFID(address, readID);
        perm = readID[TWN4_ID_ACCESS_BYTE] & 0x01;
        /*sprintf(serielle_text,"readID:%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x from address:%lu\n",
                readID[0],readID[1],readID[2],readID[3],readID[4],readID[5],readID[6],readID[7],readID[8],readID[9],readID[10],readID[11],readID[12],readID[13],readID[14],readID[15],address);
        serielle_send();*/

        if( memory_RFID_is_ID_empty(readID) ){
            return 0;
        }

        readID[TWN4_ID_ACCESS_BYTE] &= 0xFE; // to delete access bit

        if( str_cmp(readID,ID,TWN4_MAX_ID_SIZE) ){
            return perm; // return access bit of readID
        }
        else{
            address+=16;
            address%=MEMORY_MAX_ADDRESS;
            if (address == startAddress){
                return 2; //memory error
            }
            timer_feed_watchdog();
        }
        /*sprintf(serielle_text,"new round with address:%lu\n",address);
        serielle_send();*/
    }
}

/* memory_RFID_is_empty(uint32_t address)|memory.c
*
*  Parameter:		address
*
*  Return:			true if empty or false if not empty
*
*  Description:
*
*/
unsigned char memory_RFID_is_empty(uint32_t address){
    char readID[TWN4_MAX_ID_SIZE];
    unsigned char trFls;

    memory_RFID_readRFID(address, readID);
    trFls = memory_RFID_is_ID_empty(readID);

    return trFls;
}


unsigned char memory_RFID_is_ID_empty(char* readID){
    unsigned char i;
    uint16_t signs = 0;

    for(i = 0; i < TWN4_MAX_ID_SIZE; i++)
        signs += readID[i];

    if( signs / TWN4_MAX_ID_SIZE == 0xFF){
        /*sprintf(serielle_text,"storage is empty\n");
        serielle_send();*/
        return 1;
    }
    else{
        /*sprintf(serielle_text,"storage is full\n");
        serielle_send();*/
        return 0;
    }
}

void memory_debug_write0_to_page(uint32_t pageAddr){
    unsigned char page[256];
    int uHelp;

    for(uHelp = 0; uHelp < 256; uHelp++) {
        page[uHelp] = 0xAA;
    }

    flashExtern_writeEnable(1);
    flashExtern_pageWrite(1,pageAddr,page,255);
    flashExtern_writeDisable(1);

    /*for(uHelp = 0; uHelp < 16; uHelp++){
        sprintf(serielle_text,"written part %d:%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",uHelp,
        page[0+uHelp*16],page[1+uHelp*16],page[2+uHelp*16],page[3+uHelp*16],page[4+uHelp*16],page[5+uHelp*16],page[6+uHelp*16],page[7+uHelp*16],
        page[8+uHelp*16],page[9+uHelp*16],page[10+uHelp*16],page[11+uHelp*16],page[12+uHelp*16],page[13+uHelp*16],page[14+uHelp*16],page[15+uHelp*16]);
        serielle_send();
    }*/
}

void memory_debug_read_page(uint32_t pageAddr){
    unsigned char page[256];
    int charHelp;

    flashExtern_readBytes(1,pageAddr,page,255);

    /*for(charHelp = 0; charHelp < 16; charHelp++){
        sprintf(serielle_text,"page part %d:%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",charHelp,
        page[0+charHelp*16],page[1+charHelp*16],page[2+charHelp*16],page[3+charHelp*16],page[4+charHelp*16],page[5+charHelp*16],page[6+charHelp*16],page[7+charHelp*16],
        page[8+charHelp*16],page[9+charHelp*16],page[10+charHelp*16],page[11+charHelp*16],page[12+charHelp*16],page[13+charHelp*16],page[14+charHelp*16],page[15+charHelp*16]);
        serielle_send();
    }*/
}
/*sprintf(serielle_text,"id[0]:%d id[1]:%d id[2]:%d id[3]:%d  id[4]:%d id[5]:%d id[6]:%d id[7]:%d id[8]:%d id[9]:%d id[10]:%d id[11]:%d id[12]:%d id[13]:%d id[14]:%d id[15]:%d\n",
    ID[0],ID[1],ID[2],ID[3],ID[4],ID[5],ID[6],ID[7],ID[8],ID[9],ID[10],ID[11],ID[12],ID[13],ID[14],ID[15]);
serielle_send();

sprintf(serielle_text,"rid[0]:%d rid[1]:%d rid[2]:%d rid[3]:%d  rid[4]:%d rid[5]:%d rid[6]:%d rid[7]:%d rid[8]:%d rid[9]:%d rid[10]:%d rid[11]:%d rid[12]:%d rid[13]:%d rid[14]:%d rid[15]:%d\n",
    readID[0],readID[1],readID[2],readID[3],readID[4],readID[5],readID[6],readID[7],readID[8],readID[9],readID[10],readID[11],readID[12],readID[13],readID[14],readID[15]);
serielle_send();*/

