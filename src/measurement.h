/*
* measurement.h
*
*  Created on: 02.12.2014
*      Author: Hütter Stefan
*/
#ifndef MEASUREMENT_H_
#define MEASUREMENT_H_

#define MEASUREMENT_INDEX_MAX	129022
#define MSM_AVERAGE_CTRL 16     //averaging over x measures in one routine
#define MSM_MS_AVG  20          //averaging over x ms
#define MSM_AVERAGE 5           //averaging over x seconds


void measurement_init(void);
double measurement_getU(void);
float measurement_getI(void);
double measurement_getPressure(void);
int measurement_getOnBoardTemp(void);
void measurement_meas_every_ms(void);
void measurement_buildSum(void);
void measurement_calcAverage(void);
double measurement_getAverageU(void);
double measurement_getAverageI(void);
double measurement_getAverageP(void);

double measurement_getWeight(void);
void measurement_calib_weight0(void);
void measurement_calib_weight_faktor(double weight);

#endif //MEASUREMENT_H_
