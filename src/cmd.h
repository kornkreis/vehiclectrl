/*
* cmd.h
*
*  Created on: 21.01.2015
*      Author: Hütter Stefan
*/
#ifndef CMD_H_
#define CMD_H_

#define CMD_SET_TIME		0x01
#define CMD_SET_DATE		0x02
#define CMD_SET_SN		0x03
#define CMD_SET_CURRENT		0x04
#define CMD_SET_TIME_PERIOD	0x05
#define CMD_CALIB		0x06
#define CMD_GET_MEASUREMENT	0x07
#define CMD_JSON_GOT		0x08
#define CMD_SET_INDEX		0x09
#define CMD_GET_ADC_INTERN	0x0A
#define CMD_CALIB_TEMP          0x0B
#define CMD_SET_STATE           0x0C
#define CMD_RESET_FT311         0x0D
#define CMD_CALIB_PARAM_STAT    0x0E
#define CMD_ONOFF_DATASEND      0x0F
#define CMD_GET_JSON            0x10
#define CMD_DELETE_FLASH        0x11
#define CMD_SET_LOG_OFF_DELAY   0x12

#define CMD_RESTART_CONTROLLER  0x23
#define CMD_GET_FW_VERSION      0x24
#define CMD_INSTALL_APK         0x27


#define CMD_GOTO_BOOTLOADER     0x30


// CMDs from 0x0F to 0x4E reserved for charger
// CMDs from 0x4F to 0x70 reserved for vehicle controller
#define CMD_SET_MOVEMENT_THRESHOLD  0x4F
#define CMD_CHANGE_PERM             0x50
#define CMD_ADMIN_MODE              0x51
#define CMD_LOGOFF                  0x52
#define CMD_SET_USER_VERSION        0x53

#define CMD_CALIB_MODE              0x55
#define CMD_CALIB_WEIGHT            0x56





#define CMD_SET_TIME_PERIOD_RUN		0x00
#define CMD_SET_TIME_PERIOD_BUSY	0x01

#define CMD_SENSOR_SUPPL_VOLTAGE	0x00
#define CMD_SENSOR_BATT_VOLTAGE		0x01
#define CMD_SENSOR_BATT_CURREN		0x02
#define CMD_SENSOR_BATT_TEMP		0x03

#define CMD_OK		0x00
#define CMD_ERROR	0x01

#define CMD_DEVICE_PC		0x00
#define CMD_DEVICE_HANDY	0x01

/** CMDs FOR MOBILE **/
#define CMD_M_TAKE_PIC 0x01

extern void cmd_init(void);
extern void cmd_exec(char *recBuffer, unsigned char len, char device);
extern void cmd_answer(char *recBuffer,char device);


extern unsigned char cmd_ready;
extern unsigned char cmd_send_json;
extern unsigned char CmdChanged;

#endif //CMD_H_
