/*
 * main.h
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#ifndef MAIN_H_
#define MAIN_H_

#define MAIN_TYPE_CHARGER               1
#define MAIN_TYPE_BATTERY               2
#define MAIN_TYPE_BATTERY_CONTROLLER    3
#define MAIN_TYPE_CHARGER_CONTROLLER    4
#define MAIN_TYPE_CAR_CONTROLLER        5
#define MAIN_TYPE_INFO_SCREEN           6

#define MAIN_TYPE_SN_BIT    28

void main_led_control();
extern unsigned char main_controller_type;
extern unsigned char main_send_on;
extern unsigned char calib;

#endif /* MAIN_H_ */
