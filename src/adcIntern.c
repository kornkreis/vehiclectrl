/*
* adcIntern.c
*
*  Created on: 19.11.2014
*      Author: Hütter Stefan
*/
#include "adcIntern.h"
#include "defines.h"
#include "LPC214x.h"

/*
*  adcIntern_init()|adcIntern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert den ADC zur Messung der Spannung
*
*/
 void adcIntern_init(void)
 {
	PINSEL0|=0x300000; //pin 0.10 auf ADC1.2 schalten

	PINSEL1&= ~((0<<26)|(1<<27));//Pin 0.29 auf ADC0.2 schalten
	PINSEL1|=  ((1<<26)|(0<<27));

	PCONP  |=0x100000;
	PCONP  |=   (1<<12);


	AD1CR =   (1<<2)	//Select ADC1.2 014
			| (3 << 8) //CLKDIV auf 3
			| (0 <<17) //CLKS auf 0 -->10bit
			| (1 <<21); //PN auf 1 (converter is operational)
 }

/*
*  adcIntern_read_ADC1_2()|adcIntern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Liefert den 10-Bitwert des ADCs(Spannungsmessung)
*
*/
int adcIntern_read_ADC1_2(void){
  // Stop
  AD1CR &= ~(7<<24);
  // Start
  AD1CR |=  (1<<24);

  // Wait for the conversion to complete
  while (!(AD1DR2 & (1<<31)));

  // Return the processed results

  return ((AD1DR2 & 0xFFC0)>>6);

}

/*
*  adcIntern_read_ADC0_2()|adcIntern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	AD0.2 -->
*					Liefert den 10-Bitwert ADCs-->(Eingangsspannungsmessung)
*
*/
int adcIntern_read_ADC0_2(void){
  unsigned long dummy;
  dummy=(AD0GDR & (1<<31));
  AD0CR =   (1<<2)	//Select ADC0.2 014
			| (3 << 8) //CLKDIV auf 3
			| (0 <<17) //CLKS auf 0 -->10bit
			| (1 <<21); //PN auf 1 (converter is operational)
  dummy=(AD0GDR & (1<<31));
  // Stop
  AD0CR &= ~(7<<24);
  // Start
  AD0CR |=  (1<<24);

 // Stop
	AD0CR &= ~(7<<24); // 0b111
	// Start
	AD0CR |=  (1<<24);

	while (!(AD0GDR & (1<<31)));
	// Return the processed results
	return ((AD0GDR & 0xFFC0)>>6);

}



/*
*  adcIntern_read_ADC0_4()|adcIntern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	AD0.4 -->
*					Liefert den 10-Bitwert ADCs-->(Eingangsspannungsmessung)
*
*/
int adcIntern_read_ADC0_4(void){
  unsigned long dummy;
  dummy=(AD0GDR & (1<<31));
  AD0CR =   (1<<4)	//Select ADC0.4 014
			| (3 << 8) //CLKDIV auf 3
			| (0 <<17) //CLKS auf 0 -->10bit
			| (1 <<21); //PN auf 1 (converter is operational)
  dummy=(AD0GDR & (1<<31));
  // Stop
  AD0CR &= ~(7<<24);
  // Start
  AD0CR |=  (1<<24);

 // Stop
	AD0CR &= ~(7<<24); // 0b111
	// Start
	AD0CR |=  (1<<24);

	while (!(AD0GDR & (1<<31)));
	// Return the processed results
	return ((AD0GDR & 0xFFC0)>>6);

}
