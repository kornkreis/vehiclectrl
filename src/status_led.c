/*
 * status_led.c
 *
 *  Created on: 21.05.2014
 *      Author: huetter_st
 */

#include "main.h"
#include "LPC214x.h"
#include "status_led.h"
#include "hardware.h"
#include "status.h"

//int status_LED_time_batt_discon = 0;

void status_LED_init(void){
	PINSEL0=PINSEL0&0xF03F00FF; //0 dann ist 0.4-0.7 und 0.11-0.13 auf GPIO --> 11110000001111110000000011111111 --> 0xF03F00FF
	//PINSEL2=PINSEL2&0xFFFFFFF7;//1.16-1.25 als GPIO --> 11111111111111111111111111110111 --> 0xFFFFFFF7
	IODIR0 = IODIR0 | ((1<<4)|(1<<5)|(1<<6)|(1<<7)|(1<<11)|(1<<12)|(1<<13));//auf Output
	IODIR1 = IODIR1 | ((1<<24));//auf Output
    //IODIR1 = IODIR1 | ((1<<16)|(1<<17)|(1<<18)); //1.16 auf Output --> f�r chargerCtrl: externe LEDs
}

//status_LED_enable: sets the enable Pin of LED-Driver
void status_LED_enable(void){
	IOCLR1=1<<24;
}

void status_LED_disable(void){
	IOSET1=1<<24;
}

void status_LED_set(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden
	IOSET0=1<<(STATUS_LED_FIRST+nr-1);
}

void status_LED_clear(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden
	IOCLR0=1<<(STATUS_LED_FIRST+nr-1);
}

void status_LED_toggl(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden
	if(IOPIN0&(1<<(STATUS_LED_FIRST+nr-1)))
		IOCLR0=1<<(STATUS_LED_FIRST+nr-1);
	else
		IOSET0=1<<(STATUS_LED_FIRST+nr-1);
}


void status_extLED_set(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden

	HW_EXT_LED_SET=1<<(HW_STATUS_EXTLED_FIRST+nr-1);
}

void status_extLED_clear(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden
    HW_EXT_LED_CLR=1<<(HW_STATUS_EXTLED_FIRST+nr-1);
	//IOCLR0=1<<(STATUS_EXTLED_FIRST+nr-1);
}

void status_extLED_toggl(unsigned char nr){
	if((nr>3)||(nr<1))
		return;//LED nicht vorhanden
	if(HW_EXT_LED_PIN&(1<<(HW_STATUS_EXTLED_FIRST+nr-1)))
		HW_EXT_LED_CLR=1<<(HW_STATUS_EXTLED_FIRST+nr-1);
	else
		HW_EXT_LED_SET=1<<(HW_STATUS_EXTLED_FIRST+nr-1);
}

/*
*  status_peep_toggl()|status_led.c
*
*  Parameter:       ---
*
*  Return:			---
*
*  Description:     toggles the peep on relais
*
*/
void status_peep_toggl(unsigned char ctrType){
    if (ctrType == MAIN_TYPE_CHARGER_CONTROLLER)
    {
        if (STATUS_LED_PEEP_PIN&(STATUS_LED_PEEP_NR))
            STATUS_LED_PEEP_CLR;
        else
            STATUS_LED_PEEP_SET;
    }
}

/*
*  status_peep_clear()|status_led.c
*
*  Parameter:       ---
*
*  Return:			---
*
*  Description:     clears the peep on relais
*
*/
void status_peep_clear(unsigned char ctrType){
    if (ctrType == MAIN_TYPE_CHARGER_CONTROLLER)
        STATUS_LED_PEEP_CLR;
}

/*
*  status_peep_set()|status_led.c
*
*  Parameter:       ---
*
*  Return:			---
*
*  Description:     clears the peep on relais
*
*/
void status_peep_set(unsigned char ctrType){
    if (ctrType == MAIN_TYPE_CHARGER_CONTROLLER)
        STATUS_LED_PEEP_SET;
}

/*
*  status_LED_control()|status_led.c
*
*  Parameter:       ---
*
*  Return:			---
*
*  Description:     regulates the Stets-LEDs
*
*/
void status_LED_control(unsigned char ctrType, int ms,unsigned int sec)
{
    if(!ms%1000) // for toggling every sec
    {
        switch(status_status)
        {
            case 0:
            case 1:
                break;
        }
    }
}
