/*
 * flashExtern.c
 *
 *  Created on: 25.11.2014
 *      Author: H�tter Stefan
 */

#include "spi.h"
#include "defines.h"
#include "flashExtern.h"
#include "main.h"
#include "LPC214x.h"
#include "timer.h"

/* flashExtern_init()|flashExtern.c
*
*  Parameter:		---
* flashExtern_readID
*  Return:			---
*
*  Beschreibung:	Initialisiert die externen Flashs
*
*/
void flashExtern_init(){
	PINSEL1 &=0xFFFFCFFF;//pin 0.22 as GPIO
	PINSEL1 &=0xFFFFF3FF;//pin 0.21 as GPIO
	IODIR0  |= (1<<21)|(1<<22)|(1<<28);
	IOSET0 = (1<<21)|(1<<22)|(1<<28);
}

/* flashExtern_startConv()|flashExtern.c
*
*  Parameter:		unsigned char flashNr --> nr. des Flash (0 oder 1)
*
*  Return:			---
*
*  Beschreibung:	Startet die Conversion f�r den gew�hlten Flash
*
*/
void flashExtern_startConv(unsigned char flashNr){
	SSPCR0 = 0x0f07;
	IOCLR0 = (1<<(21+flashNr));
}

/* flashExtern_stopConv()|flashExtern.c
*
*  Parameter:		unsigned char flashNr --> nr. des Flash (0 oder 1)
*
*  Return:			---
*
*  Beschreibung:	Beendet die Conversion
*
*/
void flashExtern_stopConv(unsigned char flashNr){
	IOSET0 = (1<<(21+flashNr));
}

/* flashExtern_readByte()|flashExtern.c
*
*  Parameter:		---
*
*  Return:			unsigned int --> Byte vom SPI
*
*  Beschreibung:	Liest ein Byte vom SPI
*
*/
unsigned int flashExtern_readByte(void){
	unsigned int val;
	while (SSPSR & 0x10)
	{
	} // warten bis SPI freiunsigned int val;
	SSPDR = 0xff; // dummy schreiben
	while (!(SSPSR & 0x04))
	{
	} // warten bis SPI frei
	val=SSPDR;
	return (val);
}

/* flashExtern_writeByte()|flashExtern.c
*
*  Parameter:		unsigned int byte --> Byte das geschrieben werden muss
*
*  Return:			---
*
*  Beschreibung:	Schreibt ein Byte auf den SPI
*
*/
void flashExtern_writeByte(unsigned int byte){
	unsigned int val;

	while (SSPSR & 0x10){} // warten bis SPI frei
	SSPDR = byte; // CMD schreiben

	while (SSPSR & 0x10){}
	val=SSPDR;
}

/* flashExtern_writeEnable()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*
*  Return:			---
*
*  Beschreibung:	Initialisiert die externen Flashs
*
*/
void flashExtern_writeEnable(unsigned char flashNR){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_WRITE_ENABLE);
	flashExtern_stopConv(flashNR);
}

/* flashExtern_writeDisable()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*
*  Return:			---
*
*  Beschreibung:	hebt den Schreibschutz des entsprechnenden Flashs auf
*
*/
void flashExtern_writeDisable(unsigned char flashNR){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_WRITE_DISABLE);
	flashExtern_stopConv(flashNR);
}

/* flashExtern_readID()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					char *id --> Pointer auf den die ID geschrieben werden soll [20 Zeichen]
*
*  Return:			---
*
*  Beschreibung:	Initialisiert die externen Flashs
*
*/
void flashExtern_readID(unsigned char flashNR,unsigned char *id){
	int i;
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_READ_IDENTIFICATION);
	for(i=0;i<20;i++)
		id[i]=flashExtern_readByte();
	flashExtern_stopConv(flashNR);
}

/* flashExtern_readStatusRegister()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*
*  Return:			unsigned char --> Statusregister
*
*  Beschreibung:	Liest den Status des Flashs
*
*/
unsigned char flashExtern_readStatusRegister(unsigned char flashNR){
	unsigned char StatusReg;
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_READ_STATUS_REGISTER);
	StatusReg=flashExtern_readByte();
	flashExtern_stopConv(flashNR);
	return StatusReg;
}

/* flashExtern_readBytes()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					unsigned int startAddr --> Adresse des ersten Bytes
*					char *bytes --> pointer auf den Buffer f�r die gelesenen Bytes
*					unsigned char len --> Anzahl der zu lesenden Bytes - 1 (bei 0 wird ein Byte gelesen)
*
*  Return:			---
*
*  Beschreibung:	Lie�t ein Byte aus dem Speicher der Wahl aus
*
*/
void flashExtern_readBytes(unsigned char flashNR, unsigned int startAddr, unsigned char *bytes, unsigned char len){
	int i;
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_READ_DATA_BYTES);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	for(i=0;i<=len;i++)
		bytes[i]=flashExtern_readByte();
	flashExtern_stopConv(flashNR);
}

/* flashExtern_pageWrite()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					unsigned int startAddr --> Adresse des ersten Bytes
*					unsigned char *bytes --> Pointer auf die zu schreibenden Bytes
*					unsigned char len --> Anzahl der zu schreibenden Bytes -1 (bei 0 wird ein Byte geschrieben)
*
*  Return:			---
*
*  Beschreibung:	Schreiben einer Seite auf den Flash
*
*/
void flashExtern_pageWrite(unsigned char flashNR, unsigned int startAddr,unsigned char *bytes, unsigned char len){
	int i;
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_PAGE_WRITE);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	for(i=0;i<=len;i++)
		flashExtern_writeByte(bytes[i]);
	flashExtern_stopConv(flashNR);

    //warten bis WIP=0 (Bit 0 im Statusregister)
	while(flashExtern_readStatusRegister(flashNR)&0x01){
		timer_feed_watchdog();
	}
}

/* flashExtern_pageProgram()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					unsigned int startAddr --> Adresse des ersten Bytes
*					unsigned char *bytes --> Pointer auf die zu schreibenden Bytes
*					unsigned char len --> Anzahl der zu schreibenden Bytes -1 (bei 0 wird ein Byte geschrieben)
*
*  Return:			---
*
*  Beschreibung:	Programmieren einer Seite auf den Flash*
*/
void flashExtern_pageProgram(unsigned char flashNR, unsigned int startAddr,unsigned char *bytes, unsigned char len){
	int i;
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_PAGE_PROGRAM);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	for(i=0;i<=len;i++)
		flashExtern_writeByte(bytes[i]);

	flashExtern_stopConv(flashNR);
}

/* flashExtern_pageErase()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					unsigned int startAddr --> Startadresse
*
*  Return:			---
*
*  Beschreibung:	L�scht eine Seite ab der Start Adresse
*
*/
void flashExtern_pageErase(unsigned char flashNR, unsigned int startAddr){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_PAGE_ERASE);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	flashExtern_stopConv(flashNR);
}

/* flashExtern_sectorErase()|flashExtern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	L�scht einen Sektor ab der Start Adresse
*
*/
void flashExtern_sectorErase(unsigned char flashNR, unsigned int startAddr){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_SECTOR_ERASE);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	flashExtern_stopConv(flashNR);
}

/* flashExtern_startConv()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*					unsigned int startAddr --> Startadresse
*
*  Return:			---
*
*  Beschreibung:	Initialisiert die externen Flashs
*
*/
void flashExtern_subSectorErase(unsigned char flashNR, unsigned int startAddr){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_SUBSECTOR_ERASE);
	flashExtern_writeByte((unsigned char)((startAddr&0xFF0000)>>16));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF00)>>8));
	flashExtern_writeByte((unsigned char)((startAddr&0xFF)));
	flashExtern_stopConv(flashNR);
}

/* flashExtern_bulkErase()|flashExtern.c
*
*  Parameter:		unsigned char flashNR --> nr. des Flash (0 oder 1)
*
*  Return:			---
*
*  Beschreibung:	L�scht den gesammten Flash
*
*/
void flashExtern_bulkErase(unsigned char flashNR){
	flashExtern_startConv(flashNR);
	flashExtern_writeByte(FLASHEXTERN_CMD_BULK_ERASE);
	flashExtern_stopConv(flashNR);
	while(flashExtern_readStatusRegister(flashNR)&0x01)//warten bis WIP=0 (Bit 0 im Statusregister)
	{
		timer_feed_watchdog();
	}
}
