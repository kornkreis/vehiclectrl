/*
* battCtrl.h
*
*  Created on: 05.05.2015
*      Author: H�tter Wolfgang
*/
#ifndef BATTCTRL_H_
#define BATTCTRL_H_

#define HW_ADCEXT_SET_CS        IOSET0 = 0x10000       //CS_A
#define HW_ADCEXT_CLR_CS        IOCLR0 = 0x10000
#define HW_ADCEXT_SET_GPIO      PINSEL1 &=  0xFFFFFCFC  //als GPIO
#define HW_ADCEXT_SSPCR         SSPCR0 = 0x02cb    //SSP Control Register 0 --> 12bit transfer, SPI... - Manual S.201

#define HW_EXT_LED_CLR IOCLR1 //IOCLR0 --> Port 0
#define HW_EXT_LED_SET IOSET1
#define HW_EXT_LED_PIN IOPIN1

#define HW_LED_CLR IOCLR0
#define HW_LED_SET IOSET0

#define HW_STATUS_EXTLED_FIRST 16   //First Port Pin

//functions
#endif //BATTCTRL_H_
