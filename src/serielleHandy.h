/*
 * serielle.h
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#ifndef SERIELLEHANDY_H_
#define SERIELLEHANDY_H_

#define SERIELLEHANDY_STATE_WAIT_FOR_START	0
#define SERIELLEHANDY_STATE_WAIT_FOR_LEN		1
#define SERIELLEHANDY_STATE_WAIT_FOR_DATA	2
#define SERIELLEHANDY_STATE_WAIT_FOR_CKSUM	3
#define SERIELLEHANDY_STATE_WAIT_FOR_ACK		4

#define SERIELLEHANDY_REC_ERROR_NO		0
#define SERIELLEHANDY_REC_ERROR_CKSUM	1
#define SERIELLEHANDY_REC_ERROR_NORET	2

#define SERIELLEHANDY_REC_BUSY	0
#define SERIELLEHANDY_REC_READY	1


#define SERIELLEHANDY_START_SIGN_RX '#'
#define SERIELLEHANDY_START_SIGN_TX	'>'




extern char serielleHandy_text[1024];
extern char serielleHandy_rec_buffer[256];
extern unsigned char serielleHandy_rec_len;
extern unsigned char serielleHandy_rec_ready;

void __attribute__ ((interrupt("IRQ"))) serielleHandy_isr();
void serielleHandy_init();
char serielleHandy_putch(char nch);
char serielleHandy_puttext( char *textp);
void serielleHandy_send();
void serielleHandy_write_in_buffer(unsigned char c);
void serielleHandy_write_to_uart(char *buffer, unsigned char len);

#endif /* SERIELLEHANDY_H_ */
