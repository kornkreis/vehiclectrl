/*
* flashExtern.h
*
*  Created on: 25.11.2014
*      Author: Hütter Stefan
*/
#ifndef FLASHEXTERN_H_
#define FLASHEXTERN_H_

#define FLASHEXTERN_CMD_WRITE_ENABLE 0x0006
#define FLASHEXTERN_CMD_WRITE_DISABLE 0x0004
#define FLASHEXTERN_CMD_READ_IDENTIFICATION 0x009F
#define FLASHEXTERN_CMD_READ_STATUS_REGISTER 0x0005
#define FLASHEXTERN_CMD_WRITE_STATUS_REGISTER 0x0001
#define FLASHEXTERN_CMD_WRITE_LOCK_REGISTER 0x00E5
#define FLASHEXTERN_CMD_READ_LOCK_REGISTER 0x00E8
#define FLASHEXTERN_CMD_READ_DATA_BYTES 0x0003
#define FLASHEXTERN_CMD_READ_DATA_BYTES_HIGHER_SPEED 0x000B
#define FLASHEXTERN_CMD_PAGE_WRITE 0x000A
#define FLASHEXTERN_CMD_PAGE_PROGRAM 0x0002
#define FLASHEXTERN_CMD_PAGE_ERASE 0x00DB
#define FLASHEXTERN_CMD_SUBSECTOR_ERASE 0x0020
#define FLASHEXTERN_CMD_SECTOR_ERASE 0x00D8
#define FLASHEXTERN_CMD_BULK_ERASE 0x00C7
#define FLASHEXTERN_CMD_DEEP_POWER_DOWN 0x00B9
#define FLASHEXTERN_CMD_RELEASE_DEEP_POWERDOWN 0x00AB

void flashExtern_init();
void flashExtern_startConv(unsigned char flashNr);
void flashExtern_stopConv(unsigned char flashNr);
unsigned int flashExtern_readByte(void);
void flashExtern_writeByte(unsigned int byte);
void flashExtern_writeEnable(unsigned char flashNR);
void flashExtern_writeDisable(unsigned char flashNR);
void flashExtern_readID(unsigned char flashNR, unsigned char *id);
unsigned char flashExtern_readStatusRegister(unsigned char flashNR);
void flashExtern_readBytes(unsigned char flashNR, unsigned int startAddr, unsigned char *bytes, unsigned char len);
void flashExtern_pageWrite(unsigned char flashNR, unsigned int startAddr,unsigned char *bytes, unsigned char len);
void flashExtern_pageProgram(unsigned char flashNR, unsigned int startAddr,unsigned char *bytes, unsigned char len);
void flashExtern_pageErase(unsigned char flashNR, unsigned int startAddr);
void flashExtern_sectorErase(unsigned char flashNR, unsigned int startAddr);
void flashExtern_subSectorErase(unsigned char flashNR, unsigned int startAddr);
void flashExtern_bulkErase(unsigned char flashNR);
#endif //FLASHEXTERN_H_
