/*
    date:   15.07.2015
    author: Sonja Klug
*/

// Makros
#define I2C_RepeatedStart()    I20CONSET = 1 << STA         //set STA
#define I2C_Start()            I20CONSET = 1 << STA         //set STA
#define I2C_Stop()             I20CONSET = 1 << STO         //set STO
#define I2C_Wait()             while(!(I20CONSET & (1<<SI)))//solange SI ist nicht gesetzt -> warten

// Prototypen
void I2C_WriteRegister(unsigned char SlaveAddress, unsigned char RegisterAddress, char Data);
unsigned char I2C_ReadRegister(unsigned char SlaveAddress, unsigned char RegisterAddress);
void I2C_ReadMultiRegisters(unsigned char SlaveAddress, unsigned char RegisterAddress, unsigned char n, unsigned char *r);

