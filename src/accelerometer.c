/*
    date:   15.07.2015
    author: Sonja Klug
*/

#include "iic.h"
#include "iic_accel.h"
#include "MMA8652FC.h"
#include "LPC214x.h"
#include "accelerometer.h"
#include "memory.h"

#include "status_led.h"

// Globale Variablen
unsigned char AccelData[6];                  // speichert Messdaten
char AccelData_Fifo_8bit[96];                // FIFO 8bit
unsigned char AccelData_Fifo_12bit[192];              // FIFO 12bit
float Xout_g, Yout_g, Zout_g;                // speichert Daten in g
short Xout_12_bit, Yout_12_bit, Zout_12_bit; // speichert 12 Bit Daten
unsigned char DataReady_FIFO;


float accel_X_cumulated=0;
float accel_Y_cumulated=0;
float accel_Z_cumulated=0;

unsigned char accel_nrCumulations=0;
unsigned int accel_no_move_count=0;


// Stellt die Messdaten zur Verf�gung
void get_accel_data (){
    I2C_ReadMultiRegisters(MMA8652FC_I2C_ADDRESS, OUT_X_MSB_REG, 6, AccelData);		// Read data output registers 0x01-0x06

    // 12-bit accelerometer data
    Xout_12_bit = ((short) (AccelData[0]<<8 | AccelData[1])) >> 4;		// Compute 12-bit X-axis acceleration output value
    Yout_12_bit = ((short) (AccelData[2]<<8 | AccelData[3])) >> 4;		// Compute 12-bit Y-axis acceleration output value
    Zout_12_bit = ((short) (AccelData[4]<<8 | AccelData[5])) >> 4;		// Compute 12-bit Z-axis acceleration output value

    // Accelerometer data converted to g's -> 2g
    Xout_g = ((float) Xout_12_bit) / SENSITIVITY_2G;		// Compute X-axis output value in g's
    Yout_g = ((float) Yout_12_bit) / SENSITIVITY_2G;		// Compute Y-axis output value in g's
    Zout_g = ((float) Zout_12_bit) / SENSITIVITY_2G;		// Compute Z-axis output value in g's*/

    // Accelerometer data converted to g's -> 4g
    /*Xout_g = ((float) Xout_12_bit) / SENSITIVITY_4G;		// Compute X-axis output value in g's
    Yout_g = ((float) Yout_12_bit) / SENSITIVITY_4G;		// Compute Y-axis output value in g's
    Zout_g = ((float) Zout_12_bit) / SENSITIVITY_4G;		// Compute Z-axis output value in g's*/
}

// Initialisiert den Accelerometer
void MMA8652FC_Init (){
    // init ohne interrupt
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);          // Standby Mode
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG2, 0x40);      // Reset all registers to POR values
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, XYZ_DATA_CFG_REG, 0x00);       // +/- 2g
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, XYZ_DATA_CFG_REG, 0x01);      // +/- 4g
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, XYZ_DATA_CFG_REG, 0x02);      // +/- 8g
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG2, 0x02);      // High Resolution mode
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x01);      // Active mode, ODR 800Hz

	// init �C
	/*IODIR0 &= ~ (1<<20);            // port 0.20 as input
	PINSEL1 |= (1<<8) | (1<<9);     // Pin select EINT3
    EXTMODE = (1<<3);               // edge sensitive
    EXTPOLAR &= ~ (1<<3);           // active low*/

    // Pulse Detection Interrupt init
    /*I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);          // Standby Mode
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, PULSE_CFG_REG, 0x15);	    // Enable X and Y and Z single pulse
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, PULSE_THSX_REG, 0x0A);     // Set X-axis threshold to 10 * 0.063 = 0.63g
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, PULSE_THSY_REG, 0x0A);     // Set Y-axis threshold to 10 * 0.063 = 0.63g
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, PULSE_THSZ_REG, 0x0A);     // Set Z-axis threshold to 10 * 0.063 = 0.63g
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG4, 0x08);          // Enable pulse detection interrupts
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG5, 0x08);          //Route pulse detection interrupt to INT1
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x01);          // Active Mode*/
    // Vectored IRQ init �C
    /*VICVectAddr10 = (unsigned long) accel_isr;           // Accelerometer Interrupt Service Routine
    VICVectCntl10 = (1<<5) | 17;                         // enables the slot and interrupt source number
    VICIntEnable |= (1<<17);                             // enables EINT3 as IRQ*/

    // Freefall/Motion Detection Interrupt init
    /*I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);          // Standby Mode
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, FF_MT_CFG_REG, 0x78);	    // Enable motion detection and axis event flags
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, FF_MT_THS_REG, 0x0A);      // Set motion threshold to 10 * 0.063 = 0.63g
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG4, 0x04);          // Enable motion detection interrupts
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG5, 0x04);          //Route motion detection interrupt to INT1
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x01);          // Active Mode*/
    // Vectored IRQ init �C
    /*VICVectAddr10 = (unsigned long) accel_isr;           // Accelerometer Interrupt Service Routine
    VICVectCntl10 = (1<<5) | 17;                         // enables the slot and interrupt source number
    VICIntEnable |= (1<<17);                             // enables EINT3 as IRQ*/

    // Landscape/Portrait
    /*I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);          // Standby Mode
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, PL_CFG_REG, 0x40);         // enable Portrait/Landscape Orientation
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG4, 0x10);          // Enable PL Orientation interrupts
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG5, 0x10);          //Route PL orientation interrupt to INT1
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x01);          // Active Mode*/
    // Vectored IRQ init �C
    /*VICVectAddr10 = (unsigned long) accel_isr;           // Accelerometer Interrupt Service Routine
    VICVectCntl10 = (1<<5) | 17;                         // enables the slot and interrupt source number
    VICIntEnable |= (1<<17);                             // enables EINT3 as IRQ*/

    // FIFO fast read
    /*I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG2, 0x40);      // Reset all registers to POR values
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, F_SETUP_REG, 0x00);        // Fifo setup = 0 um F_Read zu setzen
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x3A);          // Standby Mode, fast read, 1.56Hz
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x32);          // standby, fast read, 6.25Hz
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, F_SETUP_REG, 0x80);        // Fifo mode 10
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, TRIG_CFG_REG, 0x08);       // Pulse Interrupt Trigger
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG3, 0x00);          // push pull, active low
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG4, 0x40);          // enable fifo interrupt
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG5, 0x40);          //Route fifo interrupt to INT1
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x3B);          // Active Mode, 1.56Hz, fast read
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x33);          // active, fast read, 6.25Hz*/

    // Vectored IRQ init �C
    /*VICVectAddr10 = (unsigned long) accel_isr;           // Accelerometer Interrupt Service Routine
    VICVectCntl10 = (1<<5) | 17;                         // enables the slot and interrupt source number
    VICIntEnable |= (1<<17);                             // enables EINT3 as IRQ*/

    // Fifo 12 bit data
    /*I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);      // standby
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG2, 0x40);      // Reset all registers to POR values
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, XYZ_DATA_CFG_REG, 0x00);   // +/- 2g
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG2, 0x02);      // High Resolution mode
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, F_SETUP_REG, 0x80);    // fill mode
    //I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, F_SETUP_REG, 0x40);    // circular buffer
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG3, 0x00);      // push pull, active low
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG4, 0x40);      // fifo interrupt
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG5, 0x40);      // route to INT1
    I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x3B);      // active, 1.56Hz, fast read*/

    // Vectored IRQ init �C
    /*VICVectAddr10 = (unsigned long) accel_isr;           // Accelerometer Interrupt Service Routine
    VICVectCntl10 = (1<<5) | 17;                         // enables the slot and interrupt source number
    VICIntEnable |= (1<<17);                             // enables EINT3 as IRQ*/
}

// Kalibriert den Accelerometer
// (Eigentlich nicht notwendig!!)
void MMA8652FC_Calibration ()
{
	char X_offset, Y_offset, Z_offset;

	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x00);		// Standby mode

	I2C_ReadMultiRegisters(MMA8652FC_I2C_ADDRESS, OUT_X_MSB_REG, 6, AccelData);		// Read data output registers 0x01-0x06

	Xout_12_bit = ((short) (AccelData[0]<<8 | AccelData[1])) >> 4;		// Compute 12-bit X-axis acceleration output value
	Yout_12_bit = ((short) (AccelData[2]<<8 | AccelData[3])) >> 4;		// Compute 12-bit Y-axis acceleration output value
	Zout_12_bit = ((short) (AccelData[4]<<8 | AccelData[5])) >> 4;		// Compute 12-bit Z-axis acceleration output value

	X_offset = Xout_12_bit / 2 * (-1);		              // Compute X-axis offset correction value
	Y_offset = Yout_12_bit / 2 * (-1);		              // Compute Y-axis offset correction value
	Z_offset = (Zout_12_bit - SENSITIVITY_2G) / 2 * (-1); // Compute Z-axis offset correction value

	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, OFF_X_REG, X_offset);
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, OFF_Y_REG, Y_offset);
	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, OFF_Z_REG, Z_offset);

	I2C_WriteRegister(MMA8652FC_I2C_ADDRESS, CTRL_REG1, 0x01);		// Active mode again
}

/* accel_checkAcceleration()|accelerometer.c
*
*  Parameter:		X_val - zuletzt gemessene Beschleunigung in X-Achse
*                   Y_val - zuletzt gemessene Beschleunigung in Y-Achse
*                   Z_val - zuletzt gemessene Beschleunigung in Z-Achse
*
*  Return:			---
*
*  Beschreibung:	1)  Errechnet den Durchschnitt der letzten auf status_XYZ_cumulated kumulierten Werte
*                   2)  Vergleicht die zuletzt gemessenen Beschleunigungswerte mit den errechneten Durchschnittswerten der
*                       letzten Messungen
*                   3)  Setzt den status_count zur�ck, sofern die zuletzt gemessenen Werte die Durchschnittswerte �ber den Grenzbereich
*                       �bersteigen (Grenzbereich f�r die jeweiligen Variablen -> status_XYZ_threshold)
*
*/
void accel_checkAcceleration(float X_val, float Y_val, float Z_val){
    float X_avg = accel_X_cumulated/accel_nrCumulations;
    float Y_avg = accel_Y_cumulated/accel_nrCumulations;
    float Z_avg = accel_Z_cumulated/accel_nrCumulations;

    if ((X_val > X_avg + memory_x_threshold) || (X_val < X_avg - memory_x_threshold) || // variation above threshold?
        (Y_val > Y_avg + memory_y_threshold) || (Y_val < Y_avg - memory_y_threshold) || // added 29.07.16 by P
        (Z_val > Z_avg + memory_z_threshold) || (Z_val < Z_avg - memory_z_threshold)) {
        accel_no_move_count = 0;
    }
    else
        accel_no_move_count++;
}

/* accel_addValuesToCumVars()|accelerometer.c
*
*  Parameter:		X_val - zuletzt gemessene Beschleunigung in X-Achse
*                   Y_val - zuletzt gemessene Beschleunigung in Y-Achse
*                   Z_val - zuletzt gemessene Beschleunigung in Z-Achse
*
*  Return:			---
*
*  Beschreibung:	Bei jedem Aufruf wird der mitgegebene Beschleunigunswert vom Accelerator auf die jeweilige
*                   status_XYZ_cumulated Variable aufaddiert.
*
*/
void accel_addValuesToCumVars(float X_val, float Y_val, float Z_val) {
    accel_X_cumulated += X_val;
    accel_Y_cumulated += Y_val;
    accel_Z_cumulated += Z_val;
    accel_nrCumulations++;
}

/* accel_reset_cumVars()|accelerometer.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Setzt die Variablen, auf welche die hereinkommenden Beschleunigungswerte
*                   aufaddiert werden, zur�ck.
*/
void accel_reset_cumVars(void) {
    accel_X_cumulated = 0;
    accel_Y_cumulated = 0;
    accel_Z_cumulated = 0;
    accel_nrCumulations = 0;
}

// Interrupt Service Routine
void __attribute__ ((interrupt("IRQ"))) accel_isr(){

    //I2C_ReadRegister(MMA8652FC_I2C_ADDRESS, PULSE_SRC_REG);        // clear interrupt Accel
    //I2C_ReadRegister(MMA8652FC_I2C_ADDRESS, FF_MT_SRC_REG);        // clear interrupt Accel
    //I2C_ReadRegister(MMA8652FC_I2C_ADDRESS, PL_STATUS_REG);         // clear interrupt accel
    status_LED_toggl(1);
    I2C_ReadMultiRegisters(MMA8652FC_I2C_ADDRESS, OUT_X_MSB_REG, 192, AccelData_Fifo_12bit); // read data from fifo
    DataReady_FIFO = 1;
    //get_accel_data();
    EXTINT = (1<<3);        //  clear interrupt �C
    I2C_ReadRegister(MMA8652FC_I2C_ADDRESS, F_SETUP_REG);            // clear interrupt Accel
    VICVectAddr = 0;        // end of interrupt

}


