 /*
 * adcIntern.h
 *
 *  Created on: 19.11.2014
 *      Author: Hütter Stefan
 */

#ifndef ADCINTERN_H_
#define ADCINTERN_H_

void adcIntern_init(void);
int adcIntern_read_ADC1_2(void);
int adcIntern_read_ADC0_2(void);
int adcIntern_read_ADC0_4(void);


#endif //ADCINTERN_H_
