#ifndef PARAM_H_
#define PARAM_H_

extern double param_U_is_null;
extern double param_I_is_null;
extern double param_U_is_full;
extern int param_t_charge_ready;
extern int param_t_batt_full;
extern double param_U_zero;
extern double param_I_zero;

extern float param_ukal;
extern float param_ikal;
extern float param_lnachl;
extern float param_umax;
extern float param_imax;
extern float param_tmess; //Abstand zwischen zwei Ladepausen in Sekunden
extern float param_umin;	// Minimale Spannung, bei der die Ladung gestartet wird
extern float param_iminaz;
extern float param_imaxaz;
extern float param_ilade;

extern float param_qmax;
extern float param_tmax;
extern float param_wpumin;
extern float param_ierh;	// Strom des Erhaltungsimpulses in A
extern float param_terh;	// Dauer des Erhaltungsimpulses in sec
extern float param_qnachlad;	// prozentuelle Nachladung nach Wendepunkt imn Prozent der bisherigen Gesamtladung
extern float param_inachlad;
extern float param_uobenerh;
extern float param_uuntenerh;
extern float param_uvollbeginn;

extern float param_tinnenmax;
extern float param_pfaktor;
extern float param_ifaktor;
extern float param_terho;
extern float param_uerho;
extern float param_tperh;
extern float param_tmmin;
extern float param_capacity;
extern float param_tinn;
extern float param_imitf;

extern float param_kennk;
extern float param_kennd;
extern float param_klcmin;
extern float param_klcmax;
extern float param_dfaktor;
extern float param_tmain;
extern float param_sernr;
extern float param_uvoll;
extern float param_terhon;
extern float param_uerhon;

extern float param_tphase2break;
extern float param_tphase2load;
extern float param_phase2impulse;
extern float param_urisecontition;
extern float param_chargecounter;

extern char param_ecs;


void param_set_default(void);

//Parameter for Chager Controller
void param_set_param_chargerctrl(double uIsNull,double iIsNull,double uIsFull,double uZero, double iZero, int tChargeReady,int tBattFull);

//Parameter for Charger
//void param_set_param_charger();
void param_set_safetyshutdown(float umin, float umax, float imax, float qmax, float tmax, float tinn, float terho, float uerho);
void param_set_rectifer(float kennk, float kennd, float klcmin, float klcmax);
void param_set_PIDCurrent(float pfaktor, float ifaktor, float dfaktor);
void param_set_consImpuls(float ierh, float terh, float uuntenerh, float tperh);
void param_set_charger_general(float ilade, float  capacity,float tmain, float sernr,
                               float uvoll, float tphase2break, float tphase2load, char ecs);
void param_set_calib(float ukal, float ikal, float imitf, float tmess);
#endif // PARAM_H_
