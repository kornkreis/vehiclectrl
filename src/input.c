/*
* input.h
*
*  Created on: 18.11.2014
*      Author: Hütter Stefan
*/

#include "input.h"
#include "defines.h"
#include "LPC214x.h"

void input_init(void)
{
	IODIR1 &= 0xFF0FFFFF;//Alle als Input
	PINSEL0 &= 0xFFFF00FF;//Alle Inputleitungen als GPIO
	
}
unsigned char input_getCH(unsigned char ch)
{
	if(IOPIN1&(1<<(ch+20)))
		return 0;
	else
		return 1;
}