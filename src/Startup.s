
# Standard definitions of Mode bits and Interrupt (I & F) flags in PSRs

        .equ    Mode_USR,       0x10
        .equ    Mode_FIQ,       0x11
        .equ    Mode_IRQ,       0x12
        .equ    Mode_SVC,       0x13
        .equ    Mode_ABT,       0x17
        .equ    Mode_UND,       0x1B
        .equ    Mode_SYS,       0x1F

        .equ    I_Bit,          0x80    /* when I bit is set, IRQ is disabled */
        .equ    F_Bit,          0x40    /* when F bit is set, FIQ is disabled */

        .equ    Top_Stack,      0x40007fe0
        .equ    UND_Stack_Size, 0x00000004
        .equ    SVC_Stack_Size, 0x00000004
        .equ    ABT_Stack_Size, 0x00000004
        .equ    FIQ_Stack_Size, 0x00000100
        .equ    IRQ_Stack_Size, 0x00000100
        .equ    USR_Stack_Size, 0x00001000 /*orig 400*/


# Phase Locked Loop (PLL) definitions
        .equ    PLL_BASE,       0xE01FC080  /* PLL Base Address */
        .equ    PLLCON_OFS,     0x00        /* PLL Control Offset*/
        .equ    PLLCFG_OFS,     0x04        /* PLL Configuration Offset */
        .equ    PLLSTAT_OFS,    0x08        /* PLL Status Offset */
        .equ    PLLFEED_OFS,    0x0C        /* PLL Feed Offset */
        .equ    PLLCON_PLLE,    (1<<0)      /* PLL Enable */
        .equ    PLLCON_PLLC,    (1<<1)      /* PLL Connect */
        .equ    PLLCFG_MSEL,    (0x1F<<0)   /* PLL Multiplier */
        .equ    PLLCFG_PSEL,    (0x03<<5)   /* PLL Divider */
        .equ    PLLSTAT_PLOCK,  (1<<10)     /* PLL Lock Status */

/*
// <e> PLL Setup
//   <o1.0..4>   MSEL: PLL Multiplier Selection
//               <1-32><#-1>
//               <i> M Value
//   <o1.5..6>   PSEL: PLL Divider Selection
//               <0=> 1   <1=> 2   <2=> 4   <3=> 8
//               <i> P Value
// </e>
*/
        .equ    PLL_SETUP,      0
        .equ    PLLCFG_Val,     0x00000024

#
# remap register
#
        .equ    REMAP_REG,      0xE01FC040      /* remap register */


# Startup Code must be linked first at Address at which it expects to run.

        .text
        .arm



        .global _startup

		.global _app_entry
	    .global	_enableInterrupts

        .func   _startup
_startup:


# Exception Vectors
#  Mapped to Address 0.
#  Absolute addressing mode must be used.
#  Dummy Handlers are implemented as infinite loops which can be modified.

Vectors:        LDR     PC, Reset_Addr
                LDR     PC, Undef_Addr
                LDR     PC, SWI_Addr
                LDR     PC, PAbt_Addr
                LDR     PC, DAbt_Addr
                .word   0xB9205F84     		   /* This is  a correct  checksum do not touch */
                LDR     PC, [PC, #-0xFF0]	   /* Vector from VicVECAddr */
#                LDR     PC, IRQ_Addr
                LDR     PC, FIQ_Addr

Reset_Addr:     .word   Reset_Handler
Undef_Addr:     .word   undef_isr			/*Undef_Handler*/
SWI_Addr:       .word   SWI_Handler			/* Ganz unten */

PAbt_Addr:      .word   PAbt_Handler
DAbt_Addr:      .word   abt_isr				/*DAbt_Handler*/
                .word   0                      /* Reserved Address */
#IRQ_Addr:       .word   IRQ_Handler
FIQ_Addr:       .word   Undef_Handler	/*OWI_FIQ_Handler*/

Undef_Handler:  B       Undef_Handler
SWI_Handler:    B       SWI_Handler
PAbt_Handler:   B       PAbt_Handler
DAbt_Handler:   B       DAbt_Handler
IRQ_Handler:    B       IRQ_Handler         /* Use Vector from VicVECAddr */
#FIQ_Handler:    B       FIQ_Handler
#OWI_FIQ_Handler:	B	OWI_FIQ_Handler

# Reset Handler
.org	0x1fc
#			.word	0x87654321				/* Last fuse */
			.word	0xffffffff
			.word	0x12345678				/* Adresse der Pr�fsumme*/
			.word	0x87654321				/* Pr�fsumme */

#.org		0x4000
_app_entry:
Reset_Handler:
.if PLL_SETUP
                LDR     R0, =PLL_BASE
                MOV     R1, #0xAA
                MOV     R2, #0x55

# Configure and Enable PLL
                MOV     R3, #PLLCFG_Val
                STR     R3, [R0, #PLLCFG_OFS]
                MOV     R3, #PLLCON_PLLE
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]

# Wait until PLL Locked
PLL_Loop:       LDR     R3, [R0, #PLLSTAT_OFS]
                ANDS    R3, R3, #PLLSTAT_PLOCK
                BEQ     PLL_Loop

# Switch to PLL Clock
                MOV     R3, #(PLLCON_PLLE | PLLCON_PLLC)
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]

.endif

# Wait until halted after target reset
				LDR		R0, DELAY_COUNT
DelayLoop:
				sub		r0,r0,#1
				cmp		r0,#0
				BNE		DelayLoop

# Initialise Interrupt System
#  ...

#                LDR     R0, =REMAP_REG
#                LDR     R1, =0x00000002
#                STR     R1, [R0]

# Setup Stack for each mode

                LDR     R0, =Top_Stack

#  Enter Undefined Instruction Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_UND|I_Bit|F_Bit
                MOV     SP, R0
                SUB     R0, R0, #UND_Stack_Size

#  Enter Abort Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_ABT|I_Bit|F_Bit
                MOV     SP, R0
                SUB     R0, R0, #ABT_Stack_Size

#  Enter FIQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_FIQ|I_Bit|F_Bit
                MOV     SP, R0
                SUB     R0, R0, #FIQ_Stack_Size

#  Enter IRQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_IRQ|I_Bit|F_Bit
                MOV     SP, R0
                SUB     R0, R0, #IRQ_Stack_Size

#  Enter Supervisor Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_SVC|I_Bit|F_Bit
                MOV     SP, R0
#                SUB     R0, R0, #SVC_Stack_Size

#  Enter User Mode and set its Stack Pointer
#                MSR     CPSR_c, #Mode_USR
#                MOV     SP, R0

#  Setup a default Stack Limit (when compiled with "-mapcs-stack-check")
                SUB     SL, SP, #USR_Stack_Size


# Relocate .data section (Copy from ROM to RAM)
                LDR     R1, =_etext
                LDR     R2, =_data
                LDR     R3, =_edata
LoopRel:        CMP     R2, R3
                LDRLO   R0, [R1], #4
                STRLO   R0, [R2], #4
                BLO     LoopRel


# Clear .bss section (Zero init)
               MOV     R0, #0
                LDR     R1, =__bss_start__
                LDR     R2, =__bss_end__
LoopZI:         CMP     R1, R2
                STRLO   R0, [R1], #4
                BLO     LoopZI

# Enter the C code
                B       main

DELAY_COUNT:
				.word		0x10000

        .endfunc


###################################################
#
# enable IRQ and FIQ at core
#
###################################################

	.func		_enableInterrupts
_enableInterrupts:
 		stmfd	sp!, {r1}
        mrs		r1, CPSR
        bic		r1, r1, #I_Bit
        bic		r1, r1, #F_Bit
        msr		CPSR_c, r1
		ldmfd	sp!, {r1}
		mov		pc, r14
		.size   __enableInterrupts, . - __enableInterrupts
        .endfunc



/*
	.func SWI_Handler
SWI_Handler:
	stmfd sp!,{r0-r12,lr}  Store registers.
	ldr r0,[lr,#-4]  Calculate address of SWI instruction and load it into r0.
	bic r0,r0,#0xff000000  Mask off top 8 bits of instruction to give SWI number.
#
# Use value in r0 to determine which SWI routine to execute.
#
	//BL C_SWI_Handler  Call C routine to handle the SWI

	ldmfd sp!, {r0-r12,pc}^  Restore registers and return.
		.endfunc
*/
		.end
