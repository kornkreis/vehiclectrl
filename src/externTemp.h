/*
* externTemp.h
*
*  Created on: 22.11.2014
*      Author: Hütter Stefan
*/
#ifndef EXTERNTEMP_H_
#define EXTERNTEMP_H_

#define EXTERNTEMP_AD04 ((0<<19)|(1<<18)) //Select AD0.4 function for P0.4
#define EXTERNTEMP_SEL_AD04 (1<<4)
#define EXTERNTEMP_CLKDIV (3-1) // 4Mhz ADC clock (ADC_CLOCK=PCLK/CLKDIV) where "CLKDIV-1" is actually used , in our case PCLK=60mhz  
#define EXTERNTEMP_BURST_MODE_OFF (0<<16) // 1 for on and 0 for off
#define EXTERNTEMP_POWER_UP (1<<21) //setting it to 0 will power it down
#define EXTERNTEMP_START_NOW ((0<<26)|(0<<25)|(1<<24)) //001 for starting the conversion immediately
#define EXTERNTEMP_ADC_DONE (1<<31)

void externTemp_init(void);
int externTemp_read_ADC(void);
#endif