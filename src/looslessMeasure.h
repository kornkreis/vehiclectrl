/*
 * looslessMeasure.h
 *
 * Created on: 14.04.2015
 *      Author: Hütter Stefan
 */
 
#ifndef LOOSLESSMEASURE_H_
#define LOOSLESSMEASURE_H_

#define LOOSLESSMEASURE_DATA_OFFSET 0x010000 //Address where the data begins
#define LOOSLESSMEASURE_DATA_MUL 0x20 //Distanze between 2 measurements
#define LOOSLESSMEASURE_PAGE_SIZE 0x100 //Distanze between 2 measurements
#define LOOSLESSMEASURE_MEM_SIZE 0x1FFFFF //Speichergroesse
void looslessMeasure_init(void);
unsigned int looslessMeasure_save(	unsigned int index,
							unsigned int time,
							unsigned short ID,
							unsigned short U_sub,
							unsigned short U,
							unsigned short I,
							unsigned short onBoardTemp,
							unsigned short BattTemp,
							unsigned char Fuellstand);
							
unsigned int looslessMeasure_get(	unsigned int index,
							unsigned int *time,
							unsigned short *ID,
							unsigned short *U_sub,
							unsigned short *U,
							unsigned short *I,
							unsigned short *onBoardTemp,
							unsigned short *BattTemp,
							unsigned char *Fuellstand);
unsigned int looslessMeasure_calc_address_from_index(unsigned int index);
unsigned int looslessMeasure_calc_page_address_from_index(unsigned int index);
unsigned char looslessMeasure_calc_inner_page_index_from_index(unsigned int index);
unsigned char looslessMeasure_calc_flash_nr(unsigned int Addr);
#endif //LOOSLESSMEASURE_H_