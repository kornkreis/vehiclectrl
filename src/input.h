 /*
 * onBoardTemp.h
 *
 *  Created on: 17.01.2014
 *      Author: Hütter Stefan
 */

#ifndef INPUT_H_
#define INPUT_H_


void input_init(void);
unsigned char input_getCH(unsigned char ch);
#endif //INPUT_H_