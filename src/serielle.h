/*
 * serielle.h
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#ifndef SERIELLE_H_
#define SERIELLE_H_

#define SERIELLE_STATE_WAIT_FOR_START	0
#define SERIELLE_STATE_WAIT_FOR_LEN		1
#define SERIELLE_STATE_WAIT_FOR_DATA	2
#define SERIELLE_STATE_WAIT_FOR_CKSUM	3
#define SERIELLE_STATE_WAIT_FOR_ACK		4

#define SERIELLE_REC_ERROR_NO		0
#define SERIELLE_REC_ERROR_CKSUM	1
#define SERIELLE_REC_ERROR_NORET	2

#define SERIELLE_REC_BUSY	0
#define SERIELLE_REC_READY	1


#define SERIELLE_START_SIGN_RX '#'
#define SERIELLE_START_SIGN_TX	'>'


extern char serielle_text[1024];
extern unsigned char serielle_rec_len;
extern char serielle_rec_buffer[256];
extern unsigned char serielle_rec_ready;

void __attribute__ ((interrupt("IRQ"))) serielle_isr();
void serielle_init();
char serielle_putch(char nch);
char serielle_puttext(char *textp);
void serielle_send();
void serielle_write_in_buffer(unsigned char c);
void serielle_write_to_uart(char *buffer, unsigned char len);

#endif /* SERIELLE_H_ */
