/*
 * accessCtrl.c
 *
 *  Created on: 21.01.2011
 *      Author: Wolfgang H�tter
 */
#include "accessCtrl.h"
#include "memory.h"
#include "flashExtern.h"
#include "stringHelper.h"
#include "twn4.h"
#include <stdlib.h>

unsigned char AccCtrlActPermission = 0;
unsigned long DataVersion = 0;

/* memory_RFID_checkRFID()|accCtrl.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
unsigned char accCtrl_RFID_checkRFID(char *ID){
    unsigned char permission = 0;

    permission = memory_get_RFID_perm(ID);
    if(permission == 1)
        return ACCCTRL_ID_ALLOWED;
    else if (permission == 0)
        return ACCCTRL_ID_DENY;
    else
        return 2; //error occurred

}
