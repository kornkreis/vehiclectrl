/*
* communicate.c
*
*  Created on: 21.01.2015
*      Author: Hütter Stefan
*/
#include "serielleHandy.h"
#include "serielle.h"
#include "LPC214x.h"
#include "communicate.h"
#include "rtc.h"
#include "hardware.h"
#include "status.h"
#include <stdio.h>


char communicate_wait_for_ok=0;
char communicate_wait_for_ok_time=0;
int communicate_wait_for_ok_index=0;
int communicate_lost_msn=0;

/* communicate_init()|communicate.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert die Kommunikation auf Seite der Hardware (Serielle Schnittstelle)
*
*/
void communicate_init(void){
	serielle_init();
	serielleHandy_init();
}

/* communicate_measurementSendToPC()|communicate.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Sendet die Aktuellen Messwerte an den PC, zur Zeit nur Dummy
*
*/
void communicate_measurementSendToPC(void){

}

void communicate_adminmode_send_JSON(char *type,
                                     unsigned char action,
                                     unsigned long serialNr,
                                     char *ID,
                                     unsigned char permission){
	sprintf(serielleHandy_text, COMMUNICATE_JSON_ADMINMODE_FORMAT,type,action,serialNr,ID,permission);
	serielleHandy_send();

	sprintf(serielle_text, COMMUNICATE_JSON_ADMINMODE_FORMAT,type,action,serialNr,ID,permission);
	serielle_send();

    communicate_lost_msn++;
	communicate_wait_for_ok=1;
	communicate_wait_for_ok_time=0;
}

/* communicate_measurementSendJSON()|communicate.c
*
*  Parameter:		unsigned int measurementIndex --> Index der Messung
*					double supplyVoltage --> Spannung der Spannungsversorgung
*					double batteryVoltage --> Spannung der Batterie
*					double batteryCurrent --> Strom
*					double onboardTemp --> Temperatur auf der Platine
*					double batteryTemp --> Temperatur in der Batterie
*					double electrolyteLevel --> Level des Elektrolyts
*                   double loadAh --> Ah from Charge
*                   double loadAhCons --> Ah from Conservation Charge
*                   double loadKwh --> kWh from Charge
*                   double loadKwhCons --> kWh from Conservation Charge
*                   unsigned int state --> status
					unsigned int state_time --> status time
                    unsigned char status_changed --> if status changed since last transfer
*
*  Return:			---
*
*  Beschreibung:	Sendet die übergebenen Messwerte an das Handy (zZ auch an den PC) als JSON formatiert
*
*/
void communicate_measurementSendJSON(unsigned long serialNr,
										unsigned int time,
										unsigned int dataVersion,
										char *ID,
										double batteryVoltage,
										double batteryCurrent,
										double power,
										double loadAhCons,
										double loadKwh,
										double loadKwhCons,
										double onboardTemp,
										unsigned int state,
										unsigned int state_time,
										unsigned char status_changed,
										unsigned char status_movement){
	sprintf(serielleHandy_text, COMMUNICATE_JSON_FORMAT_STRING,
	serialNr,
	time,
	dataVersion,
	ID,
	batteryVoltage,
	batteryCurrent,
	power,
	loadAhCons,
	loadKwh,
	loadKwhCons,
	onboardTemp,
	state,
	state_time,
	status_changed,
	status_movement);

	serielleHandy_send();

	sprintf(serielle_text, COMMUNICATE_JSON_FORMAT_STRING,
	serialNr,
	time,
	dataVersion,
	ID,
	batteryVoltage,
	batteryCurrent,
	power,
	loadAhCons,
	loadKwh,
	loadKwhCons,
	onboardTemp,
	state,
	state_time,
	status_changed,
	status_movement);
	serielle_send();

    communicate_lost_msn++;
	communicate_wait_for_ok=1;
	communicate_wait_for_ok_time=0;
	//communicate_wait_for_ok_index=measurementIndex;

}

void communicate_measurementSendJSON_picture(unsigned long serialNr,
										unsigned int time,
										unsigned int dataVersion,
										char *ID,
										double batteryVoltage,
										double batteryCurrent,
										double loadAh,
										double loadAhCons,
										double loadKwh,
										double loadKwhCons,
										double onboardTemp,
										unsigned int state,
										unsigned int state_time,
										unsigned char status_changed,
										unsigned char status_movement){
	sprintf(serielleHandy_text, COMMUNICATE_JSON_FORMAT_PICTURE_STRING,
	serialNr,
	time,
	dataVersion,
	ID,
	batteryVoltage,
	batteryCurrent,
	loadAh,
	loadAhCons,
	loadKwh,
	loadKwhCons,
	onboardTemp,
	state,
	state_time,
	status_changed,
	status_movement,
	99);

	serielleHandy_send();

	sprintf(serielle_text, COMMUNICATE_JSON_FORMAT_PICTURE_STRING,
	serialNr,
	time,
	dataVersion,
	ID,
	batteryVoltage,
	batteryCurrent,
	loadAh,
	loadAhCons,
	loadKwh,
	loadKwhCons,
	onboardTemp,
	state,
	state_time,
	status_changed,
	status_movement,
	99);
	serielle_send();

    communicate_lost_msn++;
	communicate_wait_for_ok=1;
	communicate_wait_for_ok_time=0;
	//communicate_wait_for_ok_index=measurementIndex;

}


/* communicate_check_json_answer()|communicate.c
*
*  Parameter:		unsigned int index --> index der Antwort des Handys oder PCs
*
*  Return:			0--> wenn es gelöscht wurde
*					1--> wenn er nicht übereinstimmt
*
*  Beschreibung:	Vergleicht den index mit dem zu erwartenden Index,
*					Wenn beide gleich sind, werden die entsprechenden Flags gelöscht!
*
*/
int communicate_check_json_answer(unsigned int index){
    communicate_lost_msn = 0;
    return 1; // no control about index --> no looselessmeasure implemented

	/*if(index==communicate_wait_for_ok_index)
	{
		communicate_wait_for_ok_time=0;
		communicate_wait_for_ok=0;
		return 0;
	}
	else
		return 1;*/
}

/* communicate_check_communication()|communicate.c
*
*  Parameter:	---
*
*  Return:		---
*
*  Description: checks if communication is active
*
*/
void communicate_check_communication(void){
    if ( communicate_lost_msn >= COMMUNICATE_TIMEOUT )
    {
        communicate_lost_msn = 0;
        status_server_set(STATUS_SRV_NO_CONNECTION);
        //hardware_FT311Reset(1000);

        sprintf(serielleHandy_text, "ans: resetFTDI\n");
        serielleHandy_send();

        sprintf(serielle_text,"ans: resetFTDI\n");
        serielle_send();
    }
}

