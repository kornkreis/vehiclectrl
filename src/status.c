/*
* communicate.c
*
*  Created on: 30.06.2015
*      Author: H�tter Stefan
*/
#include "LPC214x.h"
#include "param.h"
#include "status.h"
#include "measurement.h"
#include "memory.h"
#include "onBoardTemp.h"
#include "accelerometer.h"
#include "twn4.h"
#include "serielle.h"
#include "stringHelper.h"
#include <stdio.h>

unsigned char status_status=STATUS_LOG_OFF;
unsigned char status_server=STATUS_SRV_NO_CONNECTION;

unsigned char status_movement=STATUS_MOVEMENT_STAND;
unsigned char status_movement_change=0;

unsigned int status_count=0;
unsigned int status_count_full=0;
unsigned int status_count_impulse=0;
unsigned int status_count_connect=0;
unsigned int status_count_server_connect=0;


void status_set(unsigned char status){
        if(status_status!=status){
            accel_no_move_count = 0;
            status_count=0;
            status_status=status;
            status_count_connect = 0;
            if(status==STATUS_ADMIN_MODE)
                str_cpy_RFID_ASCII(Twn4LastIDString,"null\0");
        }
}

void status_server_set(unsigned char status){
    status_server = status;
    status_count_server_connect=0;//R�cksetzen des server_counts, da davon ausgegangen wird, dass das Handy mit dem Server verbunden ist
}

/* status_inc_check_server_connect()|status.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Bei jedem Aufruf wird der Status_count_server_connect erh�ht und gecheckt
*                   ob die Schwelle schon erreicht wurde
*                   Wenn die Schwelle erreicht wurde, wird der status_server auf NO_CONNECTION gesetzt.
*
*/
void status_inc_check_server_connect(void){
    if(status_count_server_connect<STATUS_MAX_SRV_CONNECTION_COUNT)
       status_count_server_connect++;
    else
       status_server=STATUS_SRV_NO_CONNECTION;
}

unsigned char status_get_status(void){
    return status_status;
}

unsigned int status_get_status_count(void){
    return status_count;
}

void status_inc_count(void){
    status_count++;
}

void status_reset_count(void) {
    status_count = 0;
}

void status_deside(unsigned char idAllowed,double current, double voltage){
    status_check_error(current,voltage);

    switch(status_status){
    case STATUS_ADMIN_MODE:
        if (status_get_status_count() >= STATUS_MAX_TIME_ADMIN_MODE)
            status_set(STATUS_LOG_OFF);
        break;
    case STATUS_LOG_ON:
        if (Twn4IdChanged){
            if(!idAllowed)
                status_set(STATUS_LOG_OFF);
        }
        else if (accel_no_move_count > memory_status_log_off_delay)
            status_set(STATUS_LOG_OFF);
        break;
    case STATUS_LOG_OFF:
        if(Twn4IdChanged){
            if(idAllowed)
                status_set(STATUS_LOG_ON);
        }
        break;
    default:
        break;
    }
}

void status_movement_deside(unsigned int movementCount){
    if(movementCount>memory_movement_count_threshold){
        if(status_movement!=STATUS_MOVEMENT_STAND){
            status_movement_change=1;
            status_movement=STATUS_MOVEMENT_STAND;
        }
    }
    else{
        if(status_movement!=STATUS_MOVEMENT_MOVE){
            status_movement_change=1;
            status_movement=STATUS_MOVEMENT_MOVE;
        }
    }

}
unsigned char status_send_picture(unsigned char time){
    if(status_movement==STATUS_MOVEMENT_MOVE){
    //if(measurement_getAverageU()>50.0){
        if(time>=20||status_movement_change){
            status_movement_change=0;
            return 1;
        }
        else
            return 0;
    }
    else if(status_movement_change){
        status_movement_change=0;
        return 1;
    }
    else
        return 0;
}

void status_check_batt_full(double current, double voltage){

}

/* status_check_error()|status.c
*
*  Parameter:		current - current current
*                   voltage - current voltage
*
*  Return:			--- (uses status_set function)
*
*  Description: 	checks if current, voltage or temp makes an error
*                   current > 300A - cable from sensor broken
*                   current > 0 but voltage < 1
*                   temp > 70�C
*
*/
void status_check_error(double current, double voltage){

}
