/*
* param.c
*
*  Created on: .07.2015
*      Author: H�tter Stefan
*/

double param_U_is_null;
double param_I_is_null;
double param_U_is_full;
double param_U_zero;
double param_I_zero;
int param_t_charge_ready;
int param_t_batt_full;

float param_ukal;
float param_ikal;
float param_lnachl;
float param_umax;
float param_imax;
float param_tmess; //Abstand zwischen zwei Ladepausen in Sekunden
float param_umin;	// Minimale Spannung, bei der die Ladung gestartet wird
float param_iminaz;
float param_imaxaz;
float param_ilade;

float param_qmax;
float param_tmax;
float param_wpumin;
float param_ierh;	// Strom des Erhaltungsimpulses in A
float param_terh;	// Dauer des Erhaltungsimpulses in sec
float param_qnachlad;	// prozentuelle Nachladung nach Wendepunkt imn Prozent der bisherigen Gesamtladung
float param_inachlad;
float param_uobenerh;
float param_uuntenerh;
float param_uvollbeginn;

float param_tinnenmax;
float param_pfaktor;
float param_ifaktor;
float param_terho;
float param_uerho;
float param_tperh;
float param_tmmin;
float param_capacity;
float param_tinn;
float param_imitf;

float param_kennk;
float param_kennd;
float param_klcmin;
float param_klcmax;
float param_dfaktor;
float param_tmain;
float param_sernr;
float param_uvoll;
float param_terhon;
float param_uerhon;

float param_tphase2break;
float param_tphase2load;
float param_phase2impulse;
float param_urisecontition;
float param_chargecounter;

char param_ecs; // true if ECS is active --> Elektrolytumw�lzung

void param_set_default(void)
{
    param_U_is_null=5.0;
    param_I_is_null=10.0;
    param_U_is_full=20.0;
    param_U_zero = 0.0;
    param_I_zero = 0.0;
    param_t_charge_ready=10;
    param_t_batt_full=1800;

    param_ukal = 1.3;
    param_ikal = 1;
    param_lnachl = 0;
    param_umax = 105;
    param_imax = 85;
    param_tmess = 300;  //Abstand zwischen zwei Ladepausen in Sekunden
    param_umin = 20;	// Minimale Spannung, bei der die Ladung gestartet wird
    param_iminaz = 2;
    param_imaxaz = 60;
    param_ilade = 70;

    param_qmax = 600;
    param_tmax = 480;
    param_wpumin = 80;
    param_ierh = 30;	// Strom des Erhaltungsimpulses in A
    param_terh = 2;	// Dauer des Erhaltungsimpulses in sec
    param_qnachlad = 2;	// prozentuelle Nachladung nach Wendepunkt imn Prozent der bisherigen Gesamtladung
    param_inachlad = 50;
    param_uobenerh = 81.6;
    param_uuntenerh = 81;
    param_uvollbeginn = 92;

    param_tinnenmax = 60;
    param_pfaktor = 5;
    param_ifaktor = 0.001;
    param_terho = 1200;
    param_uerho = 0.05;
    param_tperh = 3600;
    param_tmmin = 240;
    param_capacity = 750;
    param_tinn = 60;
    param_imitf = 0.999;

    param_kennk = 18;
    param_kennd = -1160;
    param_klcmin = 400;
    param_klcmax = 800;
    param_dfaktor = 4500;
    param_tmain = 5;
    param_sernr = 1000000;
    param_uvoll = 95;
    param_terhon = 0;
    param_uerhon = 0;

    param_tphase2break = 0;
    param_tphase2load = 0;
    param_phase2impulse = 0;
    param_urisecontition = 0;
    param_chargecounter = 0;

    param_ecs = 0;
}

void param_set_param_chargerctrl(double uIsNull,double iIsNull,double uIsFull,double uZero, double iZero, int tChargeReady,int tBattFull)
{
    param_U_is_null = uIsNull;
    param_I_is_null = iIsNull;
    param_U_is_full = uIsFull;
    param_U_zero = uZero;
    param_I_zero = iZero;
    param_t_charge_ready = tChargeReady;
    param_t_batt_full = tBattFull;
}

void param_set_safetyshutdown(float umin, float umax, float imax, float qmax, float tmax, float tinn, float terho, float uerho)
{
    // Parameter zur Sicherheitsabschaltung
    param_umin = umin;      //Mindestspannung, die anliegen muss, damit das Ladeger�t zu laden beginnt. Wird diese w�hrend der Ladung unterschritten, wird sofort abgebrochen.
    param_umax = umax;      //Maximale Spannung: wird diese �berschritten, geht das Ladeger�t in eine Error Phase
    param_imax = imax;      //??? Wird nicht verwendet
    param_qmax = qmax;      //Maximale Ladung, die geladen werden kann. Wird diese �berschritten, so wird die Ladung beendet.
    param_tmax = tmax;      //Maximale Ladezeit (derzeit deaktiviert)
    param_tinn = tinn;      //Maximale Innentemperatur (derzeit deaktiviert)
    param_terho = terho;    //Wenn in dieser Zeit in der ersten Ladephase keine Spannungserh�hung auftritt, geht das Ladeger�t in eine Error Phase.
    param_uerho = uerho;    //Spannungserh�hung, die in einer gewissen Zeit, in der ersten Ladephase stattfinden muss. Sonst geht das Ladeger�t in eine Error Phase.
}

void param_set_rectifer(float kennk, float kennd, float klcmin, float klcmax)
{
    // Parameter f�r den Rectifier
    param_kennk = kennk;    //Parameter k f�r die Kennlinie am Rectifier (f�r den Stromregler)
    param_kennd = kennd;    //Parameter d f�r die Kennlinie am Rectifier (f�r den Stromregler)
    param_klcmin = klcmin;  //Minimale counts, die am Rectifier einstellbar sind
    param_klcmax = klcmax;  //Maximale counts, die am Rectifier einstellbar sind
}

void param_set_PIDCurrent(float pfaktor, float ifaktor, float dfaktor)
{
    // Parameter f�r den PID Regler f�r den Stromregler
    param_pfaktor = pfaktor;    //p-faktor des PID Reglers f�r den Stromregler
    param_ifaktor = ifaktor;    //i-faktor des PID Reglers f�r den Stromregler
    param_dfaktor = dfaktor;    //d-faktor des PID Reglers f�r den Stromregler
}

void param_set_consImpuls(float ierh, float terh, float uuntenerh, float tperh)
{
    // Parameter f�r den Erhaltungsimpuls
    param_ierh = ierh;			//Ladestrom f�r den Erhaltungsimpuls
    param_terh = 	terh;           //Dauer des Erhaltungsimpuls
    param_uuntenerh = uuntenerh;  //untere Spannung bei der automatisch ein Erhaltungsimpuls startet
    param_tperh = tperh;          //Zeit, nach der ein Erhaltungsimpuls auftritt.
}

void param_set_charger_general(float ilade, float  capacity,float tmain, float sernr,
                               float uvoll, float tphase2break, float tphase2load, char ecs)
{
    // allgemeine Parameter
    param_ilade = ilade;                //Ladestrom in der ersten Ladephase
    param_capacity = capacity;          //Nennkapazit�t der Batterie
    param_tmain = tmain;                //Zeitabstand in denen Logs gesendet werden
    param_sernr = sernr;                //Seriennummer der Ladeger�tes
    param_uvoll = uvoll;                //Spannung, bei der die Batterie "voll" ist. Bis zu dieser Spannung wird in der ersten Ladephase geladen.
    param_tphase2break = tphase2break;  //Zeit der Pause in der Nachladephase
    param_tphase2load = tphase2load;    //Impulsdauer in der Nachladephase
    param_ecs = ecs;                    //EUW
}

void param_set_calib(float ukal, float ikal, float imitf, float tmess)
{
    param_ukal = ukal;                  //Kalibrierwert Spannung (wird mit der gemessenen Spannung multipliziert)
    param_ikal = ikal;                  //Kalibrierwert Strom (wird mit dem gemessenen Strom multipliziert)
    param_imitf = imitf;                //charge_Iruhig = (charge_Iruhig*param_imitf + charge_Iist * (1 - param_imitf));
    param_tmess = tmess;                //Nachlaufzeit der Ladung (wozu wissen wir nicht genau)
}

/*
void param_set_param_charger()
{
    param_ukal = 1.3;
    param_ikal = 1;
    param_lnachl = 0;
    param_umax = 105;
    param_imax = 85;
    param_tmess = 300;  //Abstand zwischen zwei Ladepausen in Sekunden
    param_umin = 20;	// Minimale Spannung, bei der die Ladung gestartet wird
    param_iminaz = 2;
    param_imaxaz = 60;
    param_ilade = 70;

    param_qmax = 600;
    param_tmax = 480;
    param_wpumin = 80;
    param_ierh = 30;	// Strom des Erhaltungsimpulses in A
    param_terh = 2;	// Dauer des Erhaltungsimpulses in sec
    param_qnachlad = 2;	// prozentuelle Nachladung nach Wendepunkt imn Prozent der bisherigen Gesamtladung
    param_inachlad = 50;
    param_uobenerh = 81.6;
    param_uuntenerh = 81;
    param_uvollbeginn = 92;

    param_tinnenmax = 60;
    param_pfaktor = 5;
    param_ifaktor = 0.001;
    param_terho = 1200;
    param_uerho = 0.05;
    param_tperh = 3600;
    param_tmmin = 240;
    param_capacity = 750;
    param_tinn = 60;
    param_imitf = 0.999;

    param_kennk = 18;
    param_kennd = -1160;
    param_klcmin = 400;
    param_klcmax = 800;
    param_dfaktor = 4500;
    param_tmain = 5;
    param_sernr = 1000000;
    param_uvoll = 95;
    param_terhon = 0;
    param_uerhon = 0;

    param_tphase2break = 0;
    param_tphase2load = 0;
    param_phase2impulse = 0;
    param_urisecontition = 0;
    param_chargecounter = 0;
}
*/
