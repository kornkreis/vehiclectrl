/*
 * timer.h
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#ifndef TIMER_H_
#define TIMER_H_

extern unsigned int ft311ResetTime;

int sec_flag; // Flag, dass eine Sekunde vergangen ist
int sec2_flag;
int sec_counter;
unsigned int timer_wartezeit;
unsigned int timer_2_wartezeit;
unsigned int timer_3_wartezeit;
unsigned int timer_4_wartezeit;
unsigned int timer_ft311_wartezeit;
int timer_warte_flag;
int tick_flag;
unsigned int tick_counter;
unsigned int indep_sec; // immer laufender Sekundenz�hler

void __attribute__ ((interrupt("IRQ"))) timer0_isr(void);
void timer0_init(long ms,void *functpointer);

void timer_wd_init(void);
void timer_feed_watchdog(void);

int timer_warte(int ms);
void timer_warte_start();
void timer_warte_reset();
void timer_operate();

int timer_2_warte(int ms);
void timer_2_warte_start();
void timer_2_warte_reset();
void timer_2_operate();

int timer_3_warte(int ms);
void timer_3_warte_start();
void timer_3_warte_reset();
void timer_3_operate();

int timer_4_warte(int ms);
void timer_4_warte_start();
void timer_4_warte_reset();
void timer_4_operate();

int timer_ft311_warte(int ms);
void timer_ft311_warte_start();
void timer_ft311_warte_reset();
void timer_ft311_operate();

#endif /* TIMER_H_ */
