/*
* rtc.h
*
*  Created on: 29.12.2014
*      Author: Hütter Stefan
*/
#ifndef RTC_H_
#define RTC_H_

#include <time.h>

void rtc_init(void);
void rtc_getTimeDateStuct(struct tm * timeDate);
void rtc_getTimeString(char *time);
void rtc_getDateString(char *date);
void rtc_setDate(unsigned char day,unsigned char month,unsigned short year);
void rtc_setTime(unsigned char hour,unsigned char min,unsigned char sec);
#endif //RTC_H_