  /*
 * defines.h
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define FLASH_VERSION 8
#define FW_VERSION 0x100

#define TICK		1				//Millisekunden pro Timer Tick

/*Externe Temperatur Messung
#define DEF_EXTTEMP_RV		1000 	//Vorwiderstand des PT1000 in Ohm
#define DEF_EXTTEMP_R0		1000 	//Kaltwiederstand (bei 0Grad) des Fühlers
#define DEF_EXTTEMP_SUB_SPG	1100 	//Subtrahierspannung am Subtrahierer in mV
#define DEF_EXTTEMP_GAIN	2 		//Verstärkung des Verstärkers
#define DEF_EXTTEMP_K		1.007459487 //k...Kalibrierwerte für die Spannungsmessung in mV
#define DEF_EXTTEMP_D		-118.927634 //d...Kalibrierwerte für die Spannungsmessung in mV
*/


#endif /* DEFINES_H_ */
