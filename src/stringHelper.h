 /*
 * stringHelper.h
 *
 *  Created on: 15.11.2016
 *      Author: H�tter Wolfgang
 */

#ifndef STRINGHELP_H_
#define STRINGHELP_H_
#include <stdint.h>

#define FALSE   0
#define TRUE    1
//#define NULL ((void *)0)
#define MAX_STRING_LENGTH 200

int str_len(char *s);
unsigned char str_cmp(char *str,char *cmp, unsigned char length);
void str_cpy_RFID_arr(char *dest, char *src, char len);
void str_cpy_RFID_ASCII(char *dest, char *src);
void str_array_to_string(char *arr, char* str, unsigned char arrSize);
uint64_t str_string_to_uint64(char *str);
unsigned char str_is_readable(char *readString);
void str_string_to_INTarray(char *str, char *arr, unsigned char len);

#endif //STRINGHELP_H_
