#ifndef STATUS_H_INCLUDED
#define STATUS_H_INCLUDED

#define STATUS_LOG_OFF          0
#define STATUS_LOG_ON           1
#define STATUS_ADMIN_MODE       2

#define STATUS_MOVEMENT_STAND   0
#define STATUS_MOVEMENT_MOVE    1

#define STATUS_SRV_INIT          0
#define STATUS_SRV_WAIT          1
#define STATUS_SRV_READY         2
#define STATUS_SRV_NEXT_READY    3
#define STATUS_SRV_NO_CONNECTION 255

#define STATUS_MAX_SRV_CONNECTION_COUNT 120 //2 min when all seconds count
#define STATUS_MAX_TIME_ADMIN_MODE      120 //2 min

extern unsigned char status_status;
extern unsigned char status_last;
extern unsigned char status_server;
extern unsigned int status_count;

extern unsigned char status_movement;

// Status resets


void status_set(unsigned char status);
void status_inc_check_server_connect(void);
unsigned char status_get_status(void);
unsigned int status_get_status_count(void);
void status_inc_count(void);
void status_deside(unsigned char idAllowed, double current, double voltage);
void status_movement_deside(unsigned int movementCount);
unsigned char status_send_picture(unsigned char time);
void status_check_batt_full(double current, double voltage);
void status_check_error(double current, double voltage);
void status_reset_count(void);
void status_server_set(unsigned char status);

#endif // STATUS_H_INCLUDED
