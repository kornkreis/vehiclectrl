/*
* hardware.c
*
*  Created on: 01.07.2015
*      Author: Huetter Wolfgang
*/
#include "hardware.h"
#include "timer.h"
#include "LPC214x.h"

/* hardware_FT311Reset(int timeMS)|hardware.c
*
*  Parameter:		timeMS = reset time in ms
*
*  Return:			---
*
*  Description: resets the FT311 chip (low-active)
*
*
*/
void hardware_FT311Reset(int timeMS){
    timer_ft311_warte_start();
    ft311ResetTime = HW_FT311_RESET_SHORT;

    /*HW_FT311_RESET_CLR;
    timer_3_warte_start();
    while(timer_3_warte(timeMS)==0);
    HW_FT311_RESET_SET;*/
}

/* hardware_TWN4_reset(int timeMS)|hardware.c
*
*  Parameter:		timeMS = reset time in ms
*
*  Return:			---
*
*  Description:     TWN4 client (low-active)
*
*
*/
void hardware_TWN4_reset(int timeMS){
    HW_TWN4_RESET_CLR;
    timer_3_warte_start();
    while(timer_3_warte(timeMS)==0);
    HW_TWN4_RESET_SET;
    timer_3_warte_start();
    while(timer_3_warte(500)==0);
}
