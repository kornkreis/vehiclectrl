/*
 * timer.c
 *
 *  Created on: 03.01.2011
 *      Author: hab
 */

#include "timer.h"
#include "LPC214x.h"
#include "main.h"
#include "defines.h"


void (*timerMsCall)(void);
unsigned int ft311ResetTime = 0;

void timer0_init(long ms, void *functpointer) //	Timer 0 initialisieren auf periodischen Interrupt
{
	sec_counter = 0;
	sec_flag = 0;
	tick_flag = 0;

	T0TCR = 0x01; // Timer 0 enabled
	T0PR = 14; // prescaler fuer 15 MHz auf 1 MHz
	T0MR0 = 1000 * ms; // Match0 fuer 1 ms
	T0MCR = 0x02; // Interrupt und Reset on Match 0: Interrupt wird mit dem Main() eingeschaltet, damit Dallas beim Programmstart ausgelesen wird
	T0TCR = 0x01; //  Timer 0 enablen
	T0IR = 1;
	VICVectAddr1 = (unsigned long) timer0_isr; // TC0 Interrupt -> Vector 0
	VICVectCntl1 = 0x20 | 4; // TC0 Interrupt -> IRQ Slot 0
	VICIntEnable |= 1 << 4; // Enable TC0 Interrupt

	timerMsCall = functpointer;

	timer_warte_reset();

}

void timer_wd_init(){
    WDTC    = 0x7A1200; // for 2 seconds
    WDMOD   = 0x03;
	WDFEED  = 0xAA;
	WDFEED  = 0x55;
}

void timer_feed_watchdog(void){
	/* Reload the watchdog timer       */
	T0MCR   = 0x02;	// Timer Interrupt aus
	U0IER   = 0x00;	// UART 0 Interrupt aus
	WDFEED  = 0xAA;
	WDFEED  = 0x55;
	U0IER   = 0x01;
	T0MCR   = 0x03;

}

void __attribute__ ((interrupt("IRQ"))) timer0_isr(void){
	 //Hauptprogramm sichern
	pmd_irq = 0x10000000;

    //if (timerMsCall)
        timerMsCall();

    // Hier Execution Routine
	timer_operate();
	timer_2_operate();
	timer_3_operate();
	timer_4_operate();
	timer_ft311_operate();
	tick_counter++;
	tick_flag = 1;
	pmd_irq++;

	if(++sec_counter >= 1000 / TICK)
	{
		pmd_irq = 0x10000010;
		sec_counter = 0;
		indep_sec++;
		sec_flag = 1;
		pmd_irq = 0x10000011;

		//TEMP_CONV(ntc1);
		pmd_irq = 0x10000012;
	}

	// Temperatur messen


	T0IR = 1; 			// Clear Interrupt Flag
	VICVectAddr = 0; 	// Acknowledge Interrupt
	pmd_irq = 0x99999999;
}

int timer_warte(int ms){
	if((timer_wartezeit > ms / TICK) || (timer_wartezeit == 0))
	{
		timer_wartezeit = 0;
		return 1;
	}
	return 0;
}

void timer_warte_start(){
	timer_wartezeit = 1;
}

void timer_warte_reset(){
	timer_wartezeit = 0;
}

void timer_operate(){
	if(timer_wartezeit > 0)
			timer_wartezeit++;
}


int timer_2_warte(int ms){
	if((timer_2_wartezeit > (ms / TICK)) || (timer_2_wartezeit == 0))
	{
		timer_2_wartezeit = 0;
		return 1;
	}
	return 0;
}

void timer_2_warte_start(){
	timer_2_wartezeit = 1;
}

void timer_2_warte_reset(){
	timer_2_wartezeit = 0;
}

void timer_2_operate(){
	if(timer_2_wartezeit > 0)
			timer_2_wartezeit++;
}

int timer_3_warte(int ms){
	if((timer_3_wartezeit > (ms / TICK)) || (timer_3_wartezeit == 0))
	{
		timer_3_wartezeit = 0;
		return 1;
	}
	return 0;
}

void timer_3_warte_start(){
	timer_3_wartezeit = 1;
}

void timer_3_warte_reset(){
	timer_3_wartezeit = 0;
}

void timer_3_operate(){
	if(timer_3_wartezeit > 0)
			timer_3_wartezeit++;
}


int timer_4_warte(int ms){
	if((timer_4_wartezeit > (ms / TICK)) || (timer_4_wartezeit == 0))
	{
		timer_4_wartezeit = 0;
		return 1;
	}
	return 0;
}

void timer_4_warte_start(){
	timer_4_wartezeit = 1;
}

void timer_4_warte_reset(){
	timer_4_wartezeit = 0;
}

void timer_4_operate(){
	if(timer_4_wartezeit > 0)
			timer_4_wartezeit++;
}

int timer_ft311_warte(int ms){
	if((timer_ft311_wartezeit > (ms / TICK)) || (timer_ft311_wartezeit == 0))
	{
		timer_ft311_wartezeit = 0;
		return 1;
	}
	return 0;
}

void timer_ft311_warte_start(){
	timer_ft311_wartezeit = 1;
}

void timer_ft311_warte_reset(){
	timer_ft311_wartezeit = 0;
}

void timer_ft311_operate(){
	if(timer_ft311_wartezeit > 0)
			timer_ft311_wartezeit++;
}
