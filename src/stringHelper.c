/*
 * stringHelper.c
 *
 *  Created on: 15.11.2016
 *      Author: Wolfgang H�tter
 */

#include "stringHelper.h"
#include "twn4.h"

/* str_cmp()|stringHelper.c
*
*  Parameter:		str - string1
*                   cmp - string2
*                   length - length of string
*
*  Return:			---
*
*  Description:
*
*/
unsigned char str_cmp(char *str,char *cmp, unsigned char length){
    unsigned char i;

    for (i = 0; i < length;i++){
        if (str[i] != cmp[i])
            return FALSE;
    }
    return TRUE;
}

/*
*  str_len()|stringHelper.c
*
*  Parameter:		char *s
*
*  Return:			length of string
*
*/
int str_len(char *s){
	int i = 0;
	char *cp = s;
	for(i = 0; i < MAX_STRING_LENGTH; i++){
		if(*cp++ == 0)
			return i;
	}
	return 0;
}

/*
*  str_cpy_RFID_arr()|stringHelper.c
*
*  Parameter:
*
*  Return:
*
*/
void str_cpy_RFID_arr(char *dest, char *src, char len){
    //unsigned char len = str_len(src);
    unsigned char i;

    for(i=0; i < len; i++)
        dest[i] = src[i];

    for(i = len-1; i < TWN4_MAX_ID_SIZE; i++)
        dest[i] = 0x00;

}

/*
*  str_cpy_RFID_ASCII()|stringHelper.c
*
*  Parameter:
*
*  Return:
*
*/
void str_cpy_RFID_ASCII(char *dest, char *src){
    //unsigned char len = str_len(src);
    unsigned char i;

    for(i=0; i < TWN4_MAX_ID_SIZE*2; i++)
        dest[i] = src[i];
}

/*
*  str_array_to_string()|stringHelper.c
*
*  Parameter:		char *arr --> the array which should be convert to
*                   char *str
*                   uchar size --> the size of the array -> *2 = length of string
*
*  Return:
*
*  Description:     Converts an char array (e.g.:   arr[0] = 0xa3
*                                                   arr[1] = 0x2b
*                                                   arr[2] = 0xff)
*                   to a ASCII String --> str = "a32bff\0"
*
*/
void str_array_to_string(char *arr, char* str, unsigned char arrSize){
    unsigned char i;
    unsigned char sign;

    for(i = 0; i < arrSize; i++){
        sign = (arr[i] >> 4) & 0x0F;
        if (sign < 0x0A)
            str[(i*2)] = sign + 48; // dec 48 = ASCII 0
        else
            str[(i*2)] = sign + 55; // dec 65 = ASCII A

        sign = arr[i] & 0x0F;
        if (sign < 0x0A)
            str[(i*2)+1] = sign + 48; // dec 48 = ASCII 0
        else
            str[(i*2)+1] = sign + 55; // dec 65 = ASCII A
    }

    str[arrSize*2] = '\0';
}

void str_string_to_INTarray(char *str, char *arr, unsigned char len){
    unsigned char i;
    unsigned char sign;

    for(i = 0; i < len/2; i++){

        sign = str[i*2]; //0,2,4,6,8,
        if (sign < 0x3A)
            sign -= 48; //0x30
        else
            sign -= 55; //0x37

        arr[i] <<= 4;
        arr[i] |= ((uint64_t)sign);

        sign = str[(i*2)+1]; //1,3,5,7,9
        if (sign < 0x3A)
            sign -= 48;
        else
            sign -= 55;

        arr[i] <<=4;
        arr[i] |= ((uint64_t)sign);
    }
}

uint64_t str_string_to_uint64(char *str){
    unsigned char strLength = str_len(str)/2;//ID[0];//sizeof(ID)+1;
    unsigned char i;
    unsigned char sign;
    uint64_t uintStr = 0;

    for(i = 0; i < strLength; i++){

        sign = str[i*2]; //0,2,4,6,8,
        if (sign < 0x3A)
            sign -= 48; //0x30
        else
            sign -= 55; //0x37

        uintStr <<= 4;
        uintStr |= ((uint64_t)sign);

        sign = str[(i*2)+1]; //1,3,5,7,9
        if (sign < 0x3A)
            sign -= 48;
        else
            sign -= 55;

        uintStr <<=4;
        uintStr |= ((uint64_t)sign);
    }
    return uintStr;
}

/*
*  str_is_readable()|stringHelper.c
*
*  Parameter:       char *str --> the string to check
*
*  Return:
*
*  Description:     checks if str has only readable ASCII signs
*
*/
unsigned char str_is_readable(char *str){
    int i = 0;
	unsigned char length = str_len(str);

	for(i = 0; i < length; i++){
        if(str[i] < 0x20 || str[i] > 0x7F) // readable ASCIIs (=German signs)
            return 0;
	}
	return 1;
}

