 /*
 * onBoardTemp.h
 *
 *  Created on: 17.01.2014
 *      Author: Hütter Stefan
 */

#ifndef ONBOARDTEMP_H_
#define ONBOARDTEMP_H_


unsigned char onBoardTemp_init();
int onBoardTemp_getTemp();
#endif //ONBOARDTEMP_H_
