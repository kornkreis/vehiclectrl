/*
 * externADC.c
 *
 * Kümmert sich um den externen ADC für die Strommessung
 *
 *  Created on: 22.11.2014
 *      Author: Hütter Stefan
 */

#include "spi.h"
#include "defines.h"
#include "adcExtern.h"
#include "LPC214x.h"
#include "hardware.h"


/*
*  adcExtern_init()|adcExtern.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert den Externen ADC
*
*/
void adcExtern_init() // initialisiert und startet LTC
{
	HW_ADCEXT_SET_CS_A; //0x010000;  //CS_A
	HW_ADCEXT_SET_CS_D;
	HW_ADCEXT_SET_GPIO;
	HW_ADCEXT_SET_GPIO_DIR;
}


/*
*  adcExtern_read()|adcExtern.c
*
*  Parameter:		unsigned char channel --> 0: Spannungsmessung
*                                             1: Stromsensor
*
*  Return:			unsigned int --> Wert des ADCs
*
*  Beschreibung:	Liest den Wert des externen ADCs
*
*/
unsigned int adcExtern_read(unsigned char channel)
{
	int i=0;
	unsigned int val;

	SSPCR0 = 0x02cb;
	HW_ADCEXT_CLR_CS_A;
	while(i < 100)
		i++;

	HW_ADCEXT_SET_CS_A;
	i=0;
	while(i < 100)
		i++;

	HW_ADCEXT_CLR_CS_A;
	SSPDR=0xffff; //SSP Data Register - write to start conversation
	while (SSPSR & 0x10)
	{
	}
	while (!(SSPSR & 0x04))
	{
	} // wait for SPI free
	val = SSPDR;
	HW_ADCEXT_SET_CS_A;
    return val;
}
