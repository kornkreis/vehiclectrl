/*
 * looslessMeasure.c
 *
 * Created on: 14.04.2015
 *      Author: Hütter Stefan
 */

#include "looslessMeasure.h"
#include "flashExtern.h"
#include "serielle.h"
#include "LPC214x.h"

void looslessMeasure_init(void){

}

unsigned int looslessMeasure_save(	unsigned int index,
							unsigned int time,
							unsigned short ID,
							unsigned short U_sub,
							unsigned short U,
							unsigned short I,
							unsigned short onBoardTemp,
							unsigned short BattTemp,
							unsigned char Fuellstand){
	unsigned char flashBytes[256];
	unsigned int readAddr;
	unsigned int flashNr;
	unsigned char innerIndex;

	readAddr=looslessMeasure_calc_page_address_from_index(index);
	flashNr=looslessMeasure_calc_flash_nr(readAddr);
	readAddr%=LOOSLESSMEASURE_MEM_SIZE;
	flashExtern_readBytes(flashNr,readAddr, flashBytes, 255);
	innerIndex=looslessMeasure_calc_inner_page_index_from_index(index);

	flashBytes[innerIndex]=(unsigned char)(time%0x100);
	flashBytes[innerIndex+1]=(unsigned char)((time/0x100)%0x100);
	flashBytes[innerIndex+2]=(unsigned char)((time/0x10000)%0x100);
	flashBytes[innerIndex+3]=(unsigned char)((time/0x1000000)%0x100);

	flashBytes[innerIndex+4]=(unsigned char)(ID%0x100);
	flashBytes[innerIndex+5]=(unsigned char)((ID/0x100)%0x100);

	flashBytes[innerIndex+6]=(unsigned char)((U_sub)%0x100);
	flashBytes[innerIndex+7]=(unsigned char)((U_sub/0x100)%0x100);

	flashBytes[innerIndex+8]=(unsigned char)((U)%0x100);
	flashBytes[innerIndex+9]=(unsigned char)((U/0x100)%0x100);

	flashBytes[innerIndex+10]=(unsigned char)((I)%0x100);
	flashBytes[innerIndex+11]=(unsigned char)((I/0x100)%0x100);

	flashBytes[innerIndex+12]=(unsigned char)((onBoardTemp)%0x100);
	flashBytes[innerIndex+13]=(unsigned char)((onBoardTemp/0x100)%0x100);

	flashBytes[innerIndex+14]=(unsigned char)((BattTemp)%0x100);
	flashBytes[innerIndex+15]=(unsigned char)((BattTemp/0x100)%0x100);

	flashBytes[innerIndex+16]=(unsigned char)((Fuellstand)%0x100);

	flashExtern_writeEnable(flashNr);
	flashExtern_pageWrite( flashNr, readAddr,flashBytes, 255);
	return(readAddr);

}

unsigned int looslessMeasure_get(	unsigned int index,
							unsigned int *time,
							unsigned short *ID,
							unsigned short *U_sub,
							unsigned short *U,
							unsigned short *I,
							unsigned short *onBoardTemp,
							unsigned short *BattTemp,
							unsigned char *Fuellstand){
	unsigned char flashBytes[256];
	unsigned int readAddr;
	unsigned int flashNr;
	unsigned char innerIndex;


	readAddr=looslessMeasure_calc_page_address_from_index(index);
	flashNr=looslessMeasure_calc_flash_nr(readAddr);
	readAddr%=LOOSLESSMEASURE_MEM_SIZE;
	flashExtern_readBytes(flashNr,readAddr, flashBytes, 255);
	innerIndex=looslessMeasure_calc_inner_page_index_from_index(index);

	*time=((unsigned int)(flashBytes[innerIndex+3])*0x1000000)+((unsigned int)(flashBytes[innerIndex+2])*0x10000)+((unsigned int)(flashBytes[innerIndex+1])*0x100)+flashBytes[innerIndex];

	*ID=((unsigned short)(flashBytes[innerIndex+5])*0x100)+((unsigned short)(flashBytes[innerIndex+4]));

	*U_sub=((unsigned short)(flashBytes[innerIndex+6]))+((unsigned short)(flashBytes[innerIndex+7])*0x100);

	*U=((unsigned short)(flashBytes[innerIndex+8]))+((unsigned short)(flashBytes[innerIndex+9])*0x100);

	*I=((unsigned short)(flashBytes[innerIndex+10]))+((unsigned short)(flashBytes[innerIndex+11])*0x100);

	*onBoardTemp=((unsigned short)(flashBytes[innerIndex+12]))+((unsigned short)(flashBytes[innerIndex+13])*0x100);

	*BattTemp=((unsigned short)(flashBytes[innerIndex+14]))+((unsigned short)(flashBytes[innerIndex+15])*0x100);

	*Fuellstand=flashBytes[innerIndex+16];

	return(readAddr);
}

/* looslessMeasure_calc_address_from_index()|looslessMeasure.c
*
*  Parameter:		unsigned int index
*
*  Return:			unsigned int --> Adresse des Index im Speicher
*
*  Beschreibung:	Liefert die Adresse des Index im Speicher
*
*/
unsigned int looslessMeasure_calc_address_from_index(unsigned int index){
	return (index*LOOSLESSMEASURE_DATA_MUL)+LOOSLESSMEASURE_DATA_OFFSET;
}

/* looslessMeasure_calc_page_address_from_index()|looslessMeasure.c
*
*  Parameter:		unsigned int index
*
*  Return:			unsigned int --> Adresse der Seite auf dem der Index steht
*
*  Beschreibung:	Liefert die Adresse der Seite auf der der Index im Speicher steht
*
*/
unsigned int looslessMeasure_calc_page_address_from_index(unsigned int index){
	return (looslessMeasure_calc_address_from_index(index)/LOOSLESSMEASURE_PAGE_SIZE)*LOOSLESSMEASURE_PAGE_SIZE;
}

/* looslessMeasure_calc_inner_page_index_from_index()|looslessMeasure.c
*
*  Parameter:		unsigned int index
*
*  Return:			unsigned char --> index innerhalb der Seite
*
*  Beschreibung:	Liefert den Index innerhalb der Seite
*
*/
unsigned char looslessMeasure_calc_inner_page_index_from_index(unsigned int index){
	return looslessMeasure_calc_address_from_index(index)%LOOSLESSMEASURE_PAGE_SIZE;
}

/* looslessMeasure_calc_flash_nr()|looslessMeasure.c
*
*  Parameter:		unsigned int addr
*
*  Return:			unsigned char --> flashNr
*
*  Beschreibung:	Liefert die Nummer des Flashs auf dem die Adresse ist
*
*/
unsigned char looslessMeasure_calc_flash_nr(unsigned int Addr){
	if(Addr>LOOSLESSMEASURE_MEM_SIZE)
		return 1;
	else
		return 0;

}
