/*
* iic.c
*
*  Created on: 17.01.2014
*      Author: H�tter Stefan
*/

#include "iic.h"
#include "defines.h"
#include "LPC214x.h"
#include "status_led.h"

/* iic_init()|iic.c
*
*	Stefan H�tter 02.12.2014
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert den IIC-Bus
*
*/
void iic_init(void){
	PCONP|=0x80;//IIC einschalten
	PINSEL0 &= 0xFFFFFF5F;//GPIO 0.3 und 0.4 auf SDC und SDA umschalten
	PINSEL0 |= 0x00000050;//GPIO 0.3 und 0.4 auf SDC und SDA umschalten

	I20CONCLR = 1 << I2ENC;
	//I20CONCLR=(1<<I2ENC)|(1<<STAC)|(1<<SIC)|(1<<AAC);//I2ENC-->I2C interface disable bit | STAC-->Startflag clear bit | SIC-->Interupt Clear Bit | AAC-->Assert acknowledge Clear Bit
	//I20CONSET = 1 << I2EN; //I2EN-->I2C interface enable
	I20SCLH=375;//375;//750;//20;
	I20SCLL=375;//375;//750;//20;//clock divider

	I20CONCLR = 1 << AAC;
	I20CONCLR = 1 << SIC;
	I20CONCLR = 1 << STAC;
	I20CONSET = 1 << I2EN;
}


/* iic_write()|iic.c
*
*  Stefan H�tter 02.12.2014
*
*  Parameter:		unsigned char *buf --> Buffer mit den zu sendenten Bytes
*					unsigned count --> Anzahl der zu sendenten Bytes
*
*  Return:			0 --> alles Gut
*					1 --> Empf�nger antwortet nicht mit einem AcknowledgeBit
*
*  Beschreibung:	Sendet Bytes an ein IIC Device
*
*/
unsigned char iic_write(unsigned char addr, unsigned char *buf, unsigned char data_cnt){

    I20CONSET = 1 << STA;
    while(!(I20CONSET & (1<<SI)));
    if( I20STAT != IIC_STAT_STARTCON_TRANS)
    {
        iic_stop();
        return 88;
    }

    I20DAT = addr << 1;
    I20CONCLR = 0x20;
    I20CONCLR = 0x08;

    while(!(I20CONSET & (1<<SI)));
    if( I20STAT != IIC_STAT_SLA_W_TRANS_ACK_REC)
    {
        iic_stop();
        return 22;
    }

	while (data_cnt--)
	{
		I20DAT = *buf++;						//	load data into I2DAT-Register
		I20CONCLR = 0x08;

		while(!(I20CONSET & (1<<SI)));
		if(I20STAT!=IIC_STAT_DATA_TRANS_ACK_REC)
		{
		    iic_stop();
			return I20STAT;//33
		}
	}

    //I20CONCLR = 0x08;
	//I20CONSET = 1 << STO;
	I20CONCLR = (1<<STAC)|(1<<SIC);
	I20CONSET = (1<<STO);

	return IIC_ERROR_NO;
}

/* iic_read()|iic.c
*
*  Stefan H�tter 02.12.2014
*
*  Parameter:		---
*
*  Return:			ERROR
*
*  Description:	    Reads data_cnt bytes from iic
*
*/
unsigned char iic_read(unsigned char addr, unsigned char *data, unsigned char data_cnt){
    unsigned char i;
    //status_LED_set(2);
    // Start ausgeben
    I20CONSET = 1 << STA;

    // Auf Interrupt warten (Start fertig)
    while(!(I20CONSET & (1<<SI)));
    if( I20STAT != IIC_STAT_STARTCON_TRANS){
        iic_stop();
        return 11+1;
    }

    // Adresse ausgeben
    I20DAT = (addr << 1) + 1;
    I20CONCLR = 0x20; // reset start
    I20CONCLR = 0x08;


    // Auf Interrupt warten (ACK Adresse + Read)
    while(!(I20CONSET & (1<<SI)));
    if( I20STAT != IIC_STAT_SLA_R_TRANS_ACK_REC){
        iic_stop();
        return 22+1;
    }

    for (i = 0; i < data_cnt; i++){
        // Immer ACK bis auf letztes Byte
        if (i+1 < data_cnt){

            I20CONSET = 1 << AA;    // ACK ausgeben
            I20CONCLR = 0x08;       // reset interrupt

            // Auf Interrupt warten (RX & ACK fertig)
            while(!(I20CONSET & (1<<SI)));
            if( I20STAT != IIC_STAT_DATA_REC_ACK_REC){
                iic_stop();
                return (33+1+i);//FALSE;
            }
        }
        else{

            I20CONCLR = 0x04;   // NACK
            I20CONCLR = 0x08;   // reset interrupt

            // Auf Interrupt warten (RX & NACK fertig)
            while(!(I20CONSET & (1<<SI)));
            if( I20STAT  != IIC_STAT_DATA_REC_NO_ACK){
                iic_stop();
                return (33+1+i);//FALSE;
            }
        }
        //status_LED_toggl(2);
        data[i] = I20DAT;   // read data

        // *** for TWN4-Modul ***
        // TWN4-Modul: the transfer parameter for data_cnt == 255
        if (i == 0 && data_cnt == 255){
            data[i] >>= 4;
            data_cnt = data[i] + 1; // id length in first nibble of first byte. data = length + id
        }

    }

    iic_stop();
    //status_LED_clear(2);
    return IIC_ERROR_NO;
}

/* iic_stop()|iic.c
*
*  Wolfgang H�tter
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Set IIC to Stop
*
*/
void iic_stop(){
    I20CONCLR = 0x08;       // Interrupt ruecksetzen
    I20CONSET = 1 << STO;   // Stop
}
