/*
* cmd.c
*
*  Created on: 21.01.2015
*      Author: Hütter Stefan
*/
#include "serielleHandy.h"
#include "serielle.h"
#include "LPC214x.h"
#include "cmd.h"
#include "rtc.h"
#include "memory.h"
#include "measurement.h"
#include "communicate.h"
#include "looslessMeasure.h"
#include "stringHelper.h"
#include "hardware.h"
#include "status.h"
#include "param.h"
#include "main.h"
#include "adcIntern.h"
#include "externTemp.h"
#include "accelerometer.h"
#include "accessCtrl.h"
#include "timer.h"
#include "twn4.h"
#include "flashExtern.h"
#include <stdlib.h>
#include <time.h>

#include "stdio.h"

unsigned char cmd_ready;
unsigned char CmdChanged = 0;
unsigned char cmd_send_json = 0;
struct tm time_;

/* cmd_init()|cmd.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Beschreibung:	Initialisiert alle Variablen
*
*/
void cmd_init(void){
    cmd_ready = 0;
}

/* cmd_exec()|cmd.c
*
*  Parameter:		char *recBuffer --> Buffer in dem das Commando stehen sollte
*					char device --> Device von dem das Commando kommt
*						CMD_DEVICE_PC
*						CMD_DEVICE_HANDY
*
*  Return:			---
*
*  Beschreibung:	Führt die entsprechende Reaktion auf die Commandos auf
*
*/
void cmd_exec(char *recBuffer, unsigned char len, char device){
    char IDArr[TWN4_MAX_ID_SIZE];// = malloc(TWN4_MAX_ID_SIZE);
    unsigned char iHelp;
    unsigned char charHelp;
	char answerString[256];

	switch(recBuffer[0]){
            case CMD_SET_TIME:
                rtc_setTime(recBuffer[1],
                            recBuffer[2],
                            recBuffer[3]);
                cmd_answer("ans:OK\n",device);
		break;

            case CMD_SET_DATE:
                rtc_setDate(recBuffer[1],
                            recBuffer[2],
                            recBuffer[3]*0x100+recBuffer[4]);
		cmd_answer("ans:OK\n",device);
		break;

            case CMD_SET_SN:
                memory_sn_no=(recBuffer[1]<<24)+(recBuffer[2]<<16)+(recBuffer[3]<<8)+recBuffer[4];
		memory_paramSaveAllParams();
		memory_paramLoadAllParams();
		main_controller_type=memory_sn_no>>MAIN_TYPE_SN_BIT;
		cmd_answer("ans:OK\n",device);
		break;

            case CMD_CALIB:
                switch(recBuffer[1]){
                    case CMD_SENSOR_SUPPL_VOLTAGE:
                        break;
                    case CMD_SENSOR_BATT_VOLTAGE:
                        memory_calib_batt_u_k = (recBuffer[2] << 24) + (recBuffer[3] << 16) + (recBuffer[4] << 8) + recBuffer[5];
                        memory_calib_batt_u_d = (recBuffer[6] << 24) + (recBuffer[7] << 16) + (recBuffer[8] << 8) + recBuffer[9];
                        memory_paramSaveAllParams();
                        sprintf(answerString,   "ans:Buff2: %d, Buff3: %d, Buff4: %d, Buff5: %d, \
                                                 Buff6: %d, Buff7: %d, Buff8: %d, Buff9: %d\n",
                                            recBuffer[2],recBuffer[3],recBuffer[4],recBuffer[5],
                                            recBuffer[6],recBuffer[7],recBuffer[8],recBuffer[9]);
                    //sprintf(answerString,   "ans: K: %d, D: %d\n",memory_calib_batt_u_k,memory_calib_batt_u_d);

                        cmd_answer(answerString,device);
                        cmd_answer("ans:OK\n",device);
			break;
                    case CMD_SENSOR_BATT_CURREN:
                        memory_calib_batt_i_k = (recBuffer[2] << 24) + (recBuffer[3] << 16) + (recBuffer[4] << 8) + recBuffer[5];
                        memory_calib_batt_i_d = (recBuffer[6] << 24) + (recBuffer[7] << 16) + (recBuffer[8] << 8) + recBuffer[9];
                        memory_calib_batt_avg     = recBuffer[10];
                        memory_paramSaveAllParams();
                        sprintf(answerString, "Averaging y/n: %d\n", memory_calib_batt_avg);
                        cmd_answer(answerString,device);
                        cmd_answer("ans:OK\n",device);
			break;
			}
                    break;

        case CMD_JSON_GOT:
            communicate_check_json_answer((int)(recBuffer[1])*0x10000+(int)(recBuffer[2])*0x100+recBuffer[3]);
            break;
        case CMD_RESET_FT311:
            hardware_FT311Reset((int)(recBuffer[1]*0x100+recBuffer[2]));
            sprintf(answerString,"ans:FT311 Reset for %i ms\n",(int)(recBuffer[1]*0x100+recBuffer[2]));
            cmd_answer(answerString,device);
            break;
        case CMD_RESTART_CONTROLLER:
            sprintf(answerString,"ans:Restart Controller!\n");
            cmd_answer(answerString,device);
            WDTC = 0x100;       // min wd-time
            timer_feed_watchdog();    // and feed it
            break;
        case CMD_GET_FW_VERSION: // 0x24
            sprintf(answerString,"ans:Firmware Version: v%i.%02i\n",memory_fw_version/0x100,memory_fw_version%0x100);
            cmd_answer(answerString,device);
            //cmd_answer(answerString,CMD_DEVICE_PC);
            break;
        case CMD_GOTO_BOOTLOADER:
            sprintf(answerString,"ans:Goto Bootloader!\n");
            cmd_answer(answerString,device);
            interprog = 0x76543210;
            WDTC = 0x100;       // min wd-time
            timer_feed_watchdog();    // and feed it
            break;
        case CMD_SET_MOVEMENT_THRESHOLD:
            if(len==8)
            {
                memory_x_threshold=(float)(recBuffer[1]*0x100+recBuffer[2])/1000;
                memory_y_threshold=(float)(recBuffer[3]*0x100+recBuffer[4])/1000;
                memory_z_threshold=(float)(recBuffer[5]*0x100+recBuffer[6])/1000;
                memory_movement_count_threshold=recBuffer[7]*5;//alle 200ms wird der vergleichswert erhöt!
            }
            memory_paramSaveAllParams();
            sprintf(answerString, "ans: %f %f %f %lu\n", memory_x_threshold,
                    memory_y_threshold,
                    memory_z_threshold,
                    memory_movement_count_threshold);
            cmd_answer(answerString,device);
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_CHANGE_PERM:
            charHelp = recBuffer[1];  //length

            for(iHelp=0; iHelp < charHelp; iHelp++)
                IDArr[iHelp] = recBuffer[iHelp+2];

            str_array_to_string(IDArr,Twn4TempString,recBuffer[1]);
            Twn4LastIDAddress = memory_RFID_getMemAddr(Twn4TempString);
            str_cpy_RFID_arr(IDArr,IDArr,charHelp+1);
            //str_array_to_string(IDArr, Twn4TempString,charHelp);

            //accCtrl_RFID_change_perm(Twn4TempString,recBuffer[charHelp+2]);
            memory_RFID_saveRFID_perm(IDArr,recBuffer[charHelp+2]);

            //make timestamp on dataVersion
            rtc_getTimeDateStuct(&time_);
            memory_data_version = mktime(&time_);
            memory_paramSaveAllParams();

            AccCtrlActPermission = accCtrl_RFID_checkRFID(IDArr);
            if (status_status == STATUS_ADMIN_MODE){
                status_reset_count();
                CmdChanged = 1; //need reaction from main while
            }

            cmd_answer("ans:OK\n",device);
            sprintf(answerString,"changed permission (%d) for ID:%s\n",recBuffer[charHelp+2],Twn4TempString);
            cmd_answer(answerString,CMD_DEVICE_PC);
            break;
        case CMD_LOGOFF:
            status_set(STATUS_LOG_OFF);
            cmd_answer("ans:OK\n",device);
            cmd_answer("ans:Logged off\n",CMD_DEVICE_PC);
            break;
        case CMD_ADMIN_MODE:
            if(recBuffer[1])
                status_set(STATUS_ADMIN_MODE);
            else
                status_set(STATUS_LOG_OFF);

            sprintf(answerString,"ans:Admin Mode: %d\n",recBuffer[1]);
            cmd_answer(answerString,device);
            break;
        case CMD_SET_USER_VERSION:
            memory_data_version=(recBuffer[1]<<24)+(recBuffer[2]<<16)+(recBuffer[3]<<8)+recBuffer[4];

            sprintf(answerString,"memory_data_version: %lu\n",memory_data_version);
            memory_paramSaveAllParams();
            cmd_answer(answerString,CMD_DEVICE_PC);
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_INSTALL_APK:
            timer_ft311_warte_start();
            ft311ResetTime = HW_FT311_RESET_LONG;
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_CALIB_MODE:
            if (calib)
                calib = 0;
            else
                calib = 1;
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;

        case CMD_CALIB_WEIGHT:
            if (recBuffer[1] == 0x01)
                measurement_calib_weight0();
            else if (recBuffer[1] == 0x02)
                measurement_calib_weight_faktor((recBuffer[2]<<24) + (recBuffer[3]<<16) +
                                                (recBuffer[4]<<8) + recBuffer[5]);

            sprintf(answerString,"weight0: %.2f weightFaktor: %.2f\n",memory_calib_weight_faktor,memory_calib_weight0);
            cmd_answer(answerString,CMD_DEVICE_PC);

            memory_paramSaveAllParams();
            memory_paramLoadAllParams();

            sprintf(answerString,"weight0: %.2f weightFaktor: %.2f\n",memory_calib_weight_faktor,memory_calib_weight0);
            cmd_answer(answerString,CMD_DEVICE_PC);
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_ONOFF_DATASEND:
            if (main_send_on)
                main_send_on = 0;
            else
                main_send_on = 1;
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_GET_JSON:
            cmd_send_json = 1;
            sprintf(answerString,"ans:OK\n");
            cmd_answer(answerString,device);
            break;
        case CMD_DELETE_FLASH:
            sprintf(answerString,"ans:start to delete flash %d\n",recBuffer[1]);
            cmd_answer(answerString,CMD_DEVICE_PC);
            flashExtern_writeEnable(recBuffer[1]);
            flashExtern_bulkErase(recBuffer[1]);
            flashExtern_writeDisable(recBuffer[1]);
            sprintf(answerString,"ans:deleted flash %d\n",recBuffer[1]);
            cmd_answer(answerString,CMD_DEVICE_PC);
            cmd_answer(answerString,device);
            break;
        case CMD_SET_LOG_OFF_DELAY:
            if(len==3){
                memory_status_log_off_delay = (unsigned int)(recBuffer[1]<<8) + (recBuffer[2]);
                memory_paramSaveAllParams();

                sprintf(answerString,"ans:OK\n");
                cmd_answer(answerString,device);
            }

            break;
		default:
		    sprintf(answerString,"ans:Unknown Command: 0x%02x\n",recBuffer[0]);
		    cmd_answer(answerString,device);
		    cmd_answer(answerString,CMD_DEVICE_PC);
		break;
	}
    /*sprintf(answerString,"ans:%02x %02x %02x %02x %02x %02x %02x %02x\n",
                    recBuffer[0],recBuffer[1],recBuffer[2],recBuffer[3],recBuffer[4],recBuffer[5],recBuffer[6],recBuffer[7]);
    cmd_answer(answerString,CMD_DEVICE_PC);*/
    //free(IDArr);
}

/* cmd_answer()|cmd.c
*
*  Parameter:		unsigned char *recBuffer --> Buffer der geschickt werden soll
*					char device --> CMD_DEVICE_PC
*									CMD_DEVICE_HANDY
*
*  Return:			---
*
*  Beschreibung:	Sendet die Antwort an das gewählte Device.
*					Wenn CMD_DEVICE_HANDY gewählt ist, wird nur das erste Bytes des Buffers
*					auf CMD_OK geprüft und dann ein JSON encoded String geschickt mit
*					{"status":"OK"}
*					oder
*					{"status":"ERROR"} an das Handy geschickt
*
*/
void cmd_answer(char *recBuffer, char device){
	switch(device)
	{
		case CMD_DEVICE_PC:
			serielle_write_to_uart(recBuffer,str_len(recBuffer));
		break;

		case CMD_DEVICE_HANDY:
			serielleHandy_write_to_uart(recBuffer,str_len(recBuffer));
		break;
	}
}
