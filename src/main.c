/*
 * main.c
 *
 *  Created on: 03.01.2011
 *      Author: hab
 *      Edited by Gebrueder Huetter
 */

#ifndef INFINITY
#define INFINITY (1.0/0.0)
#endif

#ifndef NAN
#define NAN (0.0/0.0)
#endif

#include "main.h"
#include "hardware.h"
#include "measurement.h"
#include "timer.h"
#include "LPC214x.h"
#include "defines.h"
#include "serielle.h"
#include "status_led.h"
#include "iic.h"
#include "input.h"
#include "adcIntern.h"
#include "spi.h"
#include "adcExtern.h"
#include "externTemp.h"
#include "serielleHandy.h"
#include "accelerometer.h"

#include "MMA8652FC.h"
#include "rtc.h"
#include "memory.h"
#include "communicate.h"
#include "startup.h"
#include "cmd.h"
#include "looslessMeasure.h"
#include "status.h"
#include "param.h"
#include "flashExtern.h"
#include "onBoardTemp.h"
#include "twn4.h"
#include "sc16is7xx.h"
#include "accessCtrl.h"
#include "stringHelper.h"
#include <math.h>
#include <stdio.h>
#include <time.h>

//unsigned int time_;

struct tm time_;
unsigned int time5s;
unsigned int time1min;
unsigned int timePictureSend=0;
unsigned long unixTime;

unsigned char tempInit;
unsigned char status_last;
unsigned char calib;

//uint32_t rfidAddr;
//unsigned char rfidIDAllowed;
unsigned char main_controller_type=0;
unsigned char main_send_on = 1;
char nullstring[5] = {'n','u','l','l','\0'};

int main()
{
    // Controller Initialisierung
    void (*timerMsCall)(void);
    timerMsCall = &measurement_meas_every_ms;
    //timerMsCall = &main_sec_ctrl;

    int tick_flag_50;
    int tick_flag_200;

    char *idStr;
    unsigned char sc16is7xxAnswer;

    unsigned char rfidIDrec;
    rfidIDrec = 0;
    /**
    * Initialisierung der Processorregister
    */
    MAMTIM = 0x04; // Flash cyclen
    MAMCR = 0x2; // MAM fully enabled

    /**
    * Initialisierung der Clocks
    */
    PLLCFG = 0x24; // von 12 auf 60 MHz
    PLLCON = 0x03; // PLL = clock source
    PLLFEED = 0xAA;
    PLLFEED = 0x55;
    VPBDIV = 0x00; // Peripheral clock 1/4 of Processor Clock PCLK = 15 MHz

    /**
    * Enable Peripheral Functions
    */
    PCONP = 0x0010061E; // HAB Enable Tim0, Tim1, I2C0, RTC, USB ist AUS, Uart0, ADC1, SSP

    /**
    * Configure Digital I/Os
    */
    PINSEL0 = 0x0F333025; // Funktionen w?hlen
    PINSEL1 = 0x000802A8;
    PINSEL2 = 0x00000000;
    IODIR0 = 0xF1FB0235; // neue Platine --> RST_FTDI auf 1 - 21.04.2015


    // --- *** DEFINED PIN SELECTION *** ---
    IODIR1 = 0xFF01D000;//0x00FF0000;

    IODIR1 = (1<<16)|(1<<17)|(1<<18)|(1<<22)|(1<<23)|(1<<24)|(1<<26)|(1<<27);//|(1<<25); // SET PINS as outputs
    IOCLR1 = (1<<16)|(1<<17)|(1<<18)|(1<<22)|(1<<23)|(1<<24)|(1<<28)|(1<<29)|(1<<30)|(1<<31);//|(1<<25); // CLEAR PINS

   /* IOCLR1 = (1<<24)|(1<<25);
    IOCLR1 =;*/
    // --- *** *** *** ---

    timer0_init(TICK,timerMsCall);
    timer_wd_init();
    // Ende Controller Initialisierung

    communicate_init();
    /**
    * Initialisierung des Default Interrupt Vectors
    */
    void isr_Default(void);
    VICDefVectAddr = (unsigned int) isr_Default;

    /**
    * Interrupts einschalten
     */
    _enableInterrupts();  //TODO von Assemblerfile Deklaration holen
    T0MCR = 0x03;	// Interrupt einschalten
    pmd_irq  = 0;

    status_LED_init();
    status_LED_enable();
    param_set_default();

    flashExtern_init();
    iic_init();
    //MMA8652FC_Init();
    MMA8652FC_Calibration();
    tempInit = onBoardTemp_init();
    spi_init();
    adcExtern_init();
    adcIntern_init();
    rtc_init();
    sc16is7xxAnswer = sc16is7xx_init();
    twn4_init();

    memory_sn_no = 0;
    memory_paramLoadAllParams();

    if(FLASH_VERSION!=memory_flash_version){
        memory_flash_version=FLASH_VERSION;
        memory_paramSaveAllParams();
	/** IMPLEMENT THIS: **/
	//status_action_handling(STAT_ACT_NEW_FW);
    }

    memory_fw_version = FW_VERSION;


    sprintf(serielle_text, "Flash Version: v%i.%02i\r\n",memory_flash_version/0x100,memory_flash_version%0x100);
    serielle_send();
    sprintf(serielle_text, "Firmware Version: v%i.%02i\r\n",memory_fw_version/0x100,memory_fw_version%0x100);
    serielle_send();
    sprintf(serielle_text, "S/N: %lu\r\n",memory_sn_no);
    serielle_send();
    sprintf(serielle_text, "sc16is7 answer: %d\n",sc16is7xxAnswer);
    serielle_send();

    //prepare LEDs
    status_LED_control(main_controller_type,0,0);
    status_LED_set(3);

    sec2_flag = 0;
    tick_flag_50 = 0;
    tick_flag_200 = 0;

    idStr = nullstring;
    str_cpy_RFID_ASCII(Twn4LastIDString,nullstring);

    ft311ResetTime = 0;
    calib = 0;
    //HW_RELAIS_1_SET; // for pressure sensor
    // Main loop
    while(1)
    {   
        if(status_status == STATUS_LOG_ON)
            HW_RELAIS_1_SET;
        else
            HW_RELAIS_1_CLR;


        /** ---------- COMMUNICATION --------- **/
	if(serielle_rec_ready){
            cmd_exec(serielle_rec_buffer,serielle_rec_len,CMD_DEVICE_PC);
            serielle_rec_ready=0;
	}
	if(serielleHandy_rec_ready){
            cmd_exec(serielleHandy_rec_buffer,serielleHandy_rec_len,CMD_DEVICE_HANDY);
			serielleHandy_rec_ready=0;
	}


        /** Every millisecond **/
	if(tick_flag == 1){
            T0MCR = 0x02;
            tick_flag = 0;
            tick_flag_50++;
            tick_flag_200++;
            T0MCR = 0x03;

            status_deside(AccCtrlActPermission,0,0);
            main_led_control();
	}

        /** Every 50 milliseconds **/
        if (tick_flag_50 == 50){
            tick_flag_50 = 0;

            get_accel_data();
            accel_addValuesToCumVars(Xout_g, Yout_g, Zout_g);

            // Check if status has changed
            if (((status_last != status_status) ||
                 Twn4IdChanged) ||
                 CmdChanged){
                if(status_status != STATUS_ADMIN_MODE){
                    status_deside(AccCtrlActPermission,0,0);
                    communicate_measurementSendJSON(memory_sn_no,unixTime,memory_data_version,
                                                Twn4LastIDString,measurement_getAverageU(),measurement_getI(),
                                                AccCtrlActPermission,0,
                                                0,0,
                                                0,status_status,status_count, 1,status_movement);
                }
                else
                    communicate_adminmode_send_JSON("user",ACCCTRL_SEND_INFO,memory_sn_no,Twn4LastIDString,AccCtrlActPermission);
                
                status_last = status_status;
                Twn4IdChanged = 0;
                CmdChanged = 0;
            }

            // if a command "send_json" got
            if (cmd_send_json){
                communicate_measurementSendJSON(memory_sn_no,unixTime,memory_data_version,
                                                Twn4LastIDString,measurement_getAverageU(),measurement_getAverageI(),
                                                measurement_getAverageP(),measurement_getPressure(),
                                                twn4online,0,
                                                (double)onBoardTemp_getTemp()/10,status_status,status_count,0,status_movement);
                cmd_send_json = 0;
            }
            //measurement_buildSum();
        }

       /** Every 200 milliseconds **/
        if (tick_flag_200 == 200) {
            tick_flag_200 = 0;
            // Check if vehicle has moved
            accel_checkAcceleration(Xout_g, Yout_g, Zout_g);
            accel_reset_cumVars();
            measurement_buildSum();

            /** Check if message is in sc16is7xx buffer*/
            if( twn4_check_waiting_message() )
                twn4_handle_message();

            status_movement_deside(accel_no_move_count);

            /*if ( status_send_picture(timePictureSend) ){
                timePictureSend=0;
                sprintf(serielleHandy_text,"cmd:%d\n",CMD_M_TAKE_PIC);
                serielleHandy_send();
            }*/

            // only if there is a connection to mobile - otherwise too much data to get a connection
            /*if (communicate_lost_msn<2)
            {
                // for alexs special view!!
                sprintf(serielle_text, "mov: X: %.3f, Y: %.3f, Z: %.3f\n", Xout_g, Yout_g, Zout_g);
                serielle_send();
            }*/

            if (calib && (communicate_lost_msn<2))
            {
                sprintf(serielleHandy_text, "weight:%.2f\n", measurement_getWeight());
                serielleHandy_send();
            }
        }

	/** Every second **/
        if(sec_flag == 1){
            sec_flag = 0;
            timePictureSend++;
            timer_feed_watchdog();
            time5s++;
            rtc_getTimeDateStuct(&time_);
            unixTime = mktime(&time_);
            status_inc_check_server_connect();
            status_inc_count();

            if (timer_ft311_warte(ft311ResetTime)){
                timer_ft311_warte_reset();
                //HW_FT311_RESET_SET;
                HW_FT311_RESET_CLR;
            }else{
                //HW_FT311_RESET_CLR;
                HW_FT311_RESET_SET;
            }
	}

         /** Every five seconds **/
         if(time5s == 5){
            time5s = 0;
            time1min++;
            //twn4online = twn4_check_online();

            if (twn4online == TWN4_OFFLINE)
                twn4_init();

            measurement_calcAverage();
            mktime(&time_);
            // *** Communication ***
            if(main_send_on){
                if (status_status != STATUS_ADMIN_MODE){
                    communicate_measurementSendJSON(memory_sn_no,unixTime,memory_data_version,
                                                    Twn4LastIDString,measurement_getAverageU(),measurement_getAverageI(),
                                                    measurement_getAverageP(),measurement_getPressure(),//AccCtrlActPermission->power
                                                    twn4online,0,
                                                    (double)onBoardTemp_getTemp()/10,status_status,status_count,0,status_movement);
                }
                else
                    communicate_adminmode_send_JSON("user",ACCCTRL_SEND_INFO,memory_sn_no,Twn4LastIDString,AccCtrlActPermission);
            }

            //sc16is7xx_send_char_to_uart(CHANA,0x41);
            /*COMMUNICATE_JSON_FORMAT_STRING
            serialNr, time, measurementIndex, *ID, batteryVoltage, batteryCurrent, loadAh, loadAhCons,
            loadKwh, loadKwhCons, onboardTemp, state, state_time, status_changed*/
            communicate_check_communication();
        }
    } // end endless while
}

/**
* Interrupt Handler
*/
void isr_Default(void){
    //PUTCH('U');
    /* clear interrupt in VIC*/
    VICVectAddr = 0;
    /**
    * ACHTUNG: Dokumentation hier falsch: Default Handler und Millisekunden Handler sind verknuepft!!!
    */
}

void __attribute__ ((interrupt("ABORT"))) abt_isr(void)
{
    unsigned long *lnk_ptr;
    __asm__ __volatile__ (
        "sub lr, lr, #8\n"
        "mov %0, lr" : "=r" (lnk_ptr)
    );
}

void __attribute__ ((interrupt("UNDEF"))) undef_isr(void)
{
    unsigned long *lnk_ptr;

    __asm__ __volatile__ (
        "sub lr, lr, #8\n"
        "mov %0, lr" : "=r" (lnk_ptr)
    );
}

/*  main_manage_status()|main.c
*
*  Parameter:
*
*  Return:			---
*
*  Description: 	this function is for main - manages everything with status:
*                   - LED control
*                   - communication to mobile phone
*
*
*/
void main_manage_status(){

}

/* main_led_ctrl()|main.c
*
*  Parameter:       ---
*
*  Return:			---
*
*  Description: 	call every second
*					flashes onboard leds
*
*
*/
void main_led_control(void){
    if(sec_counter%1000){
        if ((sec_counter < 20))
            status_LED_set(3);
        else
            status_LED_clear(3);
    }
}

void cleanBuffer(char *buffer){
    int i;
    for (i = 0; i < sizeof(buffer); i++)
        buffer[i] = 0;
}
