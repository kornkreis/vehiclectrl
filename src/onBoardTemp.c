/*
* onBoardTemp.h
*
*  Created on: 17.01.2014
*      Author: Hütter Stefan
*/

#include "iic.h"
#include "onBoardTemp.h"
#include "defines.h"
#include "LPC214x.h"

unsigned char onBoardTemp_init()
{
	unsigned char messages[2];
	unsigned char errNr;

    iic_init();
	//auf 12bit umstellen
	messages[0]=0x01;
	messages[1]=0x60;
	if((errNr = iic_write(0x48,messages,2)) != 0)
		return errNr;

	//Auf "Temperatur lesen" umstellen
	messages[0]=0;
	if((errNr = iic_write(0x48,messages,1)) != 0)
		return errNr;
	return 0;
}

/*
*  onBoardTemp_getTemp()|onBoardTemp.c
*
*  Parameter:		---
*
*  Return:			int Temperatur in 10tel Grad 15 --> 1,5°C
*
*  Beschreibung:	Liest die Temperatur aus und liefert sie in 10tel Grad
*
*/
int onBoardTemp_getTemp()
{
    unsigned char data[1];
	unsigned long temp;
	int iTemp;
	double dTemp;

	if (iic_read(0x48, data, 2) != 0)
        return 10;

	temp=data[0]*0x10;
	temp+=data[1]/0x10;
	if(temp&0x800)//negativ
	{
		iTemp=temp&(~(0x0800));
		iTemp|=0xFFFFF800;
	}
	else
		iTemp=temp;

	dTemp = iTemp;
	dTemp *= 0.0625;
	return (int)(dTemp*10);
}
