
 /*
 * accessCtrl.h
 *
 *  Created on: 22.03.2016
 *      Author: H�tter Wolfgang
 */

#ifndef ACCESSCTRL_H_
#define ACCESSCTRL_H_

#define ACCCTRL_ID_ALLOWED  1
#define ACCCTRL_ID_DENY     0

#define ACCCTRL_SEND_INFO   2 //for JSON, 0->add, 1->delete, 2-> info but 0 and 1 never used - only from app

extern unsigned char AccCtrlActPermission;

unsigned char accCtrl_RFID_checkRFID(char *ID);
unsigned char accCtrl_RFID_change_perm(char *ID, unsigned char permission);

#endif //ACCESSCTRL_H_
