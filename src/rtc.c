/*
* rtc.c
*
*  Created on: 29.12.2014
*      Author: H�tter Stefan
*/
#include "rtc.h"
#include "LPC214x.h"
#include <time.h>
#include <stdio.h>


/*
*  rtc_init()|rtc.c
*  
*  Parameter:		---
* 
*  Return:			---
*
*  Beschreibung:	Initialisiert die rtc.
*				
*
*/
void rtc_init(void)
{
	PCONP |= (1<<9);
	RTC_CCR=0x11;
}

/*
*  rtc_getTimeDateStuct()|rtc.c
*  
*  Parameter:		struct tm *timeDate ... pointer auf das struct f�r Time und Date
* 
*  Return:			---
*
*  Beschreibung:	Liefert uhrzeti und Datum auf eine timeStruct
*				
*
*/
void rtc_getTimeDateStuct(struct tm * timeDate)
{
	timeDate->tm_year = RTC_YEAR - 1900;
	timeDate->tm_mon = RTC_MONTH-1;
	timeDate->tm_mday = RTC_DOM;
	timeDate->tm_hour = RTC_HOUR;
	timeDate->tm_min = RTC_MIN;
	timeDate->tm_sec = RTC_SEC;
	timeDate->tm_isdst = -1;
	
}

/*
*  rtc_getDateString()|rtc.c
*  
*  Parameter:		char *date ... pointer f�r den String TT.MM.JJJJ mindestens 11 bytes gro�
* 
*  Return:			---
*
*  Beschreibung:	Liefert den definierten Datumsstring des aktuellen Datums.
*				
*
*/
void rtc_getDateString(char *date)
{
	sprintf(date,"%02u.%02u.%04u",(unsigned int)(RTC_DOM),(unsigned int)(RTC_MONTH),(unsigned int)(RTC_YEAR));
}

/*
*  rtc_getTimeString()|rtc.c
*  
*  Parameter:		char *time ... pointer f�r den String hh:mm:ss mindestens 9 bytes gro�
* 
*  Return:			---
*
*  Beschreibung:	Liefert den definierten Zeitstring der aktuellen Zeit
*				
*
*/
void rtc_getTimeString(char *time)
{
	sprintf(time,"%02u:%02u:%02u",(unsigned int)(RTC_HOUR),(unsigned int)(RTC_MIN),(unsigned int)(RTC_SEC));
}

/*
*  rtc_setDate()|rtc.c
*  
*  Parameter:		unsigned char day	... Tag
*					unsigned char month	... Monat
*					unsigned char year	... Jahr
* 
*  Return:			---
*
*  Beschreibung:	Setzt das Datum.
*				
*
*/
void rtc_setDate(unsigned char day,unsigned char month,unsigned short year)
{
	RTC_DOM=day;
	RTC_MONTH=month;
	RTC_YEAR=year;
}

/*
*  rtc_setTime()|rtc.c
*  
*  Parameter:		unsigned char hour ... Stunde
*					unsigned char min ... Minute
*					unsigned char sec ... Sekunde
* 
*  Return:			---
*
*  Beschreibung:	Setzt die Zeit.
*				
*
*/
void rtc_setTime(unsigned char hour,unsigned char min,unsigned char sec)
{
	RTC_HOUR=hour;
	RTC_MIN=min;
	RTC_SEC=sec;

}