/*
* communicate.h
*
*  Created on: 21.01.2015
*      Author: Hütter Stefan
*/
#ifndef COMMUNICATE_H_
#define COMMUNICATE_H_

#define COMMUNICATE_MEASUREMENT_INDEX_MAX	129022
#define COMMUNICATE_JSON_FORMAT_STRING	"{\
\"srnr\":%lu,\
\"time\":%i,\
\"dataVersion\":%i,\
\"RFID\":\"%s\",\
\"voltage\":%.2f,\
\"current\":%.1f,\
\"power\":%.1f,\
\"load_ah_cons\":%.1f,\
\"load_kwh\":%.1f,\
\"load_kwh_cons\":%.1f,\
\"temperature\":%.1f,\
\"state\":%i,\
\"stateTime\":%i,\
\"status_changed\":%i,\
\"status_movement\":%i\
}\n"

#define COMMUNICATE_JSON_FORMAT_PICTURE_STRING "{\
\"srnr\":%lu,\
\"time\":%i,\
\"dataVersion\":%i,\
\"RFID\":\"%s\",\
\"voltage\":%.2f,\
\"current\":%.1f,\
\"power\":%.1f,\
\"load_ah_cons\":%.1f,\
\"load_kwh\":%.1f,\
\"load_kwh_cons\":%.1f,\
\"temperature\":%.1f,\
\"state\":%i,\
\"stateTime\":%i,\
\"status_changed\":%i,\
\"status_movement\":%i,\
\"action\":%i\
}\n"

#define COMMUNICATE_JSON_ADMINMODE_FORMAT "{\
\"type\":%s,\
\"action\":%i,\
\"srnr\":%lu,\
\"RFID\":\"%s\",\
\"permission\":%i\
}\n"
//{"type":"user","action":"info","srnr":"123456","ID":"EEEEEEEEEEEE","permission":0}
#define COMMUNICATE_TIMEOUT                 10

extern char communicate_wait_for_ok;
char communicate_wait_for_ok_time;
extern int communicate_wait_for_ok_index;
extern int communicate_lost_msn;

void communicate_init(void);
void communicate_adminmode_send_JSON(char *type,
                                     unsigned char action,
                                     unsigned long serialNr,
                                     char *ID,
                                     unsigned char permission);
void communicate_measurementSendJSON(unsigned long serialNr,
										unsigned int time,
										unsigned int dataVersion,
										char *ID,
										double batteryVoltage,
										double batteryCurrent,
										double loadAh,
										double loadAhCons,
										double loadKwh,
										double loadKwhCons,
										double onboardTemp,
										unsigned int state,
										unsigned int state_time,
										unsigned char status_changed,
										unsigned char status_movement);

void communicate_measurementSendJSON_picture(unsigned long serialNr,
										unsigned int time,
										unsigned int dataVersion,
										char *ID,
										double batteryVoltage,
										double batteryCurrent,
										double loadAh,
										double loadAhCons,
										double loadKwh,
										double loadKwhCons,
										double onboardTemp,
										unsigned int state,
										unsigned int state_time,
										unsigned char status_changed,
										unsigned char status_movement);
int communicate_check_json_answer(unsigned int index);

void communicate_check_communication(void);

#endif //COMMUNICATE_H_
