/*
* chargerCtrl.h
*
*  Created on: 28.04.2015
*      Author: H�tter Wolfgang
*/
#ifndef CHARGERCTRL_H_
#define CHARGERCTRL_H_

// ******************* DEFINES ADCs ************************
// *********************************************************
#define HW_ADCEXT_SET_CS_A      IOSET0 = 1 << 16 //0x10000        //set CS_A
#define HW_ADCEXT_CLR_CS_A      IOCLR0 = 1 << 16 //0x10000        //clear CS_A
#define HW_ADCEXT_SET_CS_D      IOSET0 = 1 << 20 //0x10000        //set CS_D
#define HW_ADCEXT_CLR_CS_D      IOCLR0 = 1 << 20 //0x10000        //clear CS_D
#define HW_ADCEXT_SET_GPIO      PINSEL1 &=  0xFFFFFCFC  //als GPIO
#define HW_ADCEXT_SET_GPIO_DIR  IODIR0  |=  (1<<16) | (1<<20);  //als OUTPUT
#define HW_ADCEXT_SSPCR_A       SSPCR0 = 0x02cf         //SSP Control Register 0 --> 16bit transfer, SPI... - Manual S.201 0x02cf=0b1011001111
#define HW_ADCEXT_SSPCR_D       SSPCR0 = 0x02cb         //SSP Control Register 0 --> 12bit transfer, SPI... - Manual S.201 0x02cb=0b1011001011
#define HW_ADCEXT_REF_CH_0      2500                    //Reference Voltage 16 Bitler
#define HW_ADCEXT_REF_CH_1      3300                    //Reference Voltage 12 Bitler
#define HW_ADCEXT_RES           16                      //resolution in bit
#define HW_ADCEXT_MAXVAL_CH_0   65536                   //use instead of resolution (otherwise we need a exp2 function -time !?)
#define HW_ADCEXT_MAXVAL_CH_1   4095                    //use instead of resolution (otherwise we need a exp2 function -time !?)
#define HW_ADCEXT_GAIN_CH_0     56                      //the gain of signal
#define HW_ADCEXT_OFFSET_CH_1   2.5                    //Offset of the HSP-200 currentsensor --> 2500mV
#define HW_ADCEXT_IPN_CH_1      400                    //nominal current of the HSP-400 currentsensor --> 400A


// ******************* DEFINES LEDs ************************
// *********************************************************
#define HW_EXT_LED_CLR IOCLR0
#define HW_EXT_LED_SET IOSET0
#define HW_EXT_LED_PIN IOPIN0

#define HW_LED_CLR IOCLR0
#define HW_LED_SET IOSET0

#define HW_STATUS_EXTLED_FIRST 11

#define HW_EXTLED_GREEN     3
#define HW_EXTLED_RED       2
#define HW_EXTLED_ORANGE    1




//functions
#endif //CHARGERCTRL_H_
