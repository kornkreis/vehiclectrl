/*
 * status_led.h
 *
 *  Created on: 21.05.2014
 *      Author: huetter_st
 */
#pragma once
#ifndef STATUS_LED_H_
#define STATUS_LED_H_

#include "Defines.h"

#define STATUS_LED_FIRST	4 //0.4 ist erster PIN

#define STATUS_LED_PEEP_PIN     IOPIN1
#define STATUS_LED_PEEP_NR      (1<<19)
#define STATUS_LED_PEEP_SET     IOSET1 = STATUS_LED_PEEP_NR
#define STATUS_LED_PEEP_CLR     IOCLR1 = STATUS_LED_PEEP_NR

#define STATUS_LED_TIME_PEEP    300

//#define STATUS_EXTLED_FIRST	11

void status_LED_init(void);
void status_LED_enable(void);
void status_LED_disable(void);
void status_LED_set(unsigned char nr);
void status_LED_clear(unsigned char nr);
void status_LED_toggl(unsigned char nr);
void status_extLED_set(unsigned char nr);
void status_extLED_clear(unsigned char nr);
void status_extLED_toggl(unsigned char nr);
void status_LED_control(unsigned char ctrType, int ms, unsigned int sec);

//int status_LED_time_batt_discon;

#endif /*STATUS_LED_H_*/

