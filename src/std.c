/*
 * std.c
 *
 *  Created on: 10.12.2010
 *      Author: hab
 */
#include "std.h"
//#include "Serielle.h"
#include "Main.h"

/*
*  strlen()|std.c
*  
*  Parameter:		char *s
* 
*  Return:			int Länge des Strings
*
*  Beschreibung:	Liefert die Länge des Strings
*
*/
int strlen(char *s)
{
	// ohne die hintere 0
	int i = 0;
	char *cp = s;
	for(i = 0; i < MAX_STRING_LENGTH; i++) //Begrenzen
	{
		if(*cp++ == 0)
			return i;

	}
	return 0;
}