/*
    date:   15.07.2015
    author: Sonja Klug
*/

#define WATERMARK   32

// Globale Variablen
extern unsigned char AccelData[6];                 //speichert Messdaten
extern char AccelData_Fifo_8bit[96];                // FIFO 8bit Daten
extern unsigned char AccelData_Fifo_12bit[192];              // Fifo 12 bit
extern float Xout_g, Yout_g, Zout_g;                //speichert Daten in g
extern short Xout_12_bit, Yout_12_bit, Zout_12_bit; //speichert 12 Bit Daten
extern unsigned char DataReady_FIFO;

extern unsigned int accel_no_move_count;

void accel_checkAcceleration(float X_val, float Y_val, float Z_val);
void accel_addValuesToCumVars(float X_val, float Y_val, float Z_val);
void accel_reset_cumVars(void);

// Prototypen
void MMA8652FC_Init();
void MMA8652FC_Calibration();
void get_accel_data();



void __attribute__ ((interrupt("IRQ"))) accel_isr();
