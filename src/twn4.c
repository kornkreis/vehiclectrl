/*
* twn4.c
*
*  Created on: 23.02.2016
*      Author: H�tter Wolfgang
*/
#include <stdlib.h>
#include "twn4.h"
#include "iic.h"
#include "timer.h"
#include "status_led.h"
#include "hardware.h"
#include "LPC214x.h"
#include "sc16is7xx.h"


#include "memory.h"
#include "accessCtrl.h"
#include "status.h"
#include "stringHelper.h"

/**remove!!!**/
#include "serielle.h"
#include <stdio.h>
/***/

char Twn4LastIDString[TWN4_MAX_ID_SIZE*2];
uint32_t Twn4LastIDAddress;

char Twn4TempString[TWN4_MAX_ID_SIZE*2]; // as ASCII String
char* Twn4StartMessage = "It is On\0";

unsigned char twn4online;
unsigned char Twn4Busy = 0;
unsigned char Twn4IdChanged = 0;

/* twn4_init()|twn4.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:	initializing of RFID Elatec Module
*
*/
void twn4_init(){
    HW_TWN4_POWER_ON;
    hardware_TWN4_reset(100);

    sc16is7xx_send_char_to_uart(CHANA,0x06); // CMD_GET_START_MSG
    sc16is7xx_send_char_to_uart(CHANA,'>'); // END SIGN
}

/* twn4_check_waiting_message()|twn4.c
*
*  Parameter:		---
*
*  Return:			true -> there is a message waiting
*                   false -> there is no message waiting
*
*  Description:	    checks if in sc16is7xx is data in receiver, on canal A
*
*/
unsigned char twn4_check_waiting_message(){
    status_LED_set(2);

    if(sc16is7xx_is_data_in_receiver(CHANA)){
        status_LED_clear(2);
        return 1;
    }
    else{
        status_LED_clear(2);
        return 0;
    }
}

/* twn4_handle_message()|twn4.c
*
*  Parameter:		---
*
*  Return:			---
*
*  Description:
*
*/
void twn4_handle_message(){
    char readString[TWN4_MAX_ID_SIZE*2];//;
    char tempID[TWN4_MAX_ID_SIZE];
    uint64_t uintID;
    unsigned char i;
    unsigned char strLen;

    sc16is7xx_Read_String(CHANA,readString);
    sprintf(serielle_text,"TWN4 says: %s\n",readString);
    serielle_send(serielle_text);

    //if Startmessage received
    if(str_cmp(readString,Twn4StartMessage,str_len(readString))){
        twn4online = TWN4_ONLINE;
        sprintf(serielle_text,"TWN4 says hello with: %s\n",readString);
        serielle_send(serielle_text);
        return;
    }

    // if string is not readable
    if(!str_is_readable(readString)){
        sprintf(serielle_text,"TWN4 message is not readable: %s\n",readString);
        serielle_send(serielle_text);

        sc16is7xx_send_char_to_uart(CHANA, 0x07); // CMD_MESSAGE_NOT_READABLE
        sc16is7xx_send_char_to_uart(CHANA, AccCtrlActPermission); //ACCES
        sc16is7xx_send_char_to_uart(CHANA, '>'); // END SIGN
        return;
    }

    /*sprintf(serielle_text,"address: %lu\n",memory_RFID_getMemAddr(readString));
    serielle_send(serielle_text);*/
    Twn4LastIDAddress = memory_RFID_getMemAddr(readString);
    str_cpy_RFID_ASCII(Twn4LastIDString,readString);


    uintID = str_string_to_uint64(readString);
    strLen = str_len(readString);
    /*for(i = 0; i < strLen; i++)
        tempID[i] = (char)((uintID >> strLen-(i*4))&0x00000000000000FF);*/

    str_string_to_INTarray(readString,tempID,strLen);
    str_cpy_RFID_arr(tempID,tempID,(strLen/2)+1);
    AccCtrlActPermission = accCtrl_RFID_checkRFID(tempID);

    status_deside(AccCtrlActPermission,0,0);
    Twn4IdChanged = 1;

    sc16is7xx_send_char_to_uart(CHANA, 0x05); // CMD_UI_SHOW_ACCESS
    sc16is7xx_send_char_to_uart(CHANA, AccCtrlActPermission); //ACCESS
    sc16is7xx_send_char_to_uart(CHANA, '>'); // END SIGN
}
