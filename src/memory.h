/*
 * memory.h
 *
 *  Created on: 21.01.2011
 *      Author: Stefan Hütter
 */
#ifndef MEMORY_H_
#define MEMORY_H_
#include <stdint.h>

#define MEMORY_PARAM_START_ADDR					0x00000000
#define MEMORY_PARAM_SN_NO_ADDR 				0x00000000 //32bit (4Byte)
#define MEMORY_PARAM_FLASH_VERSION_ADDR 			0x00000004 //16bit (2Byte)
#define MEMORY_PARAM_HW_VERSION_ADDR			0x00000006 //16bit (2Byte)
#define MEMORY_PARAM_BAT_BUSY_I_OFFSET_ADDR		0x00000008 //8bit (1Byte)
#define MEMORY_PARAM_TIME_RUN_ADDR				0x00000009 //16bit (2Byte)
#define MEMORY_PARAM_TIME_BUSY_ADDR				0x0000000B //16bit (2Byte)
#define MEMORY_PARAM_BATT_TEMP_R_OFFSET_ADDR	0x0000000D //16bit (2Byte)
#define MEMORY_PARAM_BATT_TEMP_U_SUB_ADDR		0x0000000F //16bit (2Byte)
#define MEMORY_PARAM_BATT_TEMP_U_GAIN_ADDR		0x00000011 //16bit (2Byte)
#define MEMORY_PARAM_U_OFFSET_ADDR				0x00000013 //16bit (2Byte)
#define MEMORY_PARAM_U_GAIN_ADDR				0x00000015 //16bit (2Byte)
#define MEMORY_PARAM_I_OFFSET_ADDR				0x00000017 //16bit (2Byte)
#define MEMORY_PARAM_I_GAIN_ADDR				0x00000019 //16bit (2Byte)

#define MEMORY_MAX_ADDRESS                      0x200000

//*** Wolfi ****
#define MEM_PARAM_U_BAT_MODI_THRS              0x00000020 //16bit (2byte) threshold for mode busy/idle
// *** end wolfi ***

#define MEMORY_DATA_START_ADDR	0x00010000

extern unsigned long memory_sn_no;
extern short memory_flash_version;
extern short memory_fw_version;
extern short memory_hw_version;

extern uint32_t memory_calib_batt_i_k;
extern uint32_t memory_calib_batt_u_k;
extern int32_t memory_calib_batt_i_d;
extern int32_t memory_calib_batt_u_d;
extern char memory_calib_batt_avg;

extern uint32_t memory_movement_count_threshold;
extern double memory_x_threshold;
extern double memory_y_threshold;
extern double memory_z_threshold;

extern double memory_calib_weight0;
extern double memory_calib_weight_faktor;
extern unsigned int memory_status_log_off_delay;
extern unsigned long memory_data_version;

void memory_init(void);
void memory_paramSetToDefault(void);
void memory_paramSaveAllParams(void);
void memory_paramLoadAllParams(void);

uint32_t memory_RFID_getMemAddr(char *ID);
void memory_RFID_readRFID(uint32_t address, char *readID);
unsigned char memory_RFID_saveRFID_perm(char *ID, unsigned char perm);

unsigned char memory_get_RFID_perm(char *ID);
unsigned char memory_RFID_is_empty(uint32_t address);
unsigned char memory_RFID_is_ID_empty(char* readID);
//void memory_RFID_deleteRFID_perm(char *ID);

void memory_debug_write0_to_page(uint32_t pageAddr);
void memory_debug_read_page(uint32_t pageAddr);

#endif /* MEMORY_H_*/
